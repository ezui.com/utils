package org.activiti.app.rest.runtime;

import java.util.List;

import javax.inject.Inject;

import org.activiti.app.service.exception.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ezui.bpm.app.db.mapping.entity.PartyLiveRelationship;
import com.ezui.bpm.app.rest.AbstractTaskQueryResource;
import com.ezui.bpm.app.rest.model.common.EntityPartyRequest;
import com.ezui.bpm.app.rest.model.common.PartyLiveQueryRequest;
import com.ezui.bpm.app.rest.model.common.ResultListDataRepresentation;
import com.ezui.bpm.app.service.PartyLiveService;

@RestController
public class PartyLiveResource extends AbstractTaskQueryResource {
	
	@Inject
	protected PartyLiveService partyLiveService;
	
	@RequestMapping(value = "/rest/query/getEntityPartyRelationships", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public ResultListDataRepresentation getEntityPartyRelationships(@RequestBody PartyLiveQueryRequest form) {
		if (form == null) {
			throw new BadRequestException("No request found");
		}
		
//		int offset = form.getStart();
//		int limit = form.getSize();
//		String orderBy = "";
		
		List<PartyLiveRelationship> data = partyLiveService.getEntityPartyRelationships(form.getEntityName(), form.getEntityRk());//, offset, limit, orderBy);
		
		ResultListDataRepresentation result = new ResultListDataRepresentation(data);
//		result.setPage(form.getPage());
//		result.setSize(form.getSize());
		
//		Long totalCount = Long.valueOf(data.size());
//		if (data.size() == form.getSize()) {
//			totalCount = partyLiveService.countEntityPartyRelationships(form.getEntityName(), form.getEntityRk());
//			result.setTotal(Long.valueOf(totalCount.intValue()));
//		} else {
//			if(form.getPage()==1){
//				result.setTotal(totalCount);
//			}else{
//				result.setTotal(totalCount+form.getPage()*form.getSize());
//			}
//		}
		
		return result;
	}
	
	@RequestMapping(value = "/rest/query/searchEntityParty", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public ResultListDataRepresentation searchEntityParty(@RequestBody EntityPartyRequest form) {
		if (form == null) {
			throw new BadRequestException("No request found");
		}
		
		int offset = form.getStart();
		int limit = form.getSize();
		String orderBy = "";
		
		List<PartyLiveRelationship> data = partyLiveService.searchEntityParty(form.getPartyId(), form.getPartyFullNm(), form.getPartyTypeCd(), form.getPartyCategoryCd(), offset, limit, orderBy);//.getEntityPartyRelationships(form.getEntityName(), form.getEntityRk(), offset, limit, orderBy);
		
		ResultListDataRepresentation result = new ResultListDataRepresentation(data);
		result.setPage(form.getPage());
		result.setSize(form.getSize());
		
		Long totalCount = Long.valueOf(data.size());
//		if (data.size() == form.getSize()) {
			totalCount = partyLiveService.countEntityParty(form.getPartyId(), form.getPartyFullNm(), form.getPartyTypeCd(), form.getPartyCategoryCd());//.countEntityPartyRelationships(form.getEntityName(), form.getEntityRk());
			result.setTotal(Long.valueOf(totalCount.intValue()));
//		} else {
//			if(form.getPage()==1){
//				result.setTotal(totalCount);
//			}else{
//				result.setTotal(totalCount+form.getPage()*form.getSize());
//			}
//		}
		
		return result;
	}
	
}
