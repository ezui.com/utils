package com.ezui.tcb.adws3.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.ezui.cxf.tcb.adws3.ADWS3Locator;
import com.ezui.cxf.tcb.adws3.ADWS3Soap;

//import com.ezui.tcb.adws3.ADWS3;
//import com.ezui.tcb.adws3.ADWS3Soap;

public class ADWS3SoapUtil {
	private static ADWS3Soap service;
	public static ADWS3Soap getADWS3SoapInstance(String url) {
		if (service == null) {
			try {
				URL wsdlDocumentLocation = new URL(url);
				// ADWS3 adws = new ADWS3(wsdlDocumentLocation);
				// service = adws.getADWS3Soap();
				service = new ADWS3Locator().getADWS3Soap(wsdlDocumentLocation);
			} catch (MalformedURLException e) {
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return service;
	}

	public String getAllGroup() throws RemoteException {
		return service.allOUUnit();
	}
}
