package com.ezui.tcb.adws3.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.input.SAXBuilder;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ezui.idm.entity.EzDepartment;
import com.ezui.idm.entity.EzGroup;
import com.ezui.idm.entity.EzUser;

import java.io.IOException;
import java.io.StringReader;

/*主要將  AD WebService XML檔Parse出來*/
public class TcbAdParse {
	private static Logger logger = LoggerFactory.getLogger(TcbAdParse.class);
	public TcbAdParse() {
	}
	@SuppressWarnings("unchecked")
	public static Map<String, EzGroup> parseAllGroupXml(String xmlStr) throws IOException, JDOMException {
		Map<String, EzGroup> ezgroups = new HashMap<String, EzGroup>();
		if (xmlStr != null && xmlStr.trim().length() > 0) {
			SAXBuilder builder = new SAXBuilder(false);
			StringReader read = new StringReader(xmlStr);
			Document jdom = builder.build(read);
			Element elmtDepartmentAD = jdom.getRootElement(); // 根
			// 印出所有根節點的子節點
			List<Element> children1 = elmtDepartmentAD.getChildren("Group"); // 取出所有名為Group element的List
			for (int i = 0; i < children1.size(); i++) {
				Element el = children1.get(i);
				String name = el.getChildText("Name").trim();
				String desc = el.getChildText("description").trim();
				logger.debug("filter group name:{}, description:{} with aml_ wording", name, desc);
				name = name.toLowerCase();
				if (name.startsWith("aml_")) {
					EzGroup group = new EzGroup();
					group.setId(name);
					group.setGroupName(name);
					if (desc.length() > 0) {
						group.setGroupDescription(desc);
						group.setGroupDisplayName(desc);
					}
					ezgroups.put(name, group);
				}
			}
		}
		return ezgroups;
	}
	@SuppressWarnings("unchecked")
	public static Map<String, EzDepartment> parseALLOUUnitXml(String xmlStr) throws IOException, JDOMException {
		Map<String, EzDepartment> ezdepts = new HashMap<String, EzDepartment>();
		if (xmlStr != null && xmlStr.trim().length() > 0) {
			SAXBuilder builder = new SAXBuilder(false);
			StringReader read = new StringReader(xmlStr);
			Document jdom = builder.build(read);
			Element elmtDepartmentAD = jdom.getRootElement(); // 根
			List<Element> children1 = elmtDepartmentAD.getChildren("OU"); // 取出所有名為OU element的List
			for (int i = 0; i < children1.size(); i++) {
				Element el = (Element) children1.get(i);
				// unitname="資訊部" ouid="A01419" accid="A01419" connid="0010" unitid="0410" telphoNo="02-27045799" faxNo="02-27021435" address=""
				String deptNo = el.getAttributeValue("ouid").trim();
				String deptName = el.getAttributeValue("unitname").trim();
				String telNo = el.getAttributeValue("telphoNo").trim();
				String faxNo = el.getAttributeValue("faxNo").trim();
				String address = el.getAttributeValue("address").trim();
				EzDepartment dept = new EzDepartment();
				dept.setId(deptNo);
				dept.setDeptNo(deptNo);
				dept.setDeptName(deptName);
				if (telNo.length() > 0) {
					dept.setTelNo(telNo);
				}
				if (faxNo.length() > 0) {
					dept.setFaxNo(faxNo);
				}
				if (address.length() > 0) {
					dept.setAddress(address);
				}
				ezdepts.put(deptNo, dept);
			}
		}
		return ezdepts;
	}
	@SuppressWarnings("unchecked")
	public static Map<String, EzUser> parseGroupUserXml(String xmlStr) throws IOException, JDOMException {
		Map<String, EzUser> ezusers = new HashMap<String, EzUser>();
		if (xmlStr != null && xmlStr.trim().length() > 0) {
			SAXBuilder builder = new SAXBuilder(false);
			StringReader read = new StringReader(xmlStr);
			Document jdom = builder.build(read);
			Element elmtDepartmentAD = jdom.getRootElement(); // 根
			List<Element> children0 = elmtDepartmentAD.getChildren("accounts"); // 取出所有名為accounts element的List
			for (int x = 0; x < children0.size(); x++) {
				List<Element> children1 = children0.get(x).getChildren("userid"); // 取出所有名為userid element的List
				for (int i = 0; i < children1.size(); i++) {
					Element el = (Element) children1.get(i);
					String userId = el.getChildText("uID").trim();
					String userName = el.getChildText("description").trim();
					String memberOf = el.getChildText("memberOf").trim();
					String mail = el.getChildText("mail").trim();
					String title = el.getChildText("title").trim();
					String info = el.getChildText("info").trim();
					String ou = el.getChildText("ou").trim();
					String departmentNumber = el.getChildText("departmentNumber").trim();
					String department = el.getChildText("department").trim();
					EzUser user = new EzUser();
					user.setId(userId);
					user.setLoginId(userId);
					if (userName.length() > 0) {
						user.setUsername(userName);
					} else {
						user.setUsername(userId);
					}
					if (mail.length() > 0) {
						user.setEmail(mail);
					}
					if (memberOf.length() > 0) {
						user.setUserLdapDn(memberOf);
					}
					if (title.length() > 0) {
						user.setDefaultJobTitle(title);
					}
					if (info.length() > 0) {
						user.setUserDescription(info);
					}
					if (ou.length() > 0) {
						user.setDefaultBranchNo(ou);
					}
					if (departmentNumber.length() > 0) {
						user.setDefaultDeptNo(departmentNumber);
					}
					if (department.length() > 0) {
						user.setDefaultDeptName(department);
						user.setDefaultBranchName(department);
					}
					ezusers.put(userId, user);
				}
			}
		}
		return ezusers;
	}
	@SuppressWarnings("unchecked")
	public static UserInfo parseUserInfoXml(String xmlStr) throws IOException, JDOMException {
		UserInfo userInfo = new UserInfo();
		if (xmlStr != null && xmlStr.trim().length() > 0) {
			SAXBuilder builder = new SAXBuilder(false);
			StringReader read = new StringReader(xmlStr);
			Document jdom = builder.build(read);
			Element elmtDepartmentAD = jdom.getRootElement(); // 根

			List<Element> statusEl = elmtDepartmentAD.getChildren("Status"); // 取出所有名為Status element的List
			for (int i = 0; i < statusEl.size(); i++) {
				Element el = statusEl.get(i);
				String code = el.getAttributeValue("code").trim();
				String desc = el.getAttributeValue("Description").trim();
				userInfo.setAuthStatus(code);
				userInfo.setAuthDesc(desc);
			}
			// 印出所有根節點的子節點
			List<Element> children1 = elmtDepartmentAD.getChildren("userid"); // 取出所有名為userid element的List
			for (int i = 0; i < children1.size(); i++) {
				Element el = children1.get(i);
				String userId = el.getChildText("sAMAccountName").trim();
				String userName = el.getChildText("description").trim();
				String memberOf = el.getChildText("memberOf").trim();
				String mail = el.getChildText("mail").trim();
				String title = el.getChildText("title").trim();
				String info = el.getChildText("info").trim();
				String ou = el.getChildText("ou").trim();
				String departmentNumber = el.getChildText("departmentNumber").trim();
				String department = el.getChildText("department").trim();
				EzUser user = new EzUser();
				user.setId(userId);
				user.setLoginId(userId);
				if (userName.length() > 0) {
					user.setUsername(userName);
				} else {
					user.setUsername(userId);
				}
				if (mail.length() > 0) {
					user.setEmail(mail);
				}
				if (memberOf.length() > 0) {
					user.setUserLdapDn(memberOf);
				}
				if (title.length() > 0) {
					user.setDefaultJobTitle(title);
				}
				if (info.length() > 0) {
					user.setUserDescription(info);
				}
				if (ou.length() > 0) {
					user.setDefaultBranchNo(ou);
				}
				if (departmentNumber.length() > 0) {
					user.setDefaultDeptNo(departmentNumber);
				}
				if (department.length() > 0) {
					user.setDefaultDeptName(department);
					user.setDefaultBranchName(department);
				}
				userInfo.setUser(user);
			}
		}
		return userInfo;
	}

}

/*
 * 開發時TCB ALLOUUnitXml取回的xml 格式備存一份以防日後格式有變 <Department> <OU unitname="高雄分行" ouid="0340" accid="E110" connid="0340" unitid="7210" telphoNo="(07)5514221" faxNo="(07)561-8991" address="高雄市大勇路97號" /> <OU
 * unitname="資訊部" ouid="A01419" accid="A01419" connid="0010" unitid="0410" telphoNo="02-27045799" faxNo="02-27021435" address="" /> <OU unitname="信託部" ouid="1047" accid="A047" connid="1047"
 * unitid="0650" telphoNo="(02)23118001" faxNo="" address="台北市中正區永綏街7號" /> <OU unitname="業務發展部" ouid="A01436" accid="A01436" connid="0010" unitid="0520" telphoNo="02-23118811" faxNo="02-23890704"
 * address="" /> <OU unitname="證券部" ouid="A092" accid="A092" connid="3362" unitid="0680" telphoNo="02-27312798" faxNo="02-87717193" address="106台北市忠孝東路4段325號2樓" /> <OU unitname="總務處" ouid="A01407"
 * accid="A01407" connid="0010" unitid="0270" telphoNo="02-23118811" faxNo="02-23752620" address="" /> <OU unitname="昌平分行" ouid="2033" accid="Y763" connid="2033" unitid="9678" telphoNo="(04)22443037"
 * faxNo="(04)2243-8751" address="台中市昌平路一段163號" /> <OU unitname="電子金融部" ouid="A01433" accid="A01433" connid="0010" unitid="0750" telphoNo="02-23317531" faxNo="02-23707640" address="" /> <OU
 * unitname="民權分行" ouid="0770" accid="A310" connid="0770" unitid="1390" telphoNo="(02)25057011" faxNo="(02)2505-5262" address="台北市民權東路三段58號" /> <OU unitname="屏南分行" ouid="1221" accid="Z300"
 * connid="1221" unitid="8130" telphoNo="(08)7326391" faxNo="(08)733-3867" address="屏東縣屏東市民生路287號" /> <OU unitname="圓山分行" ouid="0822" accid="Y148" connid="0822" unitid="1670" telphoNo="(02)25113245"
 * faxNo="(02)2571-5416" address="台北市中山北路二段89-4號" /> <OU unitname="忠孝分行" ouid="0450" accid="A285" connid="0450" unitid="1290" telphoNo="(02)27718811" faxNo="(02)2731-1982" address="台北市忠孝東路四段285號" />
 * <OU unitname="鳳山分行" ouid="0320" accid="S121" connid="0320" unitid="7810" telphoNo="(07)7460181" faxNo="(07)710-1465" address="高雄市鳳山區中正路95號" /> <OU unitname="南興分行" ouid="0680" accid="D131"
 * connid="0680" unitid="6270" telphoNo="(06)2221291" faxNo="(06)220-7847" address="台南市民生路二段72號" /> <OU unitname="沙鹿分行" ouid="0210" accid="L121" connid="0210" unitid="4170" telphoNo="(04)26622141"
 * faxNo="(04)2665-3062" address="台中市沙鹿區沙田路106號" /> <OU unitname="岡山分行" ouid="0330" accid="S110" connid="0330" unitid="7110" telphoNo="(07)6216161" faxNo="(07)621-6195" address="高雄市岡山區校前路2號" /> <OU
 * unitname="台中分行" ouid="0220" accid="B117" connid="0220" unitid="4210" telphoNo="(04)22245121" faxNo="(04)2225-8759" address="台中市自由路二段2號" /> <OU unitname="信義分行" ouid="0833" accid="Y159" connid="0833"
 * unitid="1690" telphoNo="(02)27067188" faxNo="(02)2702-7870" address="台北市信義路四段172號" /> <OU unitname="五權分行" ouid="0690" accid="B139" connid="0690" unitid="4230" telphoNo="(04)23229191"
 * faxNo="(04)2320-0601" address="台中市公益路二段61號" /> <OU unitname="美村分行" ouid="1988" accid="Y718" connid="1988" unitid="9673" telphoNo="(04)22614377" faxNo="(04)2265-5369" address="台中市南區復興路二段136號" /> <OU
 * unitname="東台南分行" ouid="1232" accid="D164" connid="1232" unitid="6310" telphoNo="(06)2882211" faxNo="(06)288-3697" address="台南市中華東路二段197號" /> <OU unitname="林內分行" ouid="3074" accid="W036"
 * connid="3074" unitid="9695" telphoNo="(05)5897811" faxNo="(05)589-6868" address="雲林縣林內鄉林中村中西路21 號" /> ................... <OU unitname="合作金庫資產管理公司" ouid="VB01" accid="VB01" connid="0010"
 * unitid="9830" telphoNo="02-87720868" faxNo="02-87727153" address="" /> <OU unitname="合作金庫保險經紀公司" ouid="VB02" accid="VB02" connid="0010" unitid="9840" telphoNo="02-23898811" faxNo="02-23891155"
 * address="" /> <OU unitname="合作金庫票券金融公司" ouid="VB03" accid="VB03" connid="0010" unitid="9850" telphoNo="02-25221656" faxNo="02-25221552" address="" /> <OU unitname="長沙分行" ouid="A01466"
 * accid="A01466" connid="0010" unitid="9180" telphoNo="0731-8823225" faxNo="0731-8823227" address="湖南省長沙市開福區芙蓉中路一段416號泊富國際廣場寫字樓28層021-023單元" /> <query_result> <record_count> 377/ 493</record_count>
 * <time_span>0.4775574</time_span> </query_result> </Department>
 */
/*
 * 開發時TCB GroupUnitUser取回的xml 格式備存一份以防日後格式有變 <?xml version="1.0" encoding="utf-8"?> <EIPAD> <accounts> <userid> <uID>0017emp</uID> <description /> <mail /> <title />
 * <memberOf>OU_COEIPUSER,WFT_EMPLOYEE,OU_0017</memberOf> <department>財務部</department> <departmentNumber>A081</departmentNumber> <info>財務部,0017,A081,0017,0540</info> <ou>0017</ou> </userid> <userid>
 * <uID>WANGJIM</uID> <description>汪*郎</description> <mail>WANGJIM@tcb-bank.com.tw</mail> <title>資深專員</title> <memberOf>OU_COEIPUSER,OU_0017</memberOf> <department>財務部</department>
 * <departmentNumber>A081</departmentNumber> <info>財務部,0017,A081,0017,0540,資深專員,0970</info> <ou>0017</ou> </userid> <query_result> <record_count>2</record_count> <record_affect>2</record_affect>
 * <time_span>0.0830025</time_span> </query_result> </accounts> </EIPAD>
 */
