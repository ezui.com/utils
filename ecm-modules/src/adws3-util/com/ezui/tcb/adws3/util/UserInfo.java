package com.ezui.tcb.adws3.util;

import com.ezui.idm.entity.EzUser;

public class UserInfo {
	private String authStatus;
	private String authDesc;
	private EzUser user;
	public String getAuthStatus() {
		return authStatus;
	}
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}
	public String getAuthDesc() {
		return authDesc;
	}
	public void setAuthDesc(String authDesc) {
		this.authDesc = authDesc;
	}
	public EzUser getUser() {
		return user;
	}
	public void setUser(EzUser user) {
		this.user = user;
	}
	
}
