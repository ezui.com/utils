package com.ezui.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ezui.app.Version;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "App Runtime Exception")
public class AppRuntimeException extends RuntimeException {

	private static final long serialVersionUID = Version.GLOBAL_SERIAL_VERSION_UID;

	public AppRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public AppRuntimeException(String message) {
		super(message);
	}
	public AppRuntimeException(Throwable cause) {
		super(cause);
	}
}
