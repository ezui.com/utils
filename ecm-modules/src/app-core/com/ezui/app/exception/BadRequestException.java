package com.ezui.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ezui.app.Version;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="沒有權限,參數缺失")
public class BadRequestException extends AppRuntimeException{

	private static final long serialVersionUID = Version.GLOBAL_SERIAL_VERSION_UID;

	public BadRequestException(String message) {
		super(message);
	}
}
