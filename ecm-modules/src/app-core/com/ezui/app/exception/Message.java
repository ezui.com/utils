package com.ezui.app.exception;

import java.io.Serializable;

import org.slf4j.helpers.MessageFormatter;

import com.ezui.app.Version;

@SuppressWarnings("all")
public class Message implements Serializable{
	private static final long serialVersionUID = Version.GLOBAL_SERIAL_VERSION_UID;
	private String key;
	private String type;
	private String orgTitle;
	private String orgDetails;
	private String title;
	private String details;

	public Message() {

	}

	public Message(String key, String type, String orgTitle, String orgDetails) {
		this.key = key;
		this.type = type;
		this.orgTitle = orgTitle;
		this.orgDetails = orgDetails;
	}

	public String getKey() {
		return key;
	}

	public Message setKey(String key) {
		this.key = key;
		return this;
	}

	public String getType() {
		return type;
	}

	public Message setType(String type) {
		this.type = type;
		return this;
	}

	public String getTitle(Object[] s) {
		title=MessageFormatter.arrayFormat(getOrgTitle(),s).getMessage();
		return title;
	}

	public Message setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getDetails(Object[] s) {
		details=MessageFormatter.arrayFormat(getOrgDetails(),s).getMessage();
		return details;
	}

	public Message setDetails(String details) {
		this.details = details;
		return this;
	}

	public String getOrgTitle() {
		return orgTitle;
	}

	public void setOrgTitle(String orgTitle) {
		this.orgTitle = orgTitle;
	}

	public String getOrgDetails() {
		return orgDetails;
	}

	public void setOrgDetails(String orgDetails) {
		this.orgDetails = orgDetails;
	}

	public Message clone() {
		return new Message(this.getKey(),this.getType(), this.getOrgTitle(), this.getOrgDetails());
	}
	public static void main(String[] args) {
		Message msg=new Message("key", "error", "","{}{}{}{}")   ;
		System.out.println(msg.getDetails(new Object[] {"1","2","3","4"}));
	}
}
