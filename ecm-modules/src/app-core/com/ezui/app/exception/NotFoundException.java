package com.ezui.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ezui.app.Version;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Not Found")
public class NotFoundException extends AppRuntimeException{

	public NotFoundException(String message) {
		super(message);
	}

	private static final long serialVersionUID = Version.GLOBAL_SERIAL_VERSION_UID;

}
