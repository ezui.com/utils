package com.ezui.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ezui.app.Version;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "unauthorized!")
public class UnauthorizedException extends AppRuntimeException{
	private static final long serialVersionUID = Version.GLOBAL_SERIAL_VERSION_UID;
	public UnauthorizedException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnauthorizedException(String message) {
		super(message);
	}
	public UnauthorizedException(Throwable cause) {
		super(cause);
	}

}
