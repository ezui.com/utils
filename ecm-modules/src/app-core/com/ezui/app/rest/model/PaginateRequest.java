package com.ezui.app.rest.model;

public class PaginateRequest {
	protected Integer page;

	protected Integer start;
	protected Integer size;

	protected String sort;

	protected String order;

	public Integer getPage() {
		if (page == null || page < 1) {
			this.page = 1;
		}
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		if (size == null || size < 1) {
			this.size = 0;
		}
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSortBy() {
		String sortby = null;
		if (getSort() != null && getSort().trim().length() > 0) {
			sortby = getSort().trim();
			if (getOrder() != null) {
				if ("desc".equals(getOrder().toLowerCase())) {
					sortby += " desc";
				} else if ("asc".equals(getOrder().toLowerCase())) {
					sortby += " asc";
				}
			}
		}
		return sortby;
	}

	public Integer getStart() {
		if(start!=null) {
			return start;
		}
		return (this.getPage()-1) * this.getSize() + 1;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

}
