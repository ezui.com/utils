package com.ezui.app.rest.model;

public class PaginateResponse<T extends Object> {
	T data;
	long total;
	long start;
	long end;
	String sort;
	String order;
	int size=Integer.MAX_VALUE;
	int totalpage;
	int page;
	int pageStart;
	int pageEnd;

	public void setPaginateRequest(PaginateRequest request) {
		this.start = request.getStart();
		this.sort = request.getSort();
		this.order = request.getOrder();
		this.setSize(request.getSize());
		this.page = request.getPage();
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getStart() {
		long p = 1;
		if (getTotal() > getSize()) {
			p = (getSize() * this.getPage()) - this.getSize() + 1;
		}
		return p;
//		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getTotalpage() {
		if (this.getTotal() > 0 && this.getSize() > 0) {
			this.totalpage = (int) Math.ceil((double)this.getTotal() / (double)this.getSize());
		}
		return totalpage;
	}

	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public long getEnd() {
		this.end=this.start+this.getSize()-1;
		if(this.end>this.total){
			this.end=this.total;
		}
		return this.end;
	}

	public int getPageStart() {
		return 1;
	}

	public void setPageStart(int pageStart) {
		this.pageStart = pageStart;
	}

	public int getPageEnd() {
		int p = 1;
		if (getTotal() > getSize()) {
			p = (int)getTotal() / getSize();
			int d = (int)getTotal() % getSize();
			if (d>0) {
				p++;
			}
		}
		
		return p;
	}

	public void setPageEnd(int pageEnd) {
		this.pageEnd = pageEnd;
	}
	
}
