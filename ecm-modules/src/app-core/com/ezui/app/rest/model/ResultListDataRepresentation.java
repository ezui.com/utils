package com.ezui.app.rest.model;

import java.util.List;

public class ResultListDataRepresentation extends
		PaginateResponse<List<? extends Object>> {
	public ResultListDataRepresentation(List<? extends Object> data) {
		this.data = data;
		if(data!=null&&this.getTotal()==0l) {
			this.setTotal(data.size());
		}
	}
}
