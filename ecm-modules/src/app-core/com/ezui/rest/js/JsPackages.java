package com.ezui.rest.js;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JsPackages {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/ezjs/{filename}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public String getJs(@PathVariable String filename, HttpServletRequest req, HttpServletResponse response) {
		String ret = "";
		InputStream jsin = null;
		try {
			response.setContentType("application/javascript;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			jsin = JsPackages.class.getClassLoader().getResourceAsStream("META-INF/"+filename);
			InputStreamReader isReader = new InputStreamReader(jsin, "UTF-8");
			// Creating a BufferedReader object
			BufferedReader reader = new BufferedReader(isReader);
			StringBuffer sb = new StringBuffer();
			String str;
			while ((str = reader.readLine()) != null) {
				sb.append(str+"\n");
			}
			ret=sb.toString();
		} catch (IOException e) {
			log.error("load js failed!", e);
		} finally {
			if (jsin != null)
				IOUtils.closeQuietly(jsin);
		}
		return ret;
	}

}
