/**
 * ADWS3.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ezui.cxf.tcb.adws3;

public interface ADWS3 extends javax.xml.rpc.Service {
    public java.lang.String getADWS3SoapAddress();

    public com.ezui.cxf.tcb.adws3.ADWS3Soap getADWS3Soap() throws javax.xml.rpc.ServiceException;

    public com.ezui.cxf.tcb.adws3.ADWS3Soap getADWS3Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
