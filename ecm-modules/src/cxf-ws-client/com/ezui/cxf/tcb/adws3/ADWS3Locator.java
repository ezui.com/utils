/**
 * ADWS3Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ezui.cxf.tcb.adws3;

public class ADWS3Locator extends org.apache.axis.client.Service implements com.ezui.cxf.tcb.adws3.ADWS3 {

    public ADWS3Locator() {
    }


    public ADWS3Locator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ADWS3Locator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ADWS3Soap
    private java.lang.String ADWS3Soap_address = "http://10.0.7.182:88/adws/adws3.asmx";

    public java.lang.String getADWS3SoapAddress() {
        return ADWS3Soap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ADWS3SoapWSDDServiceName = "ADWS3Soap";

    public java.lang.String getADWS3SoapWSDDServiceName() {
        return ADWS3SoapWSDDServiceName;
    }

    public void setADWS3SoapWSDDServiceName(java.lang.String name) {
        ADWS3SoapWSDDServiceName = name;
    }

    public com.ezui.cxf.tcb.adws3.ADWS3Soap getADWS3Soap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ADWS3Soap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getADWS3Soap(endpoint);
    }

    public com.ezui.cxf.tcb.adws3.ADWS3Soap getADWS3Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ezui.cxf.tcb.adws3.ADWS3SoapStub _stub = new com.ezui.cxf.tcb.adws3.ADWS3SoapStub(portAddress, this);
            _stub.setPortName(getADWS3SoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setADWS3SoapEndpointAddress(java.lang.String address) {
        ADWS3Soap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.ezui.cxf.tcb.adws3.ADWS3Soap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ezui.cxf.tcb.adws3.ADWS3SoapStub _stub = new com.ezui.cxf.tcb.adws3.ADWS3SoapStub(new java.net.URL(ADWS3Soap_address), this);
                _stub.setPortName(getADWS3SoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ADWS3Soap".equals(inputPortName)) {
            return getADWS3Soap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/ADWebWSVB/ADWS3", "ADWS3");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/ADWebWSVB/ADWS3", "ADWS3Soap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ADWS3Soap".equals(portName)) {
            setADWS3SoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
