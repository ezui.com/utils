package com.ezui.cxf.tcb.adws3;

public class ADWS3SoapProxy implements com.ezui.cxf.tcb.adws3.ADWS3Soap {
  private String _endpoint = null;
  private com.ezui.cxf.tcb.adws3.ADWS3Soap aDWS3Soap = null;
  
  public ADWS3SoapProxy() {
    _initADWS3SoapProxy();
  }
  
  public ADWS3SoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initADWS3SoapProxy();
  }
  
  private void _initADWS3SoapProxy() {
    try {
      aDWS3Soap = (new com.ezui.cxf.tcb.adws3.ADWS3Locator()).getADWS3Soap();
      if (aDWS3Soap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)aDWS3Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)aDWS3Soap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (aDWS3Soap != null)
      ((javax.xml.rpc.Stub)aDWS3Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.ezui.cxf.tcb.adws3.ADWS3Soap getADWS3Soap() {
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap;
  }
  
  public java.lang.String allUserId(java.lang.String OUId, java.lang.String ADPath, java.lang.String stringformat, java.lang.String withOU, java.lang.String sort) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.allUserId(OUId, ADPath, stringformat, withOU, sort);
  }
  
  public java.lang.String allUserInfo(java.lang.String OUId, java.lang.String ADPath, java.lang.String param, java.lang.String groupIn) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.allUserInfo(OUId, ADPath, param, groupIn);
  }
  
  public java.lang.String allUserInfo2(java.lang.String OUId, java.lang.String ADPath, java.lang.String param, java.lang.String groupIn, java.lang.String hasValue) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.allUserInfo2(OUId, ADPath, param, groupIn, hasValue);
  }
  
  public java.lang.String userInfo_log(java.lang.String userid, java.lang.String userpswd, java.lang.String logfile) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.userInfo_log(userid, userpswd, logfile);
  }
  
  public java.lang.String userInfo(java.lang.String userid, java.lang.String userpswd) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.userInfo(userid, userpswd);
  }
  
  public java.lang.String groupUnitUser(java.lang.String OUId, java.lang.String ADPath, java.lang.String groupIn, java.lang.String showinfo) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.groupUnitUser(OUId, ADPath, groupIn, showinfo);
  }
  
  public java.lang.String allOUUnit() throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.allOUUnit();
  }
  
  public java.lang.String allGroup(java.lang.String cnfilter, java.lang.String dsfilter) throws java.rmi.RemoteException{
    if (aDWS3Soap == null)
      _initADWS3SoapProxy();
    return aDWS3Soap.allGroup(cnfilter, dsfilter);
  }
  
  
}