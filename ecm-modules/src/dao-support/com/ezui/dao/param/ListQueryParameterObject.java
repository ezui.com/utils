package com.ezui.dao.param;

import java.io.Serializable;

public class ListQueryParameterObject extends DbQueryParameter implements Serializable{
	private static final long serialVersionUID = 2762757999173347615L;
	protected int maxResults = Integer.MAX_VALUE;
	protected int firstResult;
	protected String orderByClause;
	protected boolean readUncommited=false;
	protected boolean distinct=false;
	protected String orderByColumns;
	protected int firstRow;
	protected int lastRow;

	@Deprecated
	public int getMaxResults() {
		return maxResults;
	}

	@Deprecated
	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
		this.setLastRow(maxResults);
	}

	@Deprecated
	public int getFirstResult() {
		return firstResult;
	}

	@Deprecated
	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
		this.setFirstRow(firstResult);
	}

	@Deprecated
	public String getOrderByClause() {
		return orderByClause;
	}

	@Deprecated
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
		this.setOrderByColumns(orderByClause);
	}

	public boolean isReadUncommited() {
		return readUncommited;
	}

	public void setReadUncommited(boolean readUncommited) {
		this.readUncommited = readUncommited;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public String getOrderByColumns() {
		return orderByColumns;
	}

	public void setOrderByColumns(String orderByColumns) {
		this.orderByColumns = orderByColumns;
	}

	public int getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}

	public int getLastRow() {
		return lastRow;
	}

	public void setLastRow(int lastRow) {
		this.lastRow = lastRow;
	}

}
