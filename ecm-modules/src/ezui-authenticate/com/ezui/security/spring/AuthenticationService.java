package com.ezui.security.spring;

public interface AuthenticationService {
	public abstract boolean checkPassword(String userId,String password);
	public abstract boolean isSupport(String userSource);
}
