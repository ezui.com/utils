package com.ezui.security.spring;

public class BadUserResourceException extends RuntimeException {
	private static final long serialVersionUID = 3149219019949031657L;
	public BadUserResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public BadUserResourceException(String message) {
		super(message);
	}
	public BadUserResourceException(Throwable cause) {
		super(cause);
	}

}
