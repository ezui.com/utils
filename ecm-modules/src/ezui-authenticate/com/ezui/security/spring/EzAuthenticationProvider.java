package com.ezui.security.spring;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class EzAuthenticationProvider implements AuthenticationProvider, InitializingBean, MessageSourceAware {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private AuthenticationProvider tokenAuthenticationProvider;
	private DaoAuthenticationProvider daoAuthenticationProvider;
	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messages = new MessageSourceAccessor(messageSource);
	}

	@Override
	public void afterPropertiesSet() throws Exception {

	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (tokenAuthenticationProvider != null) {
			try {
				final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
				final HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
				String token = request.getHeader("eztoken");
				if (token == null) {
					token = request.getParameter("eztoken");
				}
				return tokenAuthenticationProvider.authenticate(new TokenAuthentication(token));
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}
		if (daoAuthenticationProvider != null) {
			return daoAuthenticationProvider.authenticate(authentication);
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (Authentication.class.isAssignableFrom(authentication));
	}

	public AuthenticationProvider getTokenAuthenticationProvider() {
		return tokenAuthenticationProvider;
	}

	public void setTokenAuthenticationProvider(AuthenticationProvider tokenAuthenticationProvider) {
		this.tokenAuthenticationProvider = tokenAuthenticationProvider;
	}

	public DaoAuthenticationProvider getDaoAuthenticationProvider() {
		return daoAuthenticationProvider;
	}

	public void setDaoAuthenticationProvider(DaoAuthenticationProvider daoAuthenticationProvider) {
		this.daoAuthenticationProvider = daoAuthenticationProvider;
	}

	public MessageSourceAccessor getMessages() {
		return messages;
	}

	public void setMessages(MessageSourceAccessor messages) {
		this.messages = messages;
	}

}
