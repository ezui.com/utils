package com.ezui.security.spring;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class EzTokenAuthenticationProvider implements AuthenticationProvider, InitializingBean, MessageSourceAware{

	@Override
	public void setMessageSource(MessageSource arg0) {
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String token=(String)authentication.getPrincipal();
		System.out.println(token);
		authentication.setAuthenticated(true);
		return authentication;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}
