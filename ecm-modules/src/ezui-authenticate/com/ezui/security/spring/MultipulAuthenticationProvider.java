package com.ezui.security.spring;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class MultipulAuthenticationProvider extends DaoAuthenticationProvider {
	@Inject
	private List<AuthenticationService> providers = new ArrayList<AuthenticationService>();
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		Assert.isInstanceOf(MutableUserDetail.class, userDetails, messages.getMessage("MultipulAuthenticationProvider.onlySupports", "Only MutableUserDetail is supported"));

		UserSource userSource = ((MutableUserDetail) userDetails).getUser();
		if (userSource == null || StringUtils.isEmpty(userSource.getUserSource())) {
			throw new BadUserResourceException(this.messages.getMessage("MultipulAuthenticationProvider.badUserSource", "Bad UserSource"));
		}
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		String usersource = userSource.getUserSource();
		boolean authenticated = false;
		for (AuthenticationService provider : providers) {
			if (provider.isSupport(usersource)) {
				authenticated = provider.checkPassword(name, password);
				if (authenticated) {
					break;
				}
			}
		}
		if (authenticated) {
		} else {
			throw new BadCredentialsException("Authentication failed for this username and password");
		}
		//super.additionalAuthenticationChecks(userDetails, authentication);
	}
	public List<AuthenticationService> getProviders() {
		return providers;
	}
	public void setProviders(List<AuthenticationService> providers) {
		this.providers = providers;
	}
}
