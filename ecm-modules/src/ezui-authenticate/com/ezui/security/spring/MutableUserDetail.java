package com.ezui.security.spring;

public interface MutableUserDetail<T extends UserSource> {
	public T getUser();
}
