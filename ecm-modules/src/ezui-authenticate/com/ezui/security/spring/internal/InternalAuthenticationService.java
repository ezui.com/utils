package com.ezui.security.spring.internal;

import javax.inject.Inject;

import org.activiti.engine.IdentityService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ezui.security.spring.AuthenticationService;

@Component
public class InternalAuthenticationService implements AuthenticationService {

	@Inject
	private IdentityService identityService;

	@Transactional
	public boolean checkPassword(String userId, String password) {
		return this.identityService.checkPassword(userId, password);
	}

	@Override
	public boolean isSupport(String userSource) {
		if("inline password".equals(userSource)||userSource==null){
			return true;
		}
		return false;
	}

}
