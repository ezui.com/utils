package com.ezui.bpm.app.db.dao.fcfcore;

import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranch;

public interface AccountIntegrationBranchDao {
	public List<AccountIntegrationBranch> selectByExampleWithRowbounds(
			int start, int pagesize, String sort);

	public long countByExample();
}
