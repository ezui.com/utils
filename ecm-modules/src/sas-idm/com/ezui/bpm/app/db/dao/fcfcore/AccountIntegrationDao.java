package com.ezui.bpm.app.db.dao.fcfcore;

import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegration;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationExample;

public interface AccountIntegrationDao {
	public List<AccountIntegration> selectByExampleWithRowbounds(
			AccountIntegrationExample query, int start, int pagesize,
			String sort);

	public long countByExample(AccountIntegrationExample query);
}
