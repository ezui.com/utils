package com.ezui.bpm.app.db.dao.fcfcore;

import java.util.Collections;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Component;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranch;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranchExample;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranchMapper;
import com.ezui.mybatis.MybatisAbstractDao;

@Component
public class MybatisAccountIntegrationBranchDao extends MybatisAbstractDao
		implements AccountIntegrationBranchDao {

	public List<AccountIntegrationBranch> selectByExampleWithRowbounds(
			int start, int pagesize, String sort) {
		AccountIntegrationBranchExample query = new AccountIntegrationBranchExample();
		if (start == -1 || pagesize == -1) {
			return Collections.EMPTY_LIST;
		}
		if (sort == null || sort.trim().length() == 0) {
			sort = "OU ASC";
		}
		query.setOrderByClause(sort);
		RowBounds rowBounds = new RowBounds(start, pagesize);
		AccountIntegrationBranchMapper mapper = this.getSqlSession()
				.getMapper(AccountIntegrationBranchMapper.class);
		return mapper.selectByExampleWithRowbounds(query, rowBounds);
	}

	public long countByExample() {
		AccountIntegrationBranchExample query = new AccountIntegrationBranchExample();
		AccountIntegrationBranchMapper mapper = this.getSqlSession()
				.getMapper(AccountIntegrationBranchMapper.class);
		return mapper.countByExample(query);
	}
}
