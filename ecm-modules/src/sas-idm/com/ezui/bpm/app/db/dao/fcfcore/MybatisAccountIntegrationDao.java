package com.ezui.bpm.app.db.dao.fcfcore;

import java.util.Collections;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Component;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegration;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationExample;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationMapper;
import com.ezui.mybatis.MybatisAbstractDao;

@Component
public class MybatisAccountIntegrationDao extends MybatisAbstractDao implements
		AccountIntegrationDao {

	public List<AccountIntegration> selectByExampleWithRowbounds(
			AccountIntegrationExample query, int start, int pagesize,
			String sort) {
		if (start == -1 || pagesize == -1) {
			return Collections.EMPTY_LIST;
		}
		if (sort == null || sort.trim().length() == 0) {
			sort = "OU ASC";
		}
		query.setOrderByClause(sort);
		RowBounds rowBounds = new RowBounds(start, pagesize);
		AccountIntegrationMapper mapper = this.getSqlSession()
				.getMapper(AccountIntegrationMapper.class);
		return mapper.selectByExampleWithRowbounds(query, rowBounds);
	}

	public long countByExample(AccountIntegrationExample query) {
		AccountIntegrationMapper mapper = this.getSqlSession()
				.getMapper(AccountIntegrationMapper.class);
		return mapper.countByExample(query);
	}
}
