package com.ezui.bpm.app.db.mapping.entity.fcfcore;

import java.io.Serializable;

public class AccountIntegration implements Serializable {
    private String empId;

    private String deptName;

    private String deptNo;

    private String email;

    private String empName;

    private String encrypw;

    private String ou;

    private static final long serialVersionUID = 1L;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId == null ? null : empId.trim();
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo == null ? null : deptNo.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName == null ? null : empName.trim();
    }

    public String getEncrypw() {
        return encrypw;
    }

    public void setEncrypw(String encrypw) {
        this.encrypw = encrypw == null ? null : encrypw.trim();
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou == null ? null : ou.trim();
    }
}