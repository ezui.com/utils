package com.ezui.bpm.app.db.mapping.entity.fcfcore;

import java.io.Serializable;

public class AccountIntegrationBranch implements Serializable {
    private String deptName;

    private String ou;

    private static final long serialVersionUID = 1L;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou == null ? null : ou.trim();
    }
}