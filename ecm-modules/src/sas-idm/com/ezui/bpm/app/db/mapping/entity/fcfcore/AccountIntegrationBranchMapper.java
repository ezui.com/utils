package com.ezui.bpm.app.db.mapping.entity.fcfcore;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranch;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranchExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AccountIntegrationBranchMapper {
    long countByExample(AccountIntegrationBranchExample example);

    int deleteByExample(AccountIntegrationBranchExample example);

    int insert(AccountIntegrationBranch record);

    int insertSelective(AccountIntegrationBranch record);

    List<AccountIntegrationBranch> selectByExampleWithRowbounds(AccountIntegrationBranchExample example, RowBounds rowBounds);

    List<AccountIntegrationBranch> selectByExample(AccountIntegrationBranchExample example);

    int updateByExampleSelective(@Param("record") AccountIntegrationBranch record, @Param("example") AccountIntegrationBranchExample example);

    int updateByExample(@Param("record") AccountIntegrationBranch record, @Param("example") AccountIntegrationBranchExample example);
}