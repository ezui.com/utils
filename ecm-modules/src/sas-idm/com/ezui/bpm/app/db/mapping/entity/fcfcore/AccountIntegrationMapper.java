package com.ezui.bpm.app.db.mapping.entity.fcfcore;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegration;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AccountIntegrationMapper {
    long countByExample(AccountIntegrationExample example);

    int deleteByExample(AccountIntegrationExample example);

    int deleteByPrimaryKey(String empId);

    int insert(AccountIntegration record);

    int insertSelective(AccountIntegration record);

    List<AccountIntegration> selectByExampleWithRowbounds(AccountIntegrationExample example, RowBounds rowBounds);

    List<AccountIntegration> selectByExample(AccountIntegrationExample example);

    AccountIntegration selectByPrimaryKey(String empId);

    int updateByExampleSelective(@Param("record") AccountIntegration record, @Param("example") AccountIntegrationExample example);

    int updateByExample(@Param("record") AccountIntegration record, @Param("example") AccountIntegrationExample example);

    int updateByPrimaryKeySelective(AccountIntegration record);

    int updateByPrimaryKey(AccountIntegration record);
}