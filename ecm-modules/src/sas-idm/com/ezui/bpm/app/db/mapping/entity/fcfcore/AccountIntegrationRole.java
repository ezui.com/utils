package com.ezui.bpm.app.db.mapping.entity.fcfcore;

import java.io.Serializable;

public class AccountIntegrationRole implements Serializable {
    private String empId;

    private String role;

    private String roleDesc;

    private static final long serialVersionUID = 1L;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId == null ? null : empId.trim();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc == null ? null : roleDesc.trim();
    }
}