package com.ezui.bpm.app.db.mapping.entity.fcfcore;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationRole;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AccountIntegrationRoleMapper {
    long countByExample(AccountIntegrationRoleExample example);

    int deleteByExample(AccountIntegrationRoleExample example);

    int insert(AccountIntegrationRole record);

    int insertSelective(AccountIntegrationRole record);

    List<AccountIntegrationRole> selectByExampleWithRowbounds(AccountIntegrationRoleExample example, RowBounds rowBounds);

    List<AccountIntegrationRole> selectByExample(AccountIntegrationRoleExample example);

    int updateByExampleSelective(@Param("record") AccountIntegrationRole record, @Param("example") AccountIntegrationRoleExample example);

    int updateByExample(@Param("record") AccountIntegrationRole record, @Param("example") AccountIntegrationRoleExample example);
}