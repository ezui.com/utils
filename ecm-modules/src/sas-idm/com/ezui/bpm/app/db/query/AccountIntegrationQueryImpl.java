package com.ezui.bpm.app.db.query;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.UserQueryProperty;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

public class AccountIntegrationQueryImpl extends UserQueryImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3464972413598332962L;
	private String empId;

	private String deptName;
	private String deptNameLike;

	private String deptNo;
	private String deptNoLike;

	private String email;
	private String emailLike;

	private String empName;
	private String empNameLike;

	private String ou;
	private String ouLike;
	private String role;

	public AccountIntegrationQueryImpl() {
	}

	public AccountIntegrationQueryImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public AccountIntegrationQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	public UserQuery userId(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		this.empId = id;
		return this;
	}

	public UserQuery userFirstName(String firstName) {
		if (firstName == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided firstName is null");
		}
		this.firstName = firstName;
		this.empName = firstName;
		return this;
	}

	public UserQuery userFirstNameLike(String firstNameLike) {
		if (firstNameLike == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided firstNameLike is null");
		}
		this.firstNameLike = firstNameLike;
		this.empNameLike = firstNameLike;
		return this;
	}

	public UserQuery userLastName(String lastName) {
		if (lastName == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided lastName is null");
		}
		this.lastName = lastName;
		this.empName = lastName;
		return this;
	}

	public UserQuery userLastNameLike(String lastNameLike) {
		if (lastNameLike == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided lastNameLike is null");
		}
		this.lastNameLike = lastNameLike;
		this.empNameLike = lastNameLike;
		return this;
	}

	public UserQuery userFullNameLike(String fullNameLike) {
		if (fullNameLike == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided full name is null");
		}
		this.fullNameLike = fullNameLike;
		this.empNameLike = fullNameLike;
		return this;
	}

	public UserQuery userEmail(String email) {
		if (email == null) {
			throw new ActivitiIllegalArgumentException("Provided email is null");
		}
		this.email = email;
		return this;
	}

	public UserQuery userEmailLike(String emailLike) {
		if (emailLike == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided emailLike is null");
		}
		this.emailLike = emailLike;
		return this;
	}

	public UserQuery memberOfGroup(String groupId) {
		if (groupId == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided groupIds is null or empty");
		}
		this.groupId = groupId;
		this.role = groupId;
		return this;
	}

	public UserQuery potentialStarter(String procDefId) {
		if (procDefId == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided processDefinitionId is null or empty");
		}
		this.procDefId = procDefId;
		return this;

	}

	// sorting //////////////////////////////////////////////////////////
	public static final UserQueryProperty USER_ID = new UserQueryProperty(
			"RES.EMP_ID");
	public static final UserQueryProperty FIRST_NAME = new UserQueryProperty(
			"RES.EMP_NAME");
	public static final UserQueryProperty LAST_NAME = new UserQueryProperty(
			"RES.EMP_NAME");
	public static final UserQueryProperty EMAIL = new UserQueryProperty(
			"RES.EMAIL");

	public UserQuery orderByUserId() {
		return orderBy(USER_ID);
	}

	public UserQuery orderByUserEmail() {
		return orderBy(EMAIL);
	}

	public UserQuery orderByUserFirstName() {
		return orderBy(FIRST_NAME);
	}

	public UserQuery orderByUserLastName() {
		return orderBy(LAST_NAME);
	}

	public String getOrderBy() {
		if (orderBy == null) {
			return "RES.EMP_ID asc";
		} else {
			return orderBy;
		}
	}

	// results //////////////////////////////////////////////////////////

	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getUserEntityManager()
				.findUserCountByQueryCriteria(this);
	}

	public List<User> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getUserEntityManager().findUserByQueryCriteria(
				this, page);
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptNameLike() {
		return deptNameLike;
	}

	public void setDeptNameLike(String deptNameLike) {
		this.deptNameLike = deptNameLike;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptNoLike() {
		return deptNoLike;
	}

	public void setDeptNoLike(String deptNoLike) {
		this.deptNoLike = deptNoLike;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailLike() {
		return emailLike;
	}

	public void setEmailLike(String emailLike) {
		this.emailLike = emailLike;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpNameLike() {
		return empNameLike;
	}

	public void setEmpNameLike(String empNameLike) {
		this.empNameLike = empNameLike;
	}

	public String getOu() {
		return ou;
	}

	public void setOu(String ou) {
		this.ou = ou;
	}

	public String getOuLike() {
		return ouLike;
	}

	public void setOuLike(String ouLike) {
		this.ouLike = ouLike;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
