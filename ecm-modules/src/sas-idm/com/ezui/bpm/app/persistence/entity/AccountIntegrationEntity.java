package com.ezui.bpm.app.persistence.entity;

import org.activiti.engine.impl.persistence.entity.UserEntity;

public interface AccountIntegrationEntity extends UserEntity {
	public String getEmpId();

	public void setEmpId(String empId);

	public String getDeptName();

	public void setDeptName(String deptName);

	public String getDeptNo();

	public void setDeptNo(String deptNo);

	public String getEmail();

	public void setEmail(String email);

	public String getEmpName();

	public void setEmpName(String empName);

	public String getEncrypw();

	public void setEncrypw(String encrypw);

	public String getOu();

	public void setOu(String ou);
}
