package com.ezui.bpm.app.persistence.entity;

import org.activiti.engine.impl.persistence.entity.GroupEntity;

public interface AccountIntegrationRoleEntity extends GroupEntity {
}
