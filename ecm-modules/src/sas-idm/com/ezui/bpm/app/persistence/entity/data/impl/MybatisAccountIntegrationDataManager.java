package com.ezui.bpm.app.persistence.entity.data.impl;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.data.impl.MybatisUserDataManager;

import com.ezui.bpm.app.persistence.entity.AccountIntegrationEntity;
import com.ezui.bpm.app.persistence.entity.AccountIntegrationEntityImpl;
import com.ezui.bpm.app.persistence.entity.data.AccountIntegrationDataManager;

public class MybatisAccountIntegrationDataManager extends
		MybatisUserDataManager implements AccountIntegrationDataManager {

	public MybatisAccountIntegrationDataManager(
			ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends AccountIntegrationEntity> getManagedEntityClass() {
		return AccountIntegrationEntityImpl.class;
	}

	@Override
	public AccountIntegrationEntity create() {
		return new AccountIntegrationEntityImpl();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUserByQueryCriteria(
			UserQueryImpl query, Page page) {
		return getDbSqlSession().selectList(
				"selectAccountIntegrationByQueryCriteria", query, page);
	}
	@Override
	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		return (Long) getDbSqlSession().selectOne(
				"selectAccountIntegrationCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	public List<Group> findGroupsByUser(String userId) {
		return getDbSqlSession().selectList("selectRolesByEmpId", userId);
	}

	@SuppressWarnings("unchecked")
	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap,
			int firstResult, int maxResults) {
		return getDbSqlSession().selectListWithRawParameter(
				"selectAccountIntegrationByNativeQuery", parameterMap,
				firstResult, maxResults);
	}

	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		return (Long) getDbSqlSession().selectOne(
				"selectAccountIntegrationCountByNativeQuery", parameterMap);
	}
}
