package com.ezui.bpm.app.persistence.entity.data.impl;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.data.impl.MybatisGroupDataManager;

import com.ezui.bpm.app.persistence.entity.AccountIntegrationRoleEntityImpl;
import com.ezui.bpm.app.persistence.entity.data.AccountIntegrationRoleDataManager;

public class MybatisAccountIntegrationRoleDataManager extends
		MybatisGroupDataManager implements AccountIntegrationRoleDataManager {

	public MybatisAccountIntegrationRoleDataManager(
			ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends GroupEntity> getManagedEntityClass() {
		return AccountIntegrationRoleEntityImpl.class;
	}

	@Override
	public GroupEntity create() {
		return new AccountIntegrationRoleEntityImpl();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		return getDbSqlSession().selectList("selectAccountIntegrationRoleByQueryCriteria",
				query, page);
	}

	@Override
	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		return (Long) getDbSqlSession().selectOne(
				"selectAccountIntegrationRoleCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupsByUser(String userId) {
		return getDbSqlSession().selectList("selectAccountIntegrationRolesByUserId", userId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupsByNativeQuery(
			Map<String, Object> parameterMap, int firstResult, int maxResults) {
		return getDbSqlSession().selectListWithRawParameter(
				"selectAccountIntegrationRoleByNativeQuery", parameterMap, firstResult,
				maxResults);
	}

	@Override
	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		return (Long) getDbSqlSession().selectOne(
				"selectAccountIntegrationRoleCountByNativeQuery", parameterMap);
	}

}
