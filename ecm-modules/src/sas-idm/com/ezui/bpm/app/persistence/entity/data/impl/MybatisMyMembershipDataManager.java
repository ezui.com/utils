package com.ezui.bpm.app.persistence.entity.data.impl;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.MembershipEntity;
import org.activiti.engine.impl.persistence.entity.MembershipEntityImpl;
import org.activiti.engine.impl.persistence.entity.data.impl.MybatisMembershipDataManager;

import com.ezui.bpm.app.persistence.entity.MyMembershipEntityImpl;
import com.ezui.bpm.app.persistence.entity.data.MyMembershipDataManager;

public class MybatisMyMembershipDataManager extends
		MybatisMembershipDataManager implements MyMembershipDataManager {

	public MybatisMyMembershipDataManager(
			ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends MembershipEntity> getManagedEntityClass() {
		return MyMembershipEntityImpl.class;
	}

	@Override
	public MembershipEntity create() {
		return new MyMembershipEntityImpl();
	}

	@Override
	public void deleteMembership(String userId, String groupId) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("userId", userId);
		parameters.put("groupId", groupId);
		getDbSqlSession().delete("deleteMyMembership", parameters,
				MembershipEntityImpl.class);
	}

	@Override
	public void deleteMembershipByGroupId(String groupId) {
		getDbSqlSession().delete("deleteMyMembershipsByGroupId", groupId,
				MembershipEntityImpl.class);
	}

	@Override
	public void deleteMembershipByUserId(String userId) {
		getDbSqlSession().delete("deleteMyMembershipsByUserId", userId,
				MembershipEntityImpl.class);
	}
}
