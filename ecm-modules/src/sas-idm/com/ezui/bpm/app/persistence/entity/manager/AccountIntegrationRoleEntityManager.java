package com.ezui.bpm.app.persistence.entity.manager;

import org.activiti.engine.impl.persistence.entity.GroupEntityManager;

public interface AccountIntegrationRoleEntityManager extends GroupEntityManager {

}
