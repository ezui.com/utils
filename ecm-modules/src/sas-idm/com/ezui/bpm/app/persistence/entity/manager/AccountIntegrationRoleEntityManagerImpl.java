package com.ezui.bpm.app.persistence.entity.manager;

import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.GroupEntityManagerImpl;
import org.activiti.engine.impl.persistence.entity.data.DataManager;
import org.activiti.engine.impl.persistence.entity.data.GroupDataManager;

import com.ezui.bpm.app.db.query.AccountIntegrationRoleQueryImpl;
import com.ezui.bpm.app.persistence.entity.data.AccountIntegrationRoleDataManager;

public class AccountIntegrationRoleEntityManagerImpl extends
		GroupEntityManagerImpl implements AccountIntegrationRoleEntityManager {

	public AccountIntegrationRoleEntityManagerImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration,
			AccountIntegrationRoleDataManager accountIntegrationRoleDataManager) {
		super(processEngineConfiguration, accountIntegrationRoleDataManager);
		this.groupDataManager = accountIntegrationRoleDataManager;
	}

	@Override
	protected DataManager<GroupEntity> getDataManager() {
		return groupDataManager;
	}

	@Override
	public Group createNewGroup(String groupId) {
		GroupEntity groupEntity = groupDataManager.create();
		groupEntity.setId(groupId);
		groupEntity.setRevision(0); // Needed as groups can be transient and not
									// save when they are returned
		return groupEntity;
	}

	@Override
	public void delete(String groupId) {
		GroupEntity group = groupDataManager.findById(groupId);

		if (group != null) {

			getMembershipEntityManager().deleteMembershipByGroupId(groupId);
			if (getEventDispatcher().isEnabled()) {
				getEventDispatcher().dispatchEvent(
						ActivitiEventBuilder.createMembershipEvent(
								ActivitiEventType.MEMBERSHIPS_DELETED, groupId,
								null));
			}

			delete(group);
		}
	}

	@Override
	public GroupQuery createNewGroupQuery() {
		return new AccountIntegrationRoleQueryImpl(getCommandExecutor());
	}

	@Override
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		return groupDataManager.findGroupByQueryCriteria(query, page);
	}

	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		return groupDataManager.findGroupCountByQueryCriteria(query);
	}

	public List<Group> findGroupsByUser(String userId) {
		return groupDataManager.findGroupsByUser(userId);
	}

	public List<Group> findGroupsByNativeQuery(
			Map<String, Object> parameterMap, int firstResult, int maxResults) {
		return groupDataManager.findGroupsByNativeQuery(parameterMap,
				firstResult, maxResults);
	}

	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		return groupDataManager.findGroupCountByNativeQuery(parameterMap);
	}

	@Override
	public boolean isNewGroup(Group group) {
		return ((GroupEntity) group).getRevision() == 0;
	}

	public GroupDataManager getGroupDataManager() {
		return groupDataManager;
	}

	public void setGroupDataManager(GroupDataManager groupDataManager) {
		this.groupDataManager = groupDataManager;
	}
}
