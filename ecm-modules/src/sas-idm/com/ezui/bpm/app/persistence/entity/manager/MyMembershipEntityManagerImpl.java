package com.ezui.bpm.app.persistence.entity.manager;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.MembershipEntity;
import org.activiti.engine.impl.persistence.entity.MembershipEntityManagerImpl;
import org.activiti.engine.impl.persistence.entity.data.DataManager;
import org.activiti.engine.impl.persistence.entity.data.MembershipDataManager;

import com.ezui.bpm.app.persistence.entity.data.MyMembershipDataManager;

public class MyMembershipEntityManagerImpl extends MembershipEntityManagerImpl
		implements MyMembershipEntityManager {

	public MyMembershipEntityManagerImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration,
			MyMembershipDataManager membershipDataManager) {
		super(processEngineConfiguration, membershipDataManager);
	}

	@Override
	protected DataManager<MembershipEntity> getDataManager() {
		return membershipDataManager;
	}

	public void createMembership(String userId, String groupId) {
		MembershipEntity membershipEntity = create();
		membershipEntity.setUserId(userId);
		membershipEntity.setGroupId(groupId);
		insert(membershipEntity, false);

		if (getEventDispatcher().isEnabled()) {
			getEventDispatcher().dispatchEvent(
					ActivitiEventBuilder.createMembershipEvent(
							ActivitiEventType.MEMBERSHIP_CREATED, groupId,
							userId));
		}
	}

	public void deleteMembership(String userId, String groupId) {
		membershipDataManager.deleteMembership(userId, groupId);
		if (getEventDispatcher().isEnabled()) {
			getEventDispatcher().dispatchEvent(
					ActivitiEventBuilder.createMembershipEvent(
							ActivitiEventType.MEMBERSHIP_DELETED, groupId,
							userId));
		}
	}

	@Override
	public void deleteMembershipByGroupId(String groupId) {
		membershipDataManager.deleteMembershipByGroupId(groupId);
	}

	@Override
	public void deleteMembershipByUserId(String userId) {
		membershipDataManager.deleteMembershipByUserId(userId);
	}

	public MembershipDataManager getMembershipDataManager() {
		return membershipDataManager;
	}

	public void setMembershipDataManager(
			MembershipDataManager membershipDataManager) {
		this.membershipDataManager = membershipDataManager;
	}

}
