package com.ezui.bpm.app.service.runtime.fcfcore;

import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranch;

public interface AccountIntegrationBranchService {
	public List<AccountIntegrationBranch> selectByExampleWithRowbounds(
			int start, int pagesize, String sort);
	public long countByExample();
}
