package com.ezui.bpm.app.service.runtime.fcfcore;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.ezui.bpm.app.db.dao.fcfcore.AccountIntegrationBranchDao;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationBranch;

@Service
public class AccountIntegrationBranchServiceImpl implements AccountIntegrationBranchService{
	@Inject
	private AccountIntegrationBranchDao accountIntegrationBranchDao;

	public List<AccountIntegrationBranch> selectByExampleWithRowbounds(
			int start, int pagesize, String sort) {
		return accountIntegrationBranchDao.selectByExampleWithRowbounds(start,
				pagesize, sort);
	}

	public long countByExample() {
		return accountIntegrationBranchDao.countByExample();
	}
}
