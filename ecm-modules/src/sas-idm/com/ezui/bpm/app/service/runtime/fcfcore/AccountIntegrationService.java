package com.ezui.bpm.app.service.runtime.fcfcore;

import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegration;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationExample;

public interface AccountIntegrationService {
	public List<AccountIntegration> selectByExampleWithRowbounds(
			AccountIntegrationExample query, int start, int pagesize,
			String sort);

	public long countByExample(AccountIntegrationExample query);
}
