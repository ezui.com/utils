package com.ezui.bpm.app.service.runtime.fcfcore;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.ezui.bpm.app.db.dao.fcfcore.AccountIntegrationDao;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegration;
import com.ezui.bpm.app.db.mapping.entity.fcfcore.AccountIntegrationExample;

@Service
public class AccountIntegrationServiceImpl implements AccountIntegrationService {
	@Inject
	private AccountIntegrationDao accountIntegrationDao;

	public List<AccountIntegration> selectByExampleWithRowbounds(
			AccountIntegrationExample query, int start, int pagesize,
			String sort) {
		return accountIntegrationDao.selectByExampleWithRowbounds(query, start,
				pagesize, sort);
	}

	public long countByExample(AccountIntegrationExample query) {
		return accountIntegrationDao.countByExample(query);
	}
}
