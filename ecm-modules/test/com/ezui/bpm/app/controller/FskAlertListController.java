package com.ezui.bpm.app.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ezui.bpm.app.mode.FskAlertPresentation;
import com.ezui.bpm.app.persistence.entity.FskAlertScenarioEntityImpl;
import com.ezui.bpm.app.service.SasamlService;

@Controller
@RequestMapping(value = "/sasaml/fskAlerts")
public class FskAlertListController {
	@Inject
	private SasamlService sasamlService;

	@RequestMapping
	public String list(
			@RequestParam(value = "rows", defaultValue = "10", required = false) int rows,
			@RequestParam(value = "page", defaultValue = "1", required = false) int page,
			@ModelAttribute("fskAlert") FskAlertPresentation fskAlertPresentation,
			ModelMap model) throws Exception {
		if (page < 1) {
			page = 1;
		}
		if (rows < 1) {
			rows = 10;
		}
		int offset = (page - 1) * rows;

		List<FskAlertScenarioEntityImpl> cls = sasamlService.getFskAlertScenarios(offset, rows);
		fskAlertPresentation.setFskAlertScenarios(cls);
		model.addAttribute("fskAlert", fskAlertPresentation);
		return "sasaml/fskAlerts";
	}

}
