package com.ezui.bpm.app.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ezui.bpm.app.db.mapping.entity.fcfkc.FskScenario;
import com.ezui.bpm.app.mode.FskScenarioPresentation;
import com.ezui.bpm.app.service.SasamlService;

@Controller
@RequestMapping(value = "/sasaml/fskScenarios")
public class FskScenarioController {
	@Inject
	private SasamlService sasamlService;

	@RequestMapping
	public String list(
			@RequestParam(value = "rows", defaultValue = "10", required = false) int rows,
			@RequestParam(value = "page", defaultValue = "1", required = false) int page,
			@ModelAttribute("fskScenario") FskScenarioPresentation fskScenarioPresentation,
			ModelMap model) throws Exception {
		if (page < 1) {
			page = 1;
		}
		if (rows < 1) {
			rows = 10;
		}
		int offset = (page - 1) * rows;

		List<FskScenario> cls = sasamlService.getFskScenarios(offset, rows);
		fskScenarioPresentation.setFskScenarios(cls);
		model.addAttribute("fskScenario", fskScenarioPresentation);
		return "sasaml/fskScenarios";
	}

}
