package com.ezui.bpm.app.mode;

import java.util.ArrayList;
import java.util.List;

import com.ezui.bpm.app.persistence.entity.FskAlertScenarioEntityImpl;

public class FskAlertPresentation {
	private FskAlertScenarioEntityImpl fskAlertScenario = new FskAlertScenarioEntityImpl();
	private List<FskAlertScenarioEntityImpl> fskAlertScenarios = new ArrayList<FskAlertScenarioEntityImpl>();
	public FskAlertScenarioEntityImpl getFskAlertScenario() {
		return fskAlertScenario;
	}
	public void setFskAlertScenario(FskAlertScenarioEntityImpl fskAlertScenario) {
		this.fskAlertScenario = fskAlertScenario;
	}
	public List<FskAlertScenarioEntityImpl> getFskAlertScenarios() {
		return fskAlertScenarios;
	}
	public void setFskAlertScenarios(
			List<FskAlertScenarioEntityImpl> fskAlertScenarios) {
		this.fskAlertScenarios = fskAlertScenarios;
	}

}
