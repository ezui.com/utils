package com.ezui.bpm.app.mode;

import java.util.ArrayList;
import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.fcfkc.FskScenario;

public class FskScenarioPresentation {
	private FskScenario fskScenario = new FskScenario();
	private List<FskScenario> fskScenarios = new ArrayList<FskScenario>();

	public FskScenario getFskScenario() {
		return fskScenario;
	}

	public void setFskScenario(FskScenario fskScenario) {
		this.fskScenario = fskScenario;
	}

	public List<FskScenario> getFskScenarios() {
		return fskScenarios;
	}

	public void setFskScenarios(List<FskScenario> fskScenarios) {
		this.fskScenarios = fskScenarios;
	}

}
