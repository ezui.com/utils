package com.ezui.bpm.app.mode;

import java.util.ArrayList;
import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.ecm.IncidentLive;

public class IncidentPresentation {
	private IncidentLive incidentLive = new IncidentLive();
	private List<IncidentLive> incidentLives = new ArrayList<IncidentLive>();
	public IncidentLive getIncidentLive() {
		return incidentLive;
	}
	public void setIncidentLive(IncidentLive incidentLive) {
		this.incidentLive = incidentLive;
	}
	public List<IncidentLive> getIncidentLives() {
		return incidentLives;
	}
	public void setIncidentLives(List<IncidentLive> incidentLives) {
		this.incidentLives = incidentLives;
	}
	
}
