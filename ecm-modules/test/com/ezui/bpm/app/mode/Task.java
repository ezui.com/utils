package com.ezui.bpm.app.mode;

import java.util.ArrayList;
import java.util.List;

import org.activiti.app.domain.runtime.RelatedContent;
import org.activiti.engine.history.HistoricActivityInstance;
import org.springframework.data.domain.Page;

import com.ezui.bpm.app.db.mapping.entity.ecm.CaseLive;
import com.ezui.bpm.app.db.mapping.entity.ncsc.NameCheckRecordDetail;
import com.ezui.bpm.app.db.mapping.entity.ncsc.NameCheckRecordMain;
import com.ezui.bpm.app.db.mapping.entity.ncsc.NameHitRecord;

public class Task {
	private String processInstanceId;
	private String taskId;
	private CaseLive caseLive = new CaseLive();
	private String actionType;
	private List<HistoricActivityInstance> historys=new ArrayList<HistoricActivityInstance>();
	private String starter=null;
	private NameCheckRecordMain nameCheckRecordMain=new NameCheckRecordMain();
	private List<NameCheckRecordDetail> nameCheckRecordDetails=new ArrayList<NameCheckRecordDetail>();
	private List<NameHitRecord> nameHitRecords=new ArrayList<NameHitRecord>();
	private Page<RelatedContent> relatedContents;

	public CaseLive getCaseLive() {
		return caseLive;
	}

	public void setCaseLive(CaseLive caseLive) {
		this.caseLive = caseLive;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public List<HistoricActivityInstance> getHistorys() {
		return historys;
	}

	public void setHistorys(List<HistoricActivityInstance> historys) {
		this.historys = historys;
	}

	public String getStarter() {
		return starter;
	}

	public void setStarter(String starter) {
		this.starter = starter;
	}

	public NameCheckRecordMain getNameCheckRecordMain() {
		return nameCheckRecordMain;
	}

	public void setNameCheckRecordMain(NameCheckRecordMain nameCheckRecordMain) {
		this.nameCheckRecordMain = nameCheckRecordMain;
	}

	public List<NameCheckRecordDetail> getNameCheckRecordDetails() {
		return nameCheckRecordDetails;
	}

	public void setNameCheckRecordDetails(
			List<NameCheckRecordDetail> nameCheckRecordDetails) {
		this.nameCheckRecordDetails = nameCheckRecordDetails;
	}

	public List<NameHitRecord> getNameHitRecords() {
		return nameHitRecords;
	}

	public void setNameHitRecords(List<NameHitRecord> nameHitRecords) {
		this.nameHitRecords = nameHitRecords;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Page<RelatedContent> getRelatedContents() {
		return relatedContents;
	}

	public void setRelatedContents(Page<RelatedContent> relatedContents) {
		this.relatedContents = relatedContents;
	}

}
