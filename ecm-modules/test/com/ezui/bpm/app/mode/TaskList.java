package com.ezui.bpm.app.mode;

import java.util.ArrayList;
import java.util.List;

import com.ezui.bpm.app.db.mapping.entity.ecm.CaseLive;

public class TaskList {
	private List<CaseLive> tasks=new ArrayList<CaseLive>();

	public List<CaseLive> getTasks() {
		return tasks;
	}

	public void setTasks(List<CaseLive> tasks) {
		this.tasks = tasks;
	}
	
}
