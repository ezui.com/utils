package com.ezui.bpm.app.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.app.domain.runtime.RelatedContent;
import org.activiti.app.model.component.SimpleContentTypeMapper;
import org.activiti.app.model.runtime.RelatedContentRepresentation;
import org.activiti.app.security.SecurityUtils;
import org.activiti.app.service.exception.BadRequestException;
import org.activiti.app.service.exception.NotPermittedException;
import org.activiti.app.service.runtime.PermissionService;
import org.activiti.app.service.runtime.RelatedContentService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ezui.bpm.app.db.mapping.entity.ecm.CaseLive;
import com.ezui.bpm.app.db.mapping.entity.ncsc.NameCheckRecordDetail;
import com.ezui.bpm.app.db.mapping.entity.ncsc.NameCheckRecordMain;
import com.ezui.bpm.app.db.mapping.entity.ncsc.NameHitRecord;
import com.ezui.bpm.app.mode.Task;
import com.ezui.bpm.app.rest.object.CaseLiveResourceBody;
import com.ezui.bpm.app.service.SasamlService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
//@RequestMapping(value = "/rest/sasaml/caselives")
@RequestMapping(value = "/rest/sasaml")
public class CaseLiveResource {
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private SasamlService sasamlService;
	@Autowired
    protected TaskService taskService;
	@Autowired
    protected PermissionService permissionService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
    protected RelatedContentService contentService;
    @Autowired
    protected SimpleContentTypeMapper simpleTypeMapper;
	@Autowired
	private HistoryService historyService;
//	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, params = {
//			"rows", "page" })
//	public List<CaseLive> getGroups(
//			@RequestParam(value = "rows", defaultValue = "10", required = false) int rows,
//			@RequestParam(value = "page", defaultValue = "1", required = false) int page) {
//		if (page < 1) {
//			page = 1;
//		}
//		if (rows < 1) {
//			rows = 10;
//		}
//		int offset = (page - 1) * rows;
//
//		return sasamlService.getCaseLives(offset, rows);
//	}

	// @RequestMapping(value = "/{casePk}", method = RequestMethod.GET)
	// public CaseLive getCaseLive(@PathVariable String casePk) {
	// validateAdminRole();
	// return new
	// GroupRepresentation(identityService.createGroupQuery().groupId(groupId).singleResult());
	// }
	
	
	@RequestMapping(value = "caselives", method = RequestMethod.POST, produces = { "application/json" })
	public List<CaseLive> getGroups(@RequestBody CaseLiveResourceBody body) {
		int page = body.getPage();
		int rows = body.getRows();
		if (page < 1) {
			page = 1;
		}
		if (rows < 1) {
			rows = 10;
		}
		int offset = (page - 1) * rows;
		List<CaseLive> l  = sasamlService.getCaseLives(offset, rows);
		return l;
	}
	
	
	
	
	@RequestMapping(value = "/upload/{taskId}", method = RequestMethod.POST)
	public String upload(@PathVariable String taskId, @RequestParam("file") MultipartFile file,
			@RequestParam("actionType") String actionType) throws Exception {
		String caseRk=null;
		if ("upload".equals(actionType) && taskId != null) {
			org.activiti.engine.task.Task t=taskService.createTaskQuery().taskId(taskId).singleResult();
			String processInstanceId =t.getProcessInstanceId();
			ProcessInstance pi=runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
			caseRk=pi.getBusinessKey();
			 User user = SecurityUtils.getCurrentUserObject();
		        
		        if(!permissionService.canAddRelatedContentToTask(user, taskId)) {
		            throw new NotPermittedException("You are not allowed to read the process with id: " + taskId);
		        }
		        if (file != null && file.getName() != null) {
		            try {
		                String contentType = file.getContentType();
		                
		                // temp additional content type check for IE9 flash uploads
		                if (StringUtils.equals(file.getContentType(), "application/octet-stream")) {
		                    contentType = getContentTypeForFileExtension(file);
		                }
		                
		                RelatedContent relatedContent = contentService.createRelatedContent(user, getFileName(file), null, null, taskId, null, 
		                        contentType, file.getInputStream(), file.getSize(), true, false);
		                new RelatedContentRepresentation(relatedContent, simpleTypeMapper);
		            } catch (IOException e) {
		                throw new BadRequestException("Error while reading file data", e);
		            }
		        } else {
		            throw new BadRequestException("File to upload is missing");
		        }
		}
		return "success";
	}
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/caselives/{caseRk}", method = RequestMethod.POST, produces = { "application/json" })
	public Task detail(@PathVariable Long caseRk, @RequestBody CaseLiveResourceBody body) throws Exception {
		Task task = body.getTask();
		String processInstanceId = null;
		ProcessInstance pi=runtimeService.createProcessInstanceQuery().processDefinitionKey("financeNameCheckProcess")
		.processInstanceBusinessKey(caseRk.toString()).singleResult();
		String starter=null;
		if (pi != null) {
			processInstanceId = pi.getProcessInstanceId();
			starter=pi.getStartUserId();
			task.setStarter(starter);
		}
//		Execution exe = runtimeService.createExecutionQuery()
//				.processDefinitionKey("financeNameCheckProcess")
//				.processInstanceBusinessKey(caseRk.toString()).singleResult();
//		if (exe != null) {
//			processInstanceId = exe.getProcessInstanceId();
//		}
//		List<HistoricProcessInstance> hpi = historyService
//				.createHistoricProcessInstanceQuery()
//				.processInstanceBusinessKey(caseRk.toString()).notDeleted()
//				.processDefinitionKey("financeNameCheckProcess")
//				.orderByProcessInstanceId().desc().list();
//		if (hpi != null && hpi.size() > 0) {
//			processInstanceId = hpi.get(0).getId();
//		}
//		List<HistoricVariableInstance> hvi = historyService
//				.createHistoricVariableInstanceQuery()
//				.variableValueEquals("caseRk", caseRk).list();
//		if (hvi != null && hvi.size() > 0) {
//			processInstanceId = hvi.get(0).getProcessInstanceId();
//		}
		System.out.println(task.getActionType());
		if ("啟動".equals(task.getActionType()) && processInstanceId == null) {
			Map<String, Object> vab = new HashMap<String, Object>();
			vab.put("caseRk", caseRk);
			ProcessInstance processInstance = runtimeService
					.startProcessInstanceByKey("financeNameCheckProcess",
							caseRk.toString(), vab);
			processInstanceId = processInstance.getProcessInstanceId();
		}
		String taskId=null;
		if (processInstanceId != null) {
			task.setProcessInstanceId(processInstanceId);
			List<org.activiti.engine.task.Task> tasks=taskService.createTaskQuery().processInstanceId(processInstanceId).list();
			if(tasks!=null&&tasks.size()>0){
				taskId=tasks.get(0).getId();
				task.setTaskId(taskId);
			}
			// List<HistoricDetail>
			// hils1=historyService.createHistoricDetailQuery().processInstanceId(processInstanceId).listPage(0,
			// 10);
			List<HistoricActivityInstance> historys = historyService
					.createHistoricActivityInstanceQuery()
					.processInstanceId(processInstanceId).orderByHistoricActivityInstanceId().desc().list();
			task.setHistorys(historys);
			// List<HistoricProcessInstance>
			// hpi=historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).list();
			// List<HistoricTaskInstance>
			// hti=historyService.createHistoricTaskInstanceQuery().processInstanceId(processInstanceId).list();
			System.out.println("ok");
		}
		CaseLive caseLive = sasamlService.getByCaseRk(caseRk);
//		List<NameCheckRecordMain> ncrms=sasamlService.getnCRMainDataManager().findNameCheckRecordMainByQueryCriteria(null, 1, 10);
		NameCheckRecordMain ncrm=sasamlService.getNCRMByCaseRk(caseRk);
		String uniqueKey=ncrm.getUniqueKey();
		Integer ncReferenceId=ncrm.getNcReferenceId();
		List<NameCheckRecordDetail> ncrds=sasamlService.getNameCheckRecordDetailByPK(uniqueKey, ncReferenceId);
		List<NameHitRecord> nhrs=sasamlService.getNameHitRecordByPK(uniqueKey, ncReferenceId);
		Page<RelatedContent> rcs=contentService.getRelatedContentForTask(taskId, MAX_CONTENT_ITEMS, 0);
		task.setRelatedContents(rcs);
		task.setNameHitRecords(nhrs);
		task.setNameCheckRecordDetails(ncrds);
		task.setNameCheckRecordMain(ncrm);
		task.setCaseLive(caseLive);
//		model.addAttribute("task", task);
//		return "sasaml/tasks/detail";
		return task;
	}
	
	
	
	
	
	
	
	
	private static final int MAX_CONTENT_ITEMS = 50;
	protected String getContentTypeForFileExtension(MultipartFile file) {
        
    	String fileName = file.getOriginalFilename();
        String contentType = null;
        if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg")) {
            contentType = "image/jpeg";
        } else if (fileName.endsWith("gif")) {
            contentType = "image/gif";
        } else if (fileName.endsWith("png")) {
            contentType = "image/png";
        } else if (fileName.endsWith("bmp")) {
            contentType = "image/bmp";
        } else if (fileName.endsWith("tif") || fileName.endsWith(".tiff")) {
            contentType = "image/tiff";
        } else if (fileName.endsWith("png")) {
            contentType = "image/png";
        } else if (fileName.endsWith("doc")) {
            contentType = "application/msword";
        } else if (fileName.endsWith("docx")) {
            contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        } else if (fileName.endsWith("docm")) {
            contentType = "application/vnd.ms-word.document.macroenabled.12";
        } else if (fileName.endsWith("dotm")) {
            contentType = "application/vnd.ms-word.template.macroenabled.12";
        } else if (fileName.endsWith("odt")) {
            contentType = "application/vnd.oasis.opendocument.text";
        } else if (fileName.endsWith("ott")) {
            contentType = "application/vnd.oasis.opendocument.text-template";
        } else if (fileName.endsWith("rtf")) {
            contentType = "application/rtf";
        } else if (fileName.endsWith("txt")) {
            contentType = "application/text";
        } else if (fileName.endsWith("xls")) {
            contentType = "application/vnd.ms-excel";
        } else if (fileName.endsWith("xlsx")) {
            contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        } else if (fileName.endsWith("xlsb")) {
            contentType = "application/vnd.ms-excel.sheet.binary.macroenabled.12";
        } else if (fileName.endsWith("xltx")) {
            contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
        } else if (fileName.endsWith("ods")) {
            contentType = "application/vnd.oasis.opendocument.spreadsheet";
        } else if (fileName.endsWith("ppt")) {
            contentType = "application/vnd.ms-powerpoint";
        } else if (fileName.endsWith("pptx")) {
            contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        } else if (fileName.endsWith("ppsm")) {
            contentType = "application/vnd.ms-powerpoint.slideshow.macroenabled.12";
        } else if (fileName.endsWith("ppsx")) {
            contentType = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
        } else if (fileName.endsWith("odp")) {
            contentType = "application/vnd.oasis.opendocument.presentation";
        } else {
        	// We've done what we could
        	return file.getContentType();
        }
        
        return contentType;
    }
	protected String getFileName(MultipartFile file) {
        return file.getOriginalFilename() != null ? file.getOriginalFilename() : "Nameless file";
    }
}
