package com.ezui.bpm.app.rest.object;

import com.ezui.bpm.app.mode.Task;

public class CaseLiveResourceBody {
	private int page;
	private int rows;
	private Task task = new Task();
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}
	
}
