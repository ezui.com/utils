package com.ezui.bpm.app.test;

import java.io.InputStream;

import org.activiti.engine.DynamicBpmnService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.junit.Rule;
import org.junit.Test;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Created by Pardo David on 28/11/2016.
 */
public class DynamicBpmnServiceTest2 {

	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Test
	public void itShouldBePossibleToUpdateTheCandidateUser(){
		InputStream resourceAsStream = this.getClass().getResourceAsStream("/dynamic-bpmn-test-process.bpmn20.xml");
		RepositoryService repositoryService = activitiRule.getRepositoryService();
		Deployment deploy = repositoryService.createDeployment()
				.addInputStream("dynamic-bpmn-test-process.bpmn20.xml", resourceAsStream)
				.deploy();


		RuntimeService runtimeService = activitiRule.getRuntimeService();
		String processDefinitionId = repositoryService.createProcessDefinitionQuery().processDefinitionKey("dynamicServiceTest").latestVersion().active().singleResult().getId();

		IdentityService identityService = activitiRule.getIdentityService();
		User david = identityService.newUser("david");
		identityService.saveUser(david);

		runtimeService.startProcessInstanceById(processDefinitionId);
		TaskService taskService = activitiRule.getTaskService();
		Task task = taskService.createTaskQuery().taskCandidateUser("david").singleResult();
		//assertNotNull("",task);

		DynamicBpmnService dynamicBpmnService = activitiRule.getProcessEngine().getDynamicBpmnService();
		ObjectNode processInfo = dynamicBpmnService.changeUserTaskCandidateUser("sid-B94D5D22-E93E-4401-ADC5-C5C073E1EEB4", "bob", true);
		dynamicBpmnService.saveProcessDefinitionInfo(processDefinitionId,processInfo);

		ObjectNode infoNode = dynamicBpmnService.getProcessDefinitionInfo(processDefinitionId);
		//assertThat(infoNode,is(not(nullValue())));


		runtimeService.startProcessInstanceById(processDefinitionId);
		task = taskService.createTaskQuery().taskCandidateUser("bob").singleResult();
		//assertThat(task,is(not(nullValue())));
	}


}