package com.ezui.bpm.app.test;

import static org.junit.Assert.*;

import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;

public class MyBusinessProcess implements Runnable {

	private ActivitiRule rule;

	public MyBusinessProcess() {

	}

	@Override
	public void run() {
		IdentityService identityService = rule.getIdentityService();
		List<String> userinfo = identityService.getUserInfoKeys("admin");

		RuntimeService runtimeService = rule.getRuntimeService();
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceByKey("financeNameCheckProcess");
		String processInstanceId = processInstance.getId();

		TaskService taskService = rule.getTaskService();
		Task task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("分行經辦", task.getName());
		taskService.claim(task.getId(), "admin");
		//DelegateExecution exe=(DelegateExecution)runtimeService.createExecutionQuery().executionId(task.getExecutionId()).singleResult();
		//exe.getVariable(variableName)
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("分行乙級主管審核", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("分行甲級主管審核", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("法遵洗防經辦", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("洗防乙級主管/洗防副主任", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("洗防甲級主管/洗防主任", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("法遵長", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(0, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

	}

	public ActivitiRule getRule() {
		return rule;
	}

	public void setRule(ActivitiRule rule) {
		this.rule = rule;
	}

}
