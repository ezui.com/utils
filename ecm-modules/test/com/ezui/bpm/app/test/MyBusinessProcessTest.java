package com.ezui.bpm.app.test;

import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Rule;
import org.junit.Test;

public class MyBusinessProcessTest {
	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Test
	@Deployment
	public void namecheck() {
		for (int i = 0; i < 1; i++) {
			long begin = System.nanoTime();
			MyBusinessProcess t = new MyBusinessProcess();
			t.setRule(activitiRule);
			new Thread(t).start();
			System.out.println("[" + i + "]:" + (System.nanoTime() - begin));
		}
		try {
			Thread.sleep(1000 * 100);
		} catch (Exception e) {
		}
	}

}
