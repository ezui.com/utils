package com.ezui.bpm.app.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.junit.Rule;
import org.junit.Test;

public class NameCheckProcessTest {
	@Rule
	public ActivitiRule rule = new ActivitiRule();

	@Test
	public void nameCheckProcessV2(){
		IdentityService identityService = rule.getIdentityService();
		List<String> userinfo = identityService.getUserInfoKeys("admin");

		RuntimeService runtimeService = rule.getRuntimeService();
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceByKey("financeNameCheckProcess");
		String processInstanceId = processInstance.getId();

		TaskService taskService = rule.getTaskService();
		Task task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("分行經辦", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("分行乙級主管審核", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(1, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

		task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		assertEquals("分行甲級主管審核", task.getName());
		taskService.claim(task.getId(), "admin");
		taskService.complete(task.getId());
		assertEquals(0, runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId).count());

	}
}
