package com.ezui.bpm.app.test;

import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;

public class WorkflowJunit extends ActivitiTestCase {
	@Deployment
	  public void namecheck() {
	    runtimeService.startProcessInstanceByKey("financeNameCheckProcess");

	    Task task = taskService.createTaskQuery().singleResult();
	    assertEquals("My Task", task.getName());

	    taskService.complete(task.getId());
	    assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	  }

}
