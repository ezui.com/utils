package com.ezui.bpm.app.variable;

import org.activiti.engine.impl.variable.CacheableVariable;
import org.activiti.engine.impl.variable.ValueFields;
import org.activiti.engine.impl.variable.VariableType;

import com.ezui.bpm.app.db.mapping.entity.ecm.CaseLive;
import com.ezui.bpm.app.persistence.entity.CaseLiveRef;

public class CaseLiveType implements VariableType, CacheableVariable {
	public static final String TYPE_NAME = "bo-CaseLive";
	private boolean forceCacheable;

	@Override
	public void setForceCacheable(boolean forceCachedValue) {
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public boolean isCachable() {
		return forceCacheable;
	}

	@Override
	public boolean isAbleToStore(Object value) {
		if (value == null) {
			return true;
		}
		if (CaseLive.class.isAssignableFrom(value.getClass())) {
			CaseLive jsonValue = (CaseLive) value;
			return jsonValue.toString().length() <= 2000;
		}
		return false;
	}

	@Override
	public void setValue(Object value, ValueFields valueFields) {
		valueFields.setCachedValue(value);
		valueFields.setLongValue(value != null ? ((CaseLive) value).getCaseRk()
				: null);
	}

	@Override
	public Object getValue(ValueFields valueFields) {
		Object cachedObject = valueFields.getCachedValue();
		if (cachedObject != null) {
			return cachedObject;
		}
		CaseLive v = null;
		if (valueFields.getLongValue() != null) {
			return new CaseLiveRef(valueFields.getLongValue());
		}
		return v;
	}

}
