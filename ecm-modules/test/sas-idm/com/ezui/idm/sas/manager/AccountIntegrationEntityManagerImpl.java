package com.ezui.idm.sas.manager;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManagerImpl;
import org.activiti.engine.impl.persistence.entity.data.DataManager;

import com.ezui.idm.adapter.UserEntityManagerAdapterImpl;
import com.ezui.idm.sas.dao.AccountIntegrationDataManager;
import com.ezui.idm.sas.entity.AccountIntegrationEntity;
import com.ezui.idm.sas.mapping.param.AccountIntegrationQueryImpl;

public class AccountIntegrationEntityManagerImpl extends UserEntityManagerAdapterImpl
		implements AccountIntegrationEntityManager {

	protected AccountIntegrationDataManager accountIntegrationDataManager;

	public AccountIntegrationEntityManagerImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration,
			AccountIntegrationDataManager accountIntegrationDataManager) {
		super(processEngineConfiguration, accountIntegrationDataManager);
		this.accountIntegrationDataManager = accountIntegrationDataManager;
	}

	@Override
	protected DataManager<UserEntity> getDataManager() {
		return accountIntegrationDataManager;
	}

	@Override
	public AccountIntegrationEntity findById(String entityId) {
		return (AccountIntegrationEntity) accountIntegrationDataManager
				.findById(entityId);
	}

	@Override
	public User createNewUser(String accountIntegrationId) {
		AccountIntegrationEntity accountIntegrationEntity = (AccountIntegrationEntity) create();
		accountIntegrationEntity.setId(accountIntegrationId);
		accountIntegrationEntity.setRevision(0); // needed as
													// accountIntegrations can
													// be transient
		return accountIntegrationEntity;
	}

	@Override
	public void updateUser(User updatedUser) {
		super.update((AccountIntegrationEntity) updatedUser);
	}

	@Override
	public void delete(UserEntity accountIntegrationEntity) {
		super.delete(accountIntegrationEntity);
	}

	@Override
	public void deletePicture(User accountIntegration) {
		AccountIntegrationEntity accountIntegrationEntity = (AccountIntegrationEntity) accountIntegration;
		if (accountIntegrationEntity.getPictureByteArrayRef() != null) {
			accountIntegrationEntity.getPictureByteArrayRef().delete();
		}
	}

	@Override
	public void delete(String accountIntegrationId) {
		AccountIntegrationEntity accountIntegration = findById(accountIntegrationId);
		if (accountIntegration != null) {
			List<IdentityInfoEntity> identityInfos = getIdentityInfoEntityManager()
					.findIdentityInfoByUserId(accountIntegrationId);
			for (IdentityInfoEntity identityInfo : identityInfos) {
				getIdentityInfoEntityManager().delete(identityInfo);
			}
			getMembershipEntityManager().deleteMembershipByUserId(
					accountIntegrationId);
			delete(accountIntegration);
		}
	}

	@Override
	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
		return accountIntegrationDataManager.findUserByQueryCriteria(query,
				page);
	}

	@Override
	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		return accountIntegrationDataManager
				.findUserCountByQueryCriteria(query);
	}

	@Override
	public List<Group> findGroupsByUser(String accountIntegrationId) {
		return accountIntegrationDataManager
				.findGroupsByUser(accountIntegrationId);
	}

	@Override
	public UserQuery createNewUserQuery() {
		return new AccountIntegrationQueryImpl(getCommandExecutor());
	}

	@Override
	public Boolean checkPassword(String accountIntegrationId, String password) {
		User accountIntegration = null;

		if (accountIntegrationId != null) {
			accountIntegration = findById(accountIntegrationId);
		}

		if ((accountIntegration != null) && (password != null)
				&& (password.equals(accountIntegration.getPassword()))) {
			return true;
		}
		return false;
	}

	@Override
	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap,
			int firstResult, int maxResults) {
		return accountIntegrationDataManager.findUsersByNativeQuery(
				parameterMap, firstResult, maxResults);
	}

	@Override
	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		return accountIntegrationDataManager
				.findUserCountByNativeQuery(parameterMap);
	}

	@Override
	public boolean isNewUser(User accountIntegration) {
		return ((AccountIntegrationEntity) accountIntegration).getRevision() == 0;
	}

	@Override
	public Picture getUserPicture(String accountIntegrationId) {
		AccountIntegrationEntity accountIntegration = findById(accountIntegrationId);
		return accountIntegration.getPicture();
	}

	@Override
	public void setUserPicture(String accountIntegrationId, Picture picture) {
		AccountIntegrationEntity accountIntegration = findById(accountIntegrationId);
		if (accountIntegration == null) {
			throw new ActivitiObjectNotFoundException("accountIntegration "
					+ accountIntegrationId + " doesn't exist", User.class);
		}

		accountIntegration.setPicture(picture);
	}

	@Override
	public AccountIntegrationDataManager getUserDataManager() {
		return accountIntegrationDataManager;
	}

	public void setUserDataManager(
			AccountIntegrationDataManager accountIntegrationDataManager) {
		this.accountIntegrationDataManager = accountIntegrationDataManager;
	}
}
