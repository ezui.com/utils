package com.ezui.idm.sas.service;

import java.util.List;

import com.ezui.idm.sas.entity.AccountIntegrationEntity;
import com.ezui.idm.sas.entity.AccountIntegrationRoleEntity;
import com.ezui.idm.sas.manager.AccountIntegrationEntityManager;
import com.ezui.idm.sas.manager.AccountIntegrationRoleEntityManager;

public interface IdentityService {
	public List<AccountIntegrationRoleEntity> selectGroupsByUserId(String userId);
	public List<AccountIntegrationRoleEntity> selectAllGroupsByUserId(String userId);
	public AccountIntegrationEntity findByLoginId(String loginId);
	public AccountIntegrationEntityManager getUserService();
	public AccountIntegrationRoleEntityManager getGroupService();
}
