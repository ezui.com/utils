package com.ezui.idm.sas.service;

import java.util.List;

import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.IEzPermission;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.sas.entity.AccountIntegrationEntity;
import com.ezui.idm.sas.manager.AccountIntegrationEntityManager;
import com.ezui.idm.sas.manager.AccountIntegrationRoleEntityManager;

public class IdentityServiceImpl implements IdentityService {
	private AccountIntegrationEntityManager userService;
	private AccountIntegrationRoleEntityManager groupService;

	@Override
	public List<AccountIntegrationEntity> selectGroupsByUserId(String userId) {
		return this.groupService.selectGroupsByUserId(userId);
	}
	@Override
	public List<AccountIntegrationRoleEntity> selectAllGroupsByUserId(String userId) {// 包括遞歸查詢的子group
		return this.groupService.selectAllGroupsByUserId(userId);
	}
	@Override
	public AccountIntegrationEntity findByLoginId(String loginId) {
		return this.userService.findById(loginId);
	}

	public AccountIntegrationEntityManager getUserService() {
		return userService;
	}
	public void setUserService(AccountIntegrationEntityManager userService) {
		this.userService = userService;
	}
	public AccountIntegrationRoleEntityManager getGroupService() {
		return groupService;
	}
	public void setGroupService(AccountIntegrationRoleEntityManager groupService) {
		this.groupService = groupService;
	}

}
