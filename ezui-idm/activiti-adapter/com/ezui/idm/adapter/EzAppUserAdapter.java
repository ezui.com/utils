package com.ezui.idm.adapter;

import java.util.Collection;

import org.activiti.app.security.ActivitiAppUser;
import org.activiti.engine.identity.User;
import org.springframework.security.core.GrantedAuthority;

public class EzAppUserAdapter extends ActivitiAppUser {
	private static final long serialVersionUID = -5544068003303576227L;

	public EzAppUserAdapter(User user, String userId,
			Collection<? extends GrantedAuthority> authorities) {
		super(user, userId, authorities);
	}

}
