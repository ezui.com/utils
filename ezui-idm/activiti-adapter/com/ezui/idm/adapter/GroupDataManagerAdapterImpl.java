package com.ezui.idm.adapter;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.data.impl.MybatisGroupDataManager;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;

import com.ezui.idm.dao.EzGroupDao;
import com.ezui.idm.entity.EzGroup;

public abstract class GroupDataManagerAdapterImpl extends
		MybatisGroupDataManager implements GroupDataManagerAdapter {
	private SqlSession sqlSession;
	@Inject
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSession = sqlSessionTemplate;
	}
	public SqlSession getSqlSession() {
		return this.sqlSession;
	}
	public GroupDataManagerAdapterImpl(){
		super(null);
	}
	public GroupDataManagerAdapterImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends GroupEntity> getManagedEntityClass() {
		return EzGroup.class;
	}

	@Override
	public GroupEntity create() {
		return new EzGroup();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		return getDbSqlSession().selectList(EzGroupDao.class.getName()+".selectByPagingQueryCriteria", query, page);
	}

	@Override
	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		return (Long) getDbSqlSession().selectOne(
				EzGroupDao.class.getName()+".countByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupsByUser(String userId) {
		return getDbSqlSession().selectList(
				EzGroupDao.class.getName()+".selectGroupsByUserId", userId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupsByNativeQuery(
			Map<String, Object> parameterMap, int firstResult, int maxResults) {
		return getDbSqlSession().selectListWithRawParameter(
				EzGroupDao.class.getName()+".selectByNativeQuery", parameterMap,
				firstResult, maxResults);
	}

	@Override
	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		return (Long) getDbSqlSession().selectOne(
				EzGroupDao.class.getName()+".selectCountByNativeQuery", parameterMap);
	}
}
