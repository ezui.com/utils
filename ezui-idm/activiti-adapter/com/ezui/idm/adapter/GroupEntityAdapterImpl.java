package com.ezui.idm.adapter;

import org.activiti.engine.impl.persistence.entity.GroupEntityImpl;

import com.ezui.idm.entity.IEzGroup;

public abstract class GroupEntityAdapterImpl extends GroupEntityImpl implements
		IEzGroup {

	private static final long serialVersionUID = -5994699492902987916L;
	protected String name;
	protected String type;

	public String getName() {
		return this.getGroupName();
	}

	public void setName(String name) {
		this.name = name;
		this.setGroupName(name);
	}

	public String getType() {
		return this.getGroupType();
	}

	public void setType(String type) {
		this.type = type;
		this.setGroupType(type);
	}

}
