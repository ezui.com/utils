package com.ezui.idm.adapter;

import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.GroupEntityManagerImpl;
import org.activiti.engine.impl.persistence.entity.data.DataManager;
import org.activiti.engine.impl.persistence.entity.data.GroupDataManager;

import com.ezui.idm.dao.IEzGroupDao;
import com.ezui.idm.mapping.param.EzGroupQueryImpl;
import com.ezui.idm.service.IEzGroupService;

public abstract class GroupEntityManagerAdapterImpl extends
		GroupEntityManagerImpl implements IEzGroupService {

	public GroupEntityManagerAdapterImpl(){
		super(null,null);
	}
	public GroupEntityManagerAdapterImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration,
			IEzGroupDao ezGroupDao) {
		super(processEngineConfiguration, ezGroupDao);
	}

	@Override
	protected DataManager<GroupEntity> getDataManager() {
		return this.getGroupDao();
	}

	@Override
	public Group createNewGroup(String groupId) {
		GroupEntity groupEntity = this.getGroupDao().create();
		groupEntity.setId(groupId);
		groupEntity.setRevision(0); // Needed as groups can be transient and not
									// save when they are returned
		return groupEntity;
	}

	@Override
	public void delete(String groupId) {
		GroupEntity group = this.getGroupDao().findById(groupId);

		if (group != null) {

			getMembershipEntityManager().deleteMembershipByGroupId(groupId);
			if (getEventDispatcher().isEnabled()) {
				getEventDispatcher().dispatchEvent(
						ActivitiEventBuilder.createMembershipEvent(
								ActivitiEventType.MEMBERSHIPS_DELETED, groupId,
								null));
			}

			delete(group);
		}
	}

	@Override
	public GroupQuery createNewGroupQuery() {
		return new EzGroupQueryImpl(getCommandExecutor());
	}

	@Override
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		return this.getGroupDao().findGroupByQueryCriteria(query, page);
	}

	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		return this.getGroupDao().findGroupCountByQueryCriteria(query);
	}

	public List<Group> findGroupsByUser(String userId) {
		return this.getGroupDao().findGroupsByUser(userId);
	}

	public List<Group> findGroupsByNativeQuery(
			Map<String, Object> parameterMap, int firstResult, int maxResults) {
		return this.getGroupDao().findGroupsByNativeQuery(parameterMap,
				firstResult, maxResults);
	}

	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		return this.getGroupDao().findGroupCountByNativeQuery(parameterMap);
	}

	@Override
	public boolean isNewGroup(Group group) {
		return ((GroupEntity) group).getRevision() == 0;
	}
	
	@Override
	public GroupDataManager getGroupDataManager() {
		return this.getGroupDao();
	}

}
