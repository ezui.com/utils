package com.ezui.idm.adapter;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.GroupQueryProperty;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

import com.ezui.idm.mapping.param.EzGroupQuery;

public abstract class GroupQueryAdapterImpl extends GroupQueryImpl implements
		EzGroupQuery {

	private static final long serialVersionUID = 6412153130431443022L;

	public GroupQueryAdapterImpl() {
	}

	public GroupQueryAdapterImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public GroupQueryAdapterImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	public GroupQuery groupId(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		this.setGroupName(this.id);
		return this;
	}

	public GroupQuery groupName(String name) {
		if (name == null) {
			throw new ActivitiIllegalArgumentException("Provided name is null");
		}
		this.name = name;
		this.setGroupDescription(this.name);
		return this;
	}

	public GroupQuery groupNameLike(String nameLike) {
		if (nameLike == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided nameLike is null");
		}
		this.nameLike = nameLike;
		this.setGroupDescriptionLike(this.nameLike);
		return this;
	}

	public GroupQuery groupType(String type) {
		if (type == null) {
			throw new ActivitiIllegalArgumentException("Provided type is null");
		}
		this.type = type;
		this.setGroupType(this.type);
		return this;
	}

	public GroupQuery groupMember(String userId) {
		if (userId == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided userId is null");
		}
		this.userId = userId;
		this.setUserId(this.userId);
		return this;
	}

	public GroupQuery potentialStarter(String procDefId) {
		if (procDefId == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided processDefinitionId is null or empty");
		}
		this.procDefId = procDefId;
		return this;

	}

	// sorting ////////////////////////////////////////////////////////
	public static final GroupQueryProperty GROUP_ID = new GroupQueryProperty(
			"RES.GROUP_NAME");
	public static final GroupQueryProperty NAME = new GroupQueryProperty(
			"RES.group_description");
	public static final GroupQueryProperty TYPE = new GroupQueryProperty(
			"RES.group_type");

	public GroupQuery orderByGroupId() {
		return orderBy(GROUP_ID);
	}

	public GroupQuery orderByGroupName() {
		return orderBy(NAME);
	}

	public GroupQuery orderByGroupType() {
		return orderBy(TYPE);
	}

	public String getOrderBy() {
		if (orderBy == null) {
			orderBy = "RES.id asc";
		}
		this.setOrderByColumns(this.orderBy);
		return orderBy;
	}

	// results ////////////////////////////////////////////////////////
	@Override
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getGroupEntityManager()
				.findGroupCountByQueryCriteria(this);
	}

	@Override
	public List<Group> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getGroupEntityManager().findGroupByQueryCriteria(
				this, page);
	}

}
