package com.ezui.idm.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.data.impl.MybatisUserDataManager;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;

import com.ezui.idm.dao.EzGroupDao;
import com.ezui.idm.dao.IEzUserDao;
import com.ezui.idm.dao.EzUserDao;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.entity.EzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;

public abstract class UserDataManagerAdapterImpl extends MybatisUserDataManager
		implements IEzUserDao {
	private SqlSession sqlSession;
	@Inject
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSession = sqlSessionTemplate;
	}
	public SqlSession getSqlSession() {
		return this.sqlSession;
	}
	
	public UserDataManagerAdapterImpl() {
		super(null);
	}

	public UserDataManagerAdapterImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends IEzUser> getManagedEntityClass() {
		return IEzUser.class;
	}
	
	@Override
	public IEzUser create() {
		return new EzUser();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
		return (List<User>)(List<?>)(this.selectByPagingQueryCriteria((EzUserQueryImpl)query));
//		return getDbSqlSession().selectList(EzUserDao.class.getName()+".selectByPagingQueryCriteria", query, page);
	}

	@Override
	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		return (Long) getDbSqlSession().selectOne(EzUserDao.class.getName()+".countByQueryCriteria", query);
	}

	public List<Group> findGroupsByUser(String userId) {
		Map<String, Object> parameterMap=new HashMap<String, Object>();
		parameterMap.put("userId", userId);
		parameterMap.put("prefix", "ECM.");
		return getSqlSession().selectList(
				EzGroupDao.class.getName()+".selectGroupsByUserId", parameterMap);
	}

	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap,
			int firstResult, int maxResults) {
		 return getDbSqlSession().selectListWithRawParameter(EzUserDao.class.getName()+".selectByNativeQuery", parameterMap, firstResult, maxResults);
	}

	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		return (Long) getDbSqlSession().selectOne(EzUserDao.class.getName()+".selectCountByNativeQuery", parameterMap);
	}
}
