package com.ezui.idm.adapter;

import org.activiti.engine.impl.persistence.entity.UserEntityImpl;

import com.ezui.idm.entity.IEzUser;

public abstract class UserEntityAdapterImpl extends UserEntityImpl implements
		IEzUser {
	private static final long serialVersionUID = 5124756679804947242L;

	@Override
	public String getId() {
		return this.getLoginId();
	}

	@Override
	public void setId(String id) {
		this.id = id;
		this.setLoginId(id);
	}

	@Override
	public String getLastName() {
		if (this.lastName == null && this.firstName == null) {
			return this.getUsername();
		} else {
			return this.lastName;
		}
	}
}
