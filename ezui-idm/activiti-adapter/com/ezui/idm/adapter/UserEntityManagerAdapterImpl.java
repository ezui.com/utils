package com.ezui.idm.adapter;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManagerImpl;
import org.activiti.engine.impl.persistence.entity.data.DataManager;

import com.ezui.idm.dao.IEzUserDao;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;
import com.ezui.idm.service.IEzUserService;

public abstract class UserEntityManagerAdapterImpl extends
		UserEntityManagerImpl implements IEzUserService {

	public UserEntityManagerAdapterImpl() {
		super(null, null);
	}

	public UserEntityManagerAdapterImpl(
			ProcessEngineConfigurationImpl processEngineConfiguration,
			IEzUserDao userDao) {
		super(processEngineConfiguration, userDao);
	}

	@Override
	protected DataManager<UserEntity> getDataManager() {
		return this.getUserDao();
	}

	@Override
	public IEzUser findById(String entityId) {
		return (IEzUser) getDataManager().findById(entityId);
	}

	@Override
	public User createNewUser(String accountIntegrationId) {
		IEzUser accountIntegrationEntity = (IEzUser) create();
		accountIntegrationEntity.setId(accountIntegrationId);
		accountIntegrationEntity.setRevision(0);
		return accountIntegrationEntity;
	}

	@Override
	public void updateUser(User updatedUser) {
		super.update((IEzUser) updatedUser);
	}

	@Override
	public void delete(UserEntity accountIntegrationEntity) {
		super.delete(accountIntegrationEntity);
	}

	@Override
	public void deletePicture(User accountIntegration) {
		IEzUser accountIntegrationEntity = (IEzUser) accountIntegration;
		if (accountIntegrationEntity.getPictureByteArrayRef() != null) {
			accountIntegrationEntity.getPictureByteArrayRef().delete();
		}
	}

	@Override
	public void delete(String accountIntegrationId) {
		IEzUser accountIntegration = findById(accountIntegrationId);
		if (accountIntegration != null) {
			List<IdentityInfoEntity> identityInfos = getIdentityInfoEntityManager()
					.findIdentityInfoByUserId(accountIntegrationId);
			for (IdentityInfoEntity identityInfo : identityInfos) {
				getIdentityInfoEntityManager().delete(identityInfo);
			}
			getMembershipEntityManager().deleteMembershipByUserId(
					accountIntegrationId);
			delete(accountIntegration);
		}
	}

	@Override
	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
		return getUserDataManager().findUserByQueryCriteria(query, page);
	}

	@Override
	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		return getUserDataManager().findUserCountByQueryCriteria(query);
	}

	@Override
	public List<Group> findGroupsByUser(String accountIntegrationId) {
		return getUserDataManager().findGroupsByUser(accountIntegrationId);
	}

	@Override
	public UserQuery createNewUserQuery() {
		return new EzUserQueryImpl(getCommandExecutor());
	}

	@Override
	public Boolean checkPassword(String accountIntegrationId, String password) {
		User accountIntegration = null;

		if (accountIntegrationId != null) {
			accountIntegration = findById(accountIntegrationId);
		}

		if ((accountIntegration != null) && (password != null)
				&& (password.equals(accountIntegration.getPassword()))) {
			return true;
		}
		return false;
	}

	@Override
	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap,
			int firstResult, int maxResults) {
		return getUserDataManager().findUsersByNativeQuery(parameterMap, firstResult,
				maxResults);
	}

	@Override
	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		return getUserDataManager().findUserCountByNativeQuery(parameterMap);
	}

	@Override
	public boolean isNewUser(User accountIntegration) {
		return ((IEzUser) accountIntegration).getRevision() == 0;
	}

	@Override
	public Picture getUserPicture(String accountIntegrationId) {
		IEzUser accountIntegration = findById(accountIntegrationId);
		return accountIntegration.getPicture();
	}

	@Override
	public void setUserPicture(String accountIntegrationId, Picture picture) {
		IEzUser accountIntegration = findById(accountIntegrationId);
		if (accountIntegration == null) {
			throw new ActivitiObjectNotFoundException("accountIntegration "
					+ accountIntegrationId + " doesn't exist", User.class);
		}

		accountIntegration.setPicture(picture);
	}

	@Override
	public IEzUserDao getUserDataManager() {
		return this.getUserDao();
	}

	public void setUserDataManager(IEzUserDao userDao) {
		super.setUserDataManager(userDao);
		this.setUserDao(userDao);
	}

}
