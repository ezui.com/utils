package com.ezui.idm.adapter;

import java.util.List;

import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.UserQueryProperty;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

import com.ezui.idm.mapping.param.EzUserQuery;

public abstract class UserQueryImplAdapter extends UserQueryImpl implements
		EzUserQuery {

	private static final long serialVersionUID = 6412153130431443022L;

	public UserQueryImplAdapter() {

	}

	public UserQueryImplAdapter(CommandContext commandContext) {
		super(commandContext);
	}

	public UserQueryImplAdapter(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	protected String id;
	protected String firstName;
	protected String firstNameLike;
	protected String lastName;
	protected String lastNameLike;
	protected String fullNameLike;
	protected String email;
	protected String emailLike;
	protected String groupId;
	protected String procDefId;

	public UserQuery userId(String id) {
		if (id == null) {
			throw new RuntimeException("Provided id is null");
		}
		this.id = id;
		this.setLoginId(this.id);
		return this;
	}

	public UserQuery userFirstName(String firstName) {
		if (firstName == null) {
			throw new RuntimeException("Provided firstName is null");
		}
		this.firstName = firstName;
		this.setUsername(this.firstName);
		return this;
	}

	public UserQuery userFirstNameLike(String firstNameLike) {
		if (firstNameLike == null) {
			throw new RuntimeException("Provided firstNameLike is null");
		}
		this.firstNameLike = firstNameLike;
		this.setUsernameLike(this.firstNameLike);
		return this;
	}

	public UserQuery userLastName(String lastName) {
		if (lastName == null) {
			throw new RuntimeException("Provided lastName is null");
		}
		this.lastName = lastName;
		this.setUsername(this.lastName);
		return this;
	}

	public UserQuery userLastNameLike(String lastNameLike) {
		if (lastNameLike == null) {
			throw new RuntimeException("Provided lastNameLike is null");
		}
		this.lastNameLike = lastNameLike;
		this.setUsernameLike(this.lastNameLike);
		return this;
	}

	public UserQuery userFullNameLike(String fullNameLike) {
		if (fullNameLike == null) {
			throw new RuntimeException("Provided full name is null");
		}
		this.fullNameLike = fullNameLike;
		this.setUsernameLike(this.fullNameLike);
		return this;
	}

	public UserQuery userEmail(String email) {
		if (email == null) {
			throw new RuntimeException("Provided email is null");
		}
		this.email = email;
		this.setEmail(this.email);
		return this;
	}

	public UserQuery userEmailLike(String emailLike) {
		if (emailLike == null) {
			throw new RuntimeException("Provided emailLike is null");
		}
		this.emailLike = emailLike;
		this.setEmailLike(this.emailLike);
		return this;
	}

	public UserQuery memberOfGroup(String groupId) {
		if (groupId == null) {
			throw new RuntimeException("Provided groupIds is null or empty");
		}
		this.groupId = groupId;
		this.setGroupId(this.groupId);
		return this;
	}

	public UserQuery potentialStarter(String procDefId) {
		if (procDefId == null) {
			throw new RuntimeException(
					"Provided processDefinitionId is null or empty");
		}
		this.procDefId = procDefId;
		return this;

	}

	// sorting //////////////////////////////////////////////////////////
	UserQueryProperty USER_ID = new UserQueryProperty("RES.ID_");
	UserQueryProperty FIRST_NAME = new UserQueryProperty("RES.USER_NAME");
	UserQueryProperty LAST_NAME = new UserQueryProperty("RES.USER_NAME");
	UserQueryProperty EMAIL = new UserQueryProperty("RES.EMAIL");

	public UserQuery orderByUserId() {
		return orderBy(USER_ID);
	}

	public UserQuery orderByUserEmail() {
		return orderBy(EMAIL);
	}

	public UserQuery orderByUserFirstName() {
		return orderBy(FIRST_NAME);
	}

	public UserQuery orderByUserLastName() {
		return orderBy(LAST_NAME);
	}
	public String getOrderBy() {
		if (orderBy == null) {
			return "RES.id asc";
		} else {
			return orderBy;
		}
	}
	// results ////////////////////////////////////////////////////////
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getUserEntityManager()
				.findUserCountByQueryCriteria(this);
	}

	public List<User> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getUserEntityManager().findUserByQueryCriteria(
				this, page);
	}
}
