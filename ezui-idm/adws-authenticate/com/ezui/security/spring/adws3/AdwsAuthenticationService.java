package com.ezui.security.spring.adws3;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import org.activiti.app.service.api.UserCache;
import org.activiti.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.ezui.app.authorization.client.AuthorityType;
import com.ezui.app.authorization.client.TypedGrantedAuthority;
import com.ezui.bpm.app.security.AuthoritiesConstants;
import com.ezui.security.spring.AuthenticationService;
import com.ezui.tcb.adws3.util.ADWS3SoapUtil;
import com.ezui.tcb.adws3.util.TcbAdParse;
import com.ezui.tcb.adws3.util.UserInfo;

@Component
public class AdwsAuthenticationService implements AuthenticationService {
	private static Logger logger = LoggerFactory.getLogger(AdwsAuthenticationService.class);
	private static final String PROPKEY_DEFAULT_ROLE_PREFIX = "default.role.prefix";
	@Inject
	private Environment env;
	@Inject
	private UserCache userCache;

	@Override
	public boolean checkPassword(String userId, String password) {
		String role_prefix = env.getProperty(PROPKEY_DEFAULT_ROLE_PREFIX);
		String wsdlurl = env.getRequiredProperty("adws.wsdl.url");
		try {
			String xmlStr = ADWS3SoapUtil.getADWS3SoapInstance(wsdlurl).userInfo(userId, password);
			logger.debug("Fetch user:{}, info: {}", userId, xmlStr);
			UserInfo userInfo = TcbAdParse.parseUserInfoXml(xmlStr);
			if ("00".equals(userInfo.getAuthStatus())) {
				try {
					UserCache.CachedUser cachedUser = this.userCache.getUser(userId, true, true, false);
					User user = cachedUser.getUser();
					Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

					grantedAuthorities.add(new TypedGrantedAuthority(AuthoritiesConstants.USER, AuthorityType.ROLE));
					String memberOfS = userInfo.getUser().getUserLdapDn();
					String[] groups = memberOfS.trim().split(",");
					for (String group : groups) {
						if (role_prefix == null || (role_prefix != null && role_prefix.trim().length() == 0) || group.toLowerCase().startsWith(role_prefix)) {
							String groupName = group.toLowerCase();
							grantedAuthorities.add(new TypedGrantedAuthority(groupName, AuthorityType.ROLE));
						}
					}
					this.userCache.putUser(userId, new UserCache.CachedUser(user, grantedAuthorities));
				} catch (Exception e) {
					logger.error("Set user authority from ADWS failed!", e);
				}
				return true;
			}
		} catch (Exception e) {
			logger.error("Check password failed via ADWS3!", e);
		}
		return false;
	}
	@Override
	public boolean isSupport(String userSource) {
		if ("ADWS".equals(userSource)) {
			return true;
		}
		return false;
	}

}
