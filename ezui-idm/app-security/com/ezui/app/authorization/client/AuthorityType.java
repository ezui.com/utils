package com.ezui.app.authorization.client;

public enum AuthorityType {
	PRIVILEGE, ROLE, GROUP;

	private AuthorityType() {
	}
}
