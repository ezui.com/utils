package com.ezui.app.authorization.client;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthorizationHelper {
	public AuthorizationHelper() {
	}

	@Deprecated
	/**
	 * 因為authority的名字前面有GROUP_前綴,只能內部使用
	 */
	public boolean hasAuthority(String name) {
		if (null == name) {
			return false;
		}
		SecurityContext securityContext = SecurityContextHolder.getContext();
		if (null == securityContext) {
			return false;
		}
		Authentication authentication = securityContext.getAuthentication();
		if (null == authentication) {
			return false;
		}
		for (GrantedAuthority ga : authentication.getAuthorities()) {
			if (name.equalsIgnoreCase(ga.getAuthority())) {
				return true;
			}
		}
		return false;
	}

	private Set<GrantedAuthority> getGrantedAuthoritiesForType(
			SecurityContext securityContext, AuthorityType type) {
		if (null == securityContext) {
			return Collections.emptySet();
		}
		Authentication authentication = securityContext.getAuthentication();
		if (null == authentication) {
			return Collections.emptySet();
		}
		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		for (GrantedAuthority ga : authentication.getAuthorities()) {
			if (((ga instanceof TypedGrantedAuthority))
					&& (type.equals(((TypedGrantedAuthority) ga)
							.getAuthorityType()))) {
				roles.add(ga);
			}
		}
		return roles;
	}

	public Set<GrantedAuthority> getPrivilegesForContext(
			SecurityContext securityContext) {
		return getGrantedAuthoritiesForType(securityContext,
				AuthorityType.PRIVILEGE);
	}

	public Set<GrantedAuthority> getCurrentPrivileges() {
		return getPrivilegesForContext(SecurityContextHolder.getContext());
	}

	public Set<GrantedAuthority> getRolesForContext(
			SecurityContext securityContext) {
		return getGrantedAuthoritiesForType(securityContext, AuthorityType.ROLE);
	}

	public Set<GrantedAuthority> getCurrentRoles() {
		return getRolesForContext(SecurityContextHolder.getContext());
	}

	public Set<GrantedAuthority> getGroupsForContext(
			SecurityContext securityContext) {
		return getGrantedAuthoritiesForType(securityContext,
				AuthorityType.GROUP);
	}

	public Set<GrantedAuthority> getCurrentGroups() {
		return getGroupsForContext(SecurityContextHolder.getContext());
	}
}
