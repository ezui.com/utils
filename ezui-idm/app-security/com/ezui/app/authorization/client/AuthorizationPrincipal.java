package com.ezui.app.authorization.client;

import java.io.Serializable;
import java.security.Principal;

public class AuthorizationPrincipal implements Principal, Serializable {
	public static final String AUTHENTICATED_USERS_GROUP = "APP_AUTHENTICATED_USERS";
	private static final long serialVersionUID = 1L;
	private String _name;
	private PrincipalType _type;

	public AuthorizationPrincipal() {
	}

	public static enum PrincipalType {
		User, Group;

		private PrincipalType() {
		}
	}

	public AuthorizationPrincipal(String name) {
		this(name, PrincipalType.User);
	}

	public AuthorizationPrincipal(String name, PrincipalType type) {
		this._name = name.toString();
		if (type == null) {
			throw new NullPointerException(
					"Authorization principal type cannot be null");
		}
		this._type = type;
	}

	public PrincipalType getType() {
		return this._type;
	}

	public final String getName() {
		return this._name;
	}

	public int hashCode() {
		int hc = getClass().getName().hashCode();
		hc ^= this._name.hashCode();
		hc ^= this._type.hashCode();
		return hc;
	}

	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		Class<?> thisClass = getClass();
		Class<?> thatClass = that.getClass();
		if ((!thisClass.isAssignableFrom(thatClass))
				|| (!thatClass.isAssignableFrom(thisClass))) {
			return false;
		}
		AuthorizationPrincipal thatPrin = (AuthorizationPrincipal) that;
		if (!this._name.equals(thatPrin.getName())) {
			return false;
		}
		if (!this._type.equals(thatPrin.getType())) {
			return false;
		}
		return true;
	}

	public final String toString() {
		return getName();
	}
}
