package com.ezui.app.authorization.client;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

public final class GroupGrantedAuthority extends TypedGrantedAuthority
		implements GrantedAuthority {
	private static final long serialVersionUID = 1L;
	private static final String _groupAuthorityPrefix = "GROUP_";
	private final String _groupName;

	public GroupGrantedAuthority(String groupName) {
		super(_groupAuthorityPrefix + groupName, AuthorityType.GROUP);
		Assert.hasText(groupName,
				"A granted authority textual representation is required");
		this._groupName = groupName;
	}

	// public GroupGrantedAuthority(GrantedAuthoritySid sid)
	// {
	// super(sid.getGrantedAuthority(), AuthorityType.GROUP);
	// if (!getAuthority().startsWith(_groupAuthorityPrefix)) {
	// throw new
	// IllegalArgumentException("GrantedAuthoritySid does not represent a group");
	// }
	// this._groupName = this.authority.substring("GROUP_".length());
	// }

	public GroupGrantedAuthority(AuthorizationPrincipal principal) {
		super(_groupAuthorityPrefix + principal.getName(), AuthorityType.GROUP);
		AuthorizationPrincipal.PrincipalType type = principal.getType();
		if (!AuthorizationPrincipal.PrincipalType.Group.equals(type)) {
			throw new IllegalArgumentException(
					"AuthorizationPrincipal does not represent a group");
		}
		this._groupName = principal.getName();
	}

	public String getGroupName() {
		return this._groupName;
	}

	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if ((that == null) || (!(that instanceof GroupGrantedAuthority))) {
			return false;
		}
		return this._groupName.equals(((GroupGrantedAuthority) that)
				.getGroupName());
	}

	public int hashCode() {
		int hc = getClass().hashCode();
		hc ^= this._groupName.hashCode();
		return hc;
	}

	public String toString() {
		return getAuthority();
	}
}
