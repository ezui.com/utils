package com.ezui.app.authorization.client;

import org.springframework.security.core.GrantedAuthority;

public class TypedGrantedAuthority implements GrantedAuthority {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7112527391906615721L;
	protected String authority;
	protected AuthorityType authorityType;

	public TypedGrantedAuthority(String authority, AuthorityType authorityType) {
		this.authority = authority;
		this.authorityType = authorityType;
	}

	@Override
	public String getAuthority() {
		return this.authority;
	}

	public AuthorityType getAuthorityType() {
		return this.authorityType;
	}

	public int hashCode() {
		int prime = 31;
		int result = 1;
		result = prime * result
				+ (this.authority == null ? 0 : this.authority.hashCode());
		result = prime
				* result
				+ (this.authorityType == null ? 0 : this.authorityType
						.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TypedGrantedAuthority other = (TypedGrantedAuthority) obj;
		if (this.authority == null) {
			if (other.authority != null) {
				return false;
			}
		} else if (!this.authority.equals(other.authority)) {
			return false;
		}
		if (this.authorityType != other.authorityType) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "TypedGrantedAuthority [authority=" + this.authority
				+ ", authorityType=" + this.authorityType + "]";
	}

}
