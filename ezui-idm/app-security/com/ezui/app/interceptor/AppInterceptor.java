package com.ezui.app.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ezui.app.web.util.AppSessionContext;
import com.ezui.app.web.util.AppSessionContextHolder;

@Deprecated
public class AppInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(AppInterceptor.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (logger.isDebugEnabled()) {
			long startTime = System.currentTimeMillis();
			request.setAttribute("startTime", startTime);
		}
		HttpSession httpSession = request.getSession();
		AppSessionContext appSessionCtx = AppSessionContextHolder.getAppSessionContext(httpSession);
		if (appSessionCtx == null) {
			logger.debug("Create new appSessionContext!");
			appSessionCtx = new AppSessionContext();
			AppSessionContextHolder.setAppSessionContext(appSessionCtx, httpSession);
		}
		if (!appSessionCtx.isInitialized()) {
			appSessionCtx.initialize();
			logger.debug("New appSessionContext initialized!");
		}
		return true;
	}

	// after the handler is executed
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// log it
		if (logger.isDebugEnabled()) {
			Long startTime = (Long) request.getAttribute("startTime");
			if (startTime != null) {
				long endTime = System.currentTimeMillis();
				long executeTime = endTime - startTime;
				logger.debug("[" + handler + "] executeTime : " + executeTime + "ms");
			}
		}
		AppSessionContextHolder.clear();
		logger.debug("appSessionContext has been set to null!");
	}

}
