package com.ezui.app.web.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;

import com.ezui.app.authorization.client.AuthorizationHelper;
import com.ezui.bpm.app.security.EzAppUser;
import com.ezui.idm.entity.IEzUser;

@Deprecated
public class AppSessionContext implements Serializable {

	private static final long serialVersionUID = -2190923648467487180L;
	private static Logger logger = LoggerFactory.getLogger(AppSessionContext.class.getName());

	private Set<String> groups;
	private Set<String> roles;
	private Set<String> privileges;
	private Set<String> capabilities;

	private String userDisplayName;

	private boolean initialized;
	private List<List<String>> groupLists;

	public AppSessionContext() {
		initialized=false;
	}

	public boolean isInitialized() {
		return this.initialized;
	}

	public void initialize() {
		this.capabilities = new HashSet<String>();
		this.groups = new HashSet<String>();
		this.roles = new HashSet<String>();
		this.privileges = new HashSet<String>();

		List<List<String>> tempGroupLists = new ArrayList<List<String>>();
		this.groupLists = Collections.unmodifiableList(tempGroupLists);

		SecurityContext securityCtx = EzAppSecurityUtils.getCurrentSecurityContext();
		if (logger.isDebugEnabled()) {
			logger.debug("Security Context:: " + (securityCtx == null ? null : securityCtx.getAuthentication()));
		}
		if ((securityCtx == null) || (securityCtx.getAuthentication() == null)) {
			this.userDisplayName = null;
		} else {
			this.userDisplayName = findUserDisplayName();
			if (logger.isInfoEnabled()) {
				logger.info("Creating session context for user, " + this.userDisplayName);
			}
			AuthorizationHelper helper = new AuthorizationHelper();
			for (GrantedAuthority role : helper.getRolesForContext(securityCtx)) {
				this.roles.add(role.getAuthority());
			}
			List<String> groupList = null;
			for (GrantedAuthority group : helper.getGroupsForContext(securityCtx)) {
				String groupName;
				if (group.getAuthority().startsWith("GROUP_")) {
					groupName = group.getAuthority().replace("GROUP_", "");
				} else {
					groupName = group.getAuthority();
				}
				this.groups.add(groupName);
				if ((groupList == null) || (groupList.size() >= 1000)) {
					groupList = new ArrayList<String>();
					tempGroupLists.add(Collections.unmodifiableList(groupList));
				}
				groupList.add(groupName);
			}
			for (GrantedAuthority privilege : helper.getPrivilegesForContext(securityCtx)) {
				this.privileges.add(privilege.getAuthority().replace("PRIV_", ""));
			}
			if (logger.isDebugEnabled()) {
				logger.debug(this.userDisplayName + " is in these roles: " + this.roles);
				logger.debug(this.userDisplayName + " is a member of the groups: " + this.groups);
			}
		}
		String applicationName = null;
		if (StringUtils.isBlank(applicationName)) {
			logger.warn("SessionContext could not be locked. No application-name is defined yet.");
		} else {
		}

		this.initialized = true;
	}

	private String findUserDisplayName() {
		Object principal = EzAppSecurityUtils.getCurrentSecurityContext().getAuthentication().getPrincipal();
		if (principal == null) {
			return "";
		}
		if (principal instanceof EzAppUser) {
			EzAppUser details = (EzAppUser) principal;
			IEzUser user = (IEzUser) details.getUserObject();
			return user == null ? details.getUsername() : user.getUsername();
		} else {
			return principal.toString();
		}
	}

	public void processUserLogoff() {
		initialized=false;
		try {
			if (logger.isInfoEnabled()) {
				logger.info("Clearing session context for user, " + this.userDisplayName);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		} finally {
		}
	}

	public List<List<String>> getGroupLists() {
		return this.groupLists;
	}

	public Set<String> getGroups() {
		return groups;
	}

	public void setGroups(Set<String> groups) {
		this.groups = groups;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	public Set<String> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(Set<String> privileges) {
		this.privileges = privileges;
	}

	public Set<String> getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Set<String> capabilities) {
		this.capabilities = capabilities;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}
}
