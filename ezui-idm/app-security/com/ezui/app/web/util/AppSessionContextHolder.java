package com.ezui.app.web.util;

import javax.servlet.http.HttpSession;

@Deprecated
public class AppSessionContextHolder {
	private static ThreadLocal<AppSessionContext> obj = new InheritableThreadLocal<AppSessionContext>();

	public static void setAppSessionContext(AppSessionContext appSessionCtx,
			HttpSession session) {
		setAppSessionContext(appSessionCtx);
		session.setAttribute("ecmAppSessionContext", appSessionCtx);
	}

	public static void setAppSessionContext(AppSessionContext appSessionCtx) {
		obj.set(appSessionCtx);
	}

	public static AppSessionContext getAppSessionContext() {
		return (AppSessionContext) obj.get();
	}

	public static AppSessionContext getAppSessionContext(HttpSession session) {
		if (obj.get() == null) {
			obj.set((AppSessionContext) session
					.getAttribute("ecmAppSessionContext"));
		}
		return (AppSessionContext) obj.get();
	}

	public static void clear() {
		obj.set(null);
	}
}
