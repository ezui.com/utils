package com.ezui.app.web.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.activiti.engine.identity.User;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ezui.bpm.app.security.AuthoritiesConstants;
import com.ezui.bpm.app.security.EzAppUser;

@Deprecated
public class EzAppSecurityUtils {

	public static AppSessionContext getInitializedAppSessionContext() {
		AppSessionContext appSessionCtx = AppSessionContextHolder
				.getAppSessionContext();
		return (appSessionCtx != null && appSessionCtx.isInitialized()) ? appSessionCtx
				: null;
	}
	
	public static boolean isAdministrator(){
		Set<String> roles=getCurrentUserRoles();
		for(String role:roles){
			if(role.equals(AuthoritiesConstants.ADMIN)){
				return true;
			}
		}
		Set<String> groups=getCurrentUserGroups();
		for(String group:groups){
			if(group.equals(AuthoritiesConstants.ADMIN)){
				return true;
			}
		}
		return false;
	}

	public static Set<String> getCurrentUserGroups() {
		AppSessionContext appSessionCtx = AppSessionContextHolder
				.getAppSessionContext();
		return appSessionCtx != null ? appSessionCtx.getGroups()
				: new HashSet<String>(0);
	}

	public static Set<String> getCurrentUserRoles() {
		AppSessionContext appSessionCtx = AppSessionContextHolder
				.getAppSessionContext();
		return appSessionCtx != null ? appSessionCtx.getRoles()
				: new HashSet<String>(0);
	}

	public static SecurityContext getCurrentSecurityContext() {
		return SecurityContextHolder.getContext();
	}

	public static String getCurrentUserId() {
		User user = getCurrentUserObject();
		if (user != null) {
			return user.getId();
		}
		return null;
	}

	public static String getCurrentUserDisplayName() {
		AppSessionContext appSessionCtx = AppSessionContextHolder
				.getAppSessionContext();

		return appSessionCtx != null ? appSessionCtx.getUserDisplayName() : "";
	}

	public static List<List<String>> getCurrentUserGroupsList() {
		AppSessionContext appSessionCtx = AppSessionContextHolder
				.getAppSessionContext();

		return appSessionCtx != null ? appSessionCtx.getGroupLists()
				: new ArrayList(0);
	}

	public static User getCurrentUserObject() {
		User user = null;
		EzAppUser appUser = getCurrentEzAppUser();
		if (appUser != null) {
			user = appUser.getUserObject();
		}
		return user;
	}

	public static EzAppUser getCurrentEzAppUser() {
		SecurityContext securityContext = getCurrentSecurityContext();
		EzAppUser user = null;
		if (securityContext != null
				&& securityContext.getAuthentication() != null) {
			Object principal = securityContext.getAuthentication()
					.getPrincipal();
			if (principal != null && principal instanceof EzAppUser) {
				user = (EzAppUser) principal;
			}
		}
		return user;
	}

	public static boolean hasRole(String name) {
		Set<String> userGroups = EzAppSecurityUtils.getCurrentUserRoles();
		for (String group : userGroups) {
			if (group.equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasRole(List<String> groups) {
		Set<String> userGroups = EzAppSecurityUtils.getCurrentUserRoles();
		for (String group : userGroups) {
			for (String name : groups) {
				if (group.equalsIgnoreCase(name)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean hasGroup(String name) {
		Set<String> userGroups = EzAppSecurityUtils.getCurrentUserGroups();
		for (String group : userGroups) {
			if (group.equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasGroup(List<String> groups) {
		Set<String> userGroups = EzAppSecurityUtils.getCurrentUserGroups();
		for (String group : userGroups) {
			for (String name : groups) {
				if (group.equalsIgnoreCase(name)) {
					return true;
				}
			}
		}
		return false;
	}
}
