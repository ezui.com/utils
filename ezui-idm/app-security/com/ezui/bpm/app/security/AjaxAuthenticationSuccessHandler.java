package com.ezui.bpm.app.security;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	@Inject
	private Environment env;
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		Boolean supportPOSTSSO=this.env.getProperty("idm.sso.post",Boolean.class);
		if (supportPOSTSSO != null && supportPOSTSSO.booleanValue()) {
			String ssourlpath = "https://saml.bot.com.tw/SASAuthorizationServices/directAuth.jsp?service=https%3A%2F%2Fsaml.bot.com.tw%2FSASEntCaseManagement%2F&";
			StringBuffer sb = new StringBuffer();
			sb.append(ssourlpath);
			// @saspw 代表內部帳號密碼
			sb.append("username=").append(request.getParameter("j_username")).append("@saspw").append("&");
			sb.append("password=").append(request.getParameter("j_password"));
			String targetUrl = sb.toString();
			redirectStrategy.sendRedirect(request, response, targetUrl);
		}else {
			response.setStatus(200);
		}
	}
}
