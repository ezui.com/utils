package com.ezui.bpm.app.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.ezui.app.web.util.AppSessionContext;
import com.ezui.app.web.util.AppSessionContextHolder;

@Component
public class AjaxLogoutSuccessHandler extends
		AbstractAuthenticationTargetUrlRequestHandler implements
		LogoutSuccessHandler {
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		response.setStatus(200);
		HttpSession httpSession = request.getSession();
		AppSessionContext appSessionCtx = AppSessionContextHolder.getAppSessionContext(httpSession);
		if (appSessionCtx != null) {
			appSessionCtx.processUserLogoff();
		}
	}
}
