package com.ezui.bpm.app.security;

public final class AuthoritiesConstants {
	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";
}