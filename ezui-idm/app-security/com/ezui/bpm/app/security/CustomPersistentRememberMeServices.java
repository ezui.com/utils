package com.ezui.bpm.app.security;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.app.domain.idm.PersistentToken;
import org.activiti.app.security.ActivitiAppUser;
import org.activiti.app.service.idm.PersistentTokenService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

//@Service
public class CustomPersistentRememberMeServices extends
		AbstractRememberMeServices implements CustomRememberMeService {
	private final Logger log = LoggerFactory
			.getLogger(CustomPersistentRememberMeServices.class);
	public static final String COOKIE_NAME = "EZ_REMEMBER_ME";

	@Inject
	private PersistentTokenService persistentTokenService;

	@Inject
	private CustomUserDetailService customUserDetailService;

	@Inject
	private IdentityService identityService;
	private final int tokenMaxAgeInSeconds;
	private final long tokenMaxAgeInMilliseconds;
	private final long tokenRefreshDurationInMilliseconds;

	@Inject
	public CustomPersistentRememberMeServices(Environment env,
			UserDetailsService userDetailsService) {
		super(env.getProperty("security.rememberme.key"), userDetailsService);

		setAlwaysRemember(true);

		Integer tokenMaxAgeSeconds = (Integer) env.getProperty(
				"security.cookie.max-age", Integer.class);
		if (tokenMaxAgeSeconds != null)
			this.log.info("Cookie max-age set to " + tokenMaxAgeSeconds
					+ " seconds");
		else {
			tokenMaxAgeSeconds = Integer.valueOf(2678400);
		}
		this.tokenMaxAgeInSeconds = tokenMaxAgeSeconds.intValue();
		this.tokenMaxAgeInMilliseconds = (tokenMaxAgeSeconds.longValue() * 1000L);

		Integer tokenRefreshSeconds = (Integer) env.getProperty(
				"security.cookie.refresh-age", Integer.class);
		if (tokenRefreshSeconds != null)
			this.log.info("Cookie refresh age set to " + tokenRefreshSeconds
					+ " seconds");
		else {
			tokenRefreshSeconds = Integer.valueOf(86400);
		}
		this.tokenRefreshDurationInMilliseconds = (tokenRefreshSeconds
				.longValue() * 1000L);

		setCookieName("ACTIVITI_REMEMBER_ME");
	}

	protected void onLoginSuccess(HttpServletRequest request,
			HttpServletResponse response,
			Authentication successfulAuthentication) {
		String userEmail = successfulAuthentication.getName();

		this.log.debug("Creating new persistent login for user {}", userEmail);
		ActivitiAppUser activitiAppUser = (ActivitiAppUser) successfulAuthentication
				.getPrincipal();

		PersistentToken token = createAndInsertPersistentToken(
				activitiAppUser.getUserObject(), request.getRemoteAddr(),
				request.getHeader("User-Agent"));
		addCookie(token, request, response);
	}

	@Transactional
	protected UserDetails processAutoLoginCookie(String[] cookieTokens,
			HttpServletRequest request, HttpServletResponse response) {
		PersistentToken token = getPersistentToken(cookieTokens);

		if (new Date().getTime() - token.getTokenDate().getTime() > this.tokenRefreshDurationInMilliseconds) {
			try {
				User user = (User) this.identityService.createUserQuery()
						.userId(token.getUser()).singleResult();
				if (user != null) {
					token = this.persistentTokenService.createToken(user,
							request.getRemoteAddr(),
							request.getHeader("User-Agent"));

					addCookie(token, request, response);
				} else {
					this.log.error("invalide user token");
					throw new RememberMeAuthenticationException(
							"invalide user token");
				}
			} catch (DataAccessException e) {
				this.log.error("Failed to update token: ", e);
				throw new RememberMeAuthenticationException(
						"Autologin failed due to data access problem: "
								+ e.getMessage());
			}

		}

		UserDetails user = this.customUserDetailService.loadByUserId(token
				.getUser());

		return user;
	}

	@Transactional
	public void logout(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) {
		String rememberMeCookie = extractRememberMeCookie(request);
		if ((rememberMeCookie != null) && (rememberMeCookie.length() != 0)) {
			try {
				String[] cookieTokens = decodeCookie(rememberMeCookie);
				PersistentToken token = getPersistentToken(cookieTokens);
				this.persistentTokenService.delete(token);
			} catch (InvalidCookieException ice) {
				this.log.info("Invalid cookie, no persistent token could be deleted");
			} catch (RememberMeAuthenticationException rmae) {
				this.log.debug("No persistent token found, so no token could be deleted");
			}
		}
		super.logout(request, response, authentication);
	}

	private PersistentToken getPersistentToken(String[] cookieTokens) {
		if (cookieTokens.length != 2) {
			throw new InvalidCookieException(
					"Cookie token did not contain 2 tokens, but contained '"
							+ Arrays.asList(cookieTokens) + "'");
		}

		String presentedSeries = cookieTokens[0];
		String presentedToken = cookieTokens[1];

		PersistentToken token = this.persistentTokenService
				.getPersistentToken(presentedSeries);

		if (token == null) {
			throw new RememberMeAuthenticationException(
					"No persistent token found for series id: "
							+ presentedSeries);
		}

		if (!presentedToken.equals(token.getTokenValue())) {
			token = this.persistentTokenService.getPersistentToken(
					presentedSeries, true);
			if (!presentedToken.equals(token.getTokenValue())) {
				this.persistentTokenService.delete(token);
				throw new CookieTheftException(
						"Invalid remember-me token (Series/token) mismatch. Implies previous cookie theft attack.");
			}

		}

		if (new Date().getTime() - token.getTokenDate().getTime() > this.tokenMaxAgeInMilliseconds) {
			throw new RememberMeAuthenticationException(
					"Remember-me login has expired");
		}
		return token;
	}

	private void addCookie(PersistentToken token, HttpServletRequest request,
			HttpServletResponse response) {
		setCookie(new String[] { token.getSeries(), token.getTokenValue() },
				this.tokenMaxAgeInSeconds, request, response);
	}

	protected void setCookie(String[] tokens, int maxAge,
			HttpServletRequest request, HttpServletResponse response) {
		String cookieValue = encodeCookie(tokens);
		Cookie cookie = new Cookie(getCookieName(), cookieValue);
		cookie.setMaxAge(maxAge);
		cookie.setPath("/");

		cookie.setSecure(request.isSecure());

		Method setHttpOnlyMethod = ReflectionUtils.findMethod(Cookie.class,
				"setHttpOnly", new Class[] { Boolean.TYPE });
		if (setHttpOnlyMethod != null)
			ReflectionUtils.invokeMethod(setHttpOnlyMethod, cookie,
					new Object[] { Boolean.TRUE });
		else if (this.log.isDebugEnabled()) {
			this.log
					.debug("Note: Cookie will not be marked as HttpOnly because you are not using Servlet 3.0 (Cookie#setHttpOnly(boolean) was not found).");
		}

		response.addCookie(cookie);
	}

	public PersistentToken createAndInsertPersistentToken(User user,
			String remoteAddress, String userAgent) {
		return this.persistentTokenService.createToken(user, remoteAddress,
				userAgent);
	}
}
