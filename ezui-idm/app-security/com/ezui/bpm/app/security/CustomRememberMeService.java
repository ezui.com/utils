package com.ezui.bpm.app.security;

import org.activiti.app.domain.idm.PersistentToken;
import org.activiti.engine.identity.User;

public abstract interface CustomRememberMeService {
	public abstract PersistentToken createAndInsertPersistentToken(
			User paramUser, String paramString1, String paramString2);
}
