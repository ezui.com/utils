package com.ezui.bpm.app.security;

import org.springframework.security.core.userdetails.UserDetails;

public abstract interface CustomUserDetailService {
	public abstract UserDetails loadByUserId(String paramString);

	public abstract UserDetails loadUserByUsername(String paramString);

//	public abstract boolean checkPassword(String userId, String password);
}
