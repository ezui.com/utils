package com.ezui.bpm.app.security;

import java.util.Collection;

import org.activiti.engine.identity.User;
import org.springframework.security.core.GrantedAuthority;

import com.ezui.idm.adapter.EzAppUserAdapter;
import com.ezui.idm.entity.IEzUser;
import com.ezui.security.spring.MutableUserDetail;

public class EzAppUser extends EzAppUserAdapter implements MutableUserDetail<IEzUser>{

	private static final long serialVersionUID = -7826488818920003983L;

	public EzAppUser(User user, String userId,
			Collection<? extends GrantedAuthority> authorities) {
		super(user, userId, authorities);
	}

	@Override
	public IEzUser getUser() {
		User u=this.getUserObject();
		if(u instanceof IEzUser){
			return (IEzUser)u;
		}
		return null;
	}

}
