package com.ezui.bpm.app.security;

import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.entity.EzPersistentToken;

public abstract interface EzRememberMeService {
	public abstract EzPersistentToken createAndInsertPersistentToken(
			IEzUser paramUser, String paramString1, String paramString2);
}
