package com.ezui.bpm.app.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.activiti.app.service.api.UserCache;
import org.activiti.engine.identity.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.ezui.app.authorization.client.AuthorityType;
import com.ezui.app.authorization.client.TypedGrantedAuthority;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.service.IdentityService;

public class UserDetailsService implements
		org.springframework.security.core.userdetails.UserDetailsService,
		CustomUserDetailService {

	@Inject
	private UserCache userCache;

	@Inject
	private IdentityService identityService;

	@Inject
	private Environment env;
	private long userValidityPeriod;

	@Transactional
	public UserDetails loadUserByUsername(String login) {
		String actualLogin = login;
		User userFromDatabase = null;

//		userFromDatabase = (User) this.identityService.createUserQuery()
//				.userId(actualLogin).singleResult();
		userFromDatabase = (User) this.identityService.findByLoginId(actualLogin);

		if (userFromDatabase == null) {
			throw new UsernameNotFoundException("User " + actualLogin
					+ " was not found in the database");
		}

		Collection<GrantedAuthority> grantedAuthorities = loadUserAuthority(userFromDatabase
				.getId());

		this.userCache.putUser(userFromDatabase.getId(),
				new UserCache.CachedUser(userFromDatabase, grantedAuthorities));

		EzAppAuthentication.setAuthenticatedUserId(String
				.valueOf(userFromDatabase.getId()));

		return new EzAppUser(userFromDatabase, actualLogin, grantedAuthorities);
	}

	@Transactional
	public UserDetails loadByUserId(String userId) {
		UserCache.CachedUser cachedUser = this.userCache.getUser(userId, true,
				true, false);
		if (cachedUser == null) {
			throw new UsernameNotFoundException("User " + userId
					+ " was not found in the database");
		}

		long lastDatabaseCheck = cachedUser.getLastDatabaseCheck();
		long currentTime = System.currentTimeMillis();

		if ((this.userValidityPeriod <= 0L)
				|| (currentTime - lastDatabaseCheck >= this.userValidityPeriod)) {
			this.userCache.invalidate(userId);
			cachedUser = this.userCache.getUser(userId, true, true, false);

			cachedUser.setLastDatabaseCheck(currentTime);
		}

		User user = cachedUser.getUser();
		String actualUserId = user.getId();
		Collection<GrantedAuthority> grantedAuthorities = cachedUser
				.getGrantedAuthorities();
		if (grantedAuthorities.size() <= 1) {// 如果cache沒有找到
			grantedAuthorities = loadUserAuthority(actualUserId);
			this.userCache.putUser(user.getId(), new UserCache.CachedUser(user,
					grantedAuthorities));
		}

		EzAppAuthentication
				.setAuthenticatedUserId(String.valueOf(user.getId()));

		return new EzAppUser(user, actualUserId, grantedAuthorities);
	}

	public Collection<GrantedAuthority> loadUserAuthority(String userId) {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

		grantedAuthorities.add(new TypedGrantedAuthority(
				AuthoritiesConstants.USER, AuthorityType.ROLE));

		String superUserGroupName = this.env.getRequiredProperty("admin.group");
//		List<Group> groups = this.identityService.createGroupQuery()
//				.groupMember(userId).list();
		List<IEzGroup> groups = this.identityService.selectAllGroupsByUserId(userId);
		for (IEzGroup ezgroup : groups) {
//			if (group instanceof IEzGroup) {
//				IEzGroup ezgroup = (IEzGroup) group;
				if ("1".equals(ezgroup.getAvailable())) {
					if (StringUtils.equalsIgnoreCase(superUserGroupName,
							ezgroup.getGroupName())) {
						grantedAuthorities
								.add(new TypedGrantedAuthority(
										AuthoritiesConstants.ADMIN,
										AuthorityType.ROLE));
						grantedAuthorities.add(new TypedGrantedAuthority(ezgroup
								.getId(), AuthorityType.ROLE));
					} else {
						grantedAuthorities.add(new TypedGrantedAuthority(
								ezgroup.getGroupName(), AuthorityType.ROLE));
					}
				}
//			} else {
//				if (StringUtils.equalsIgnoreCase(superUserGroupName,
//						group.getName())) {
//					grantedAuthorities.add(new TypedGrantedAuthority(
//							AuthoritiesConstants.ADMIN, AuthorityType.ROLE));
//					grantedAuthorities.add(new TypedGrantedAuthority(group
//							.getId(), AuthorityType.ROLE));
//				} else {
//					grantedAuthorities.add(new TypedGrantedAuthority(group
//							.getId(), AuthorityType.ROLE));
//				}
//			}
		}
		return grantedAuthorities;
	}

	public void setUserValidityPeriod(long userValidityPeriod) {
		this.userValidityPeriod = userValidityPeriod;
	}

}
