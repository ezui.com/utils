package com.ezui.web.factory;

import com.ezui.web.session.DefaultSessionContext;
import com.ezui.web.session.ISessionContext;

public class DefaultSessionContextFactory implements ISessionContextFactory{

	@Override
	public ISessionContext newSessionContext() {
		return new DefaultSessionContext();
	}

}
