package com.ezui.web.factory;

import com.ezui.web.session.ISessionContext;

public interface ISessionContextFactory {
	public ISessionContext newSessionContext();
}
