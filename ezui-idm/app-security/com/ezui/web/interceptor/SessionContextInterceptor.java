package com.ezui.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ezui.web.factory.DefaultSessionContextFactory;
import com.ezui.web.factory.ISessionContextFactory;
import com.ezui.web.session.ISessionContext;
import com.ezui.web.util.SessionContextHolder;

@SuppressWarnings("all")
public class SessionContextInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(SessionContextInterceptor.class);
	private ISessionContextFactory sessionContextFactory = null;

	public SessionContextInterceptor() {

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (logger.isDebugEnabled()) {
			long startTime = System.nanoTime();
			request.setAttribute(ISessionContext.HTTP_REQUEST_START_TIME, startTime);
		}
		ISessionContext sessionctx = SessionContextHolder.getSessionContext(request.getSession());
		if (sessionctx == null) {
			logger.debug("Create new ISessionContext!");
			if (sessionContextFactory == null) {
				sessionContextFactory = new DefaultSessionContextFactory();
			}
			sessionctx = sessionContextFactory.newSessionContext();
			SessionContextHolder.setSessionContext(sessionctx, request.getSession());
		}
		if (!sessionctx.isInitialized()) {
			sessionctx.initialize(request, response);
			logger.debug("Init ISessionContext!");
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modeAndView) throws Exception {
		if (logger.isDebugEnabled()) {
			Long startTime = (Long) request.getAttribute(ISessionContext.HTTP_REQUEST_START_TIME);
			if (startTime != null) {
				long endTime = System.nanoTime();
				logger.debug("{} executeTime : {}", new Object[] { handler, (endTime - startTime) });
			}
		}
		SessionContextHolder.clear();
		logger.debug("ISessionContext has been set to null");
	}

	public ISessionContextFactory getSessionContextFactory() {
		return sessionContextFactory;
	}

	public void setSessionContextFactory(ISessionContextFactory sessionContextFactory) {
		this.sessionContextFactory = sessionContextFactory;
	}

}
