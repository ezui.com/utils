package com.ezui.web.security.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface WebUI {
	public String key() default "";
	public String name() default "";
}
