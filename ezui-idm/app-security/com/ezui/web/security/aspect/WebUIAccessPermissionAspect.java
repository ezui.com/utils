package com.ezui.web.security.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.ezui.web.security.annotation.WebUI;
import com.ezui.web.security.annotation.WebUIAction;

@Aspect
@Configuration
public class WebUIAccessPermissionAspect {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Around("@annotation(com.ezui.web.security.annotation.WebUIAction)")
	public void around(ProceedingJoinPoint joinPoint) throws Throwable {
		long startTime = System.currentTimeMillis();
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
	    Method method = signature.getMethod();
	    WebUIAction webUIAction = method.getAnnotation(WebUIAction.class);
	    String action=webUIAction.action();
		Class<?> cls=method.getDeclaringClass();
		WebUI webui = method.getAnnotation(WebUI.class);
		if(webui==null) {
			webui=cls.getAnnotation(WebUI.class);
		} 
		String uiName="";
		if(webui!=null) {
			uiName=webui.name();
		}
	    try {
			joinPoint.proceed();
			long timeTaken = System.currentTimeMillis() - startTime;
			logger.info("[SUCCESS]訪問 {} 的 {}, 耗時{}", uiName, action, timeTaken);
	    }catch(Throwable e) {
			joinPoint.proceed();
			long timeTaken = System.currentTimeMillis() - startTime;
			logger.error("[ERROR]訪問 {} 的 {}, 耗時{}", uiName, action, timeTaken, e);
	    	throw e;
	    }
	}
}
