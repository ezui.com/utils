package com.ezui.web.session;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

public class DefaultSessionContext implements ISessionContext {
	private static final long serialVersionUID = AppSerialVersionUID;
	protected transient boolean initialized = false;
	private transient ApplicationContext applicationContext;
	private transient ServletContext servletContext;

	@Override
	public ISessionContext initialize(HttpServletRequest request, HttpServletResponse response) {
		initialized = true;
		servletContext = request.getServletContext();
		applicationContext = (ApplicationContext) servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		return this;
	}

	@Override
	public boolean isInitialized() {
		return this.initialized;
	}

	@Override
	public String getUserId() {
		return null;
	}

	@Override
	public String getUserName() {
		return null;
	}

	@Override
	public Object getUserObject() {
		return null;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
