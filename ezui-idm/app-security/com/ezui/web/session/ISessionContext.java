package com.ezui.web.session;

import java.io.Serializable;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;

@SuppressWarnings("all")
public interface ISessionContext extends Serializable{
	public static final long AppSerialVersionUID = 1L;
	
	public static final String HTTP_REQUEST_START_TIME= "ez.HTTP_REQUEST_START_TIME";
	public static final String SESSION_CONTEXT= "ez.SESSION_CONTEXT";
	public static final String USER_OBJECT= "ez.USER_OBJECT";
	public static final String USER_ID= "ez.USER_ID";

	public ISessionContext initialize(HttpServletRequest request, HttpServletResponse response);
	public boolean isInitialized();
	public ServletContext getServletContext();
	public ApplicationContext getApplicationContext();
	public String getUserId();
	public String getUserName();
	public Object getUserObject();
}
