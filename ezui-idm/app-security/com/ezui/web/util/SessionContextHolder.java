package com.ezui.web.util;

import javax.servlet.http.HttpSession;

import com.ezui.web.session.ISessionContext;

@SuppressWarnings("all")
public class SessionContextHolder {
	private static ThreadLocal<ISessionContext> obj = new InheritableThreadLocal<ISessionContext>();

	public static void setSessionContext(ISessionContext sessionctx, HttpSession session) {
		setSessionContext(sessionctx);
		session.setAttribute(ISessionContext.SESSION_CONTEXT, sessionctx);
	}

	public static void setSessionContext(ISessionContext sessionctx) {
		obj.set(sessionctx);
	}

	public static ISessionContext getSessionContext() {
		return obj.get();
	}

	public static ISessionContext getSessionContext(HttpSession session) {
		ISessionContext sessionctx = obj.get();
		if (sessionctx == null) {
			sessionctx = (ISessionContext) session.getAttribute(ISessionContext.SESSION_CONTEXT);
			obj.set(sessionctx);
		}
		return sessionctx;
	}

	public static void clear() {
		obj.set(null);
	}
}
