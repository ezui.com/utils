package com.ezui.idm.mapping.entity.ecm;

import java.io.Serializable;

public class DepartmentXUser extends DepartmentXUserKey implements Serializable {
    private String titleCode;

    private String jobTitle;

    private static final long serialVersionUID = 1L;

    public String getTitleCode() {
        return titleCode;
    }

    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode == null ? null : titleCode.trim();
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle == null ? null : jobTitle.trim();
    }
}