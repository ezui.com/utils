package com.ezui.idm.mapping.entity.ecm;

import java.io.Serializable;

public class DepartmentXUserKey implements Serializable {
    private String deptId;

    private String userId;

    private static final long serialVersionUID = 1L;

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId == null ? null : deptId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
}