package com.ezui.idm.mapping.entity.ecm;

import java.io.Serializable;

public class GroupXGroupKey implements Serializable {
    private String groupId;

    private String parentId;

    private static final long serialVersionUID = 1L;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }
}