package com.ezui.idm.mapping.entity.ecm;

import java.io.Serializable;
import java.math.BigDecimal;

public class GroupXPermissionKey implements Serializable {
    private String groupId;

    private BigDecimal permissionId;

    private static final long serialVersionUID = 1L;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    public BigDecimal getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(BigDecimal permissionId) {
        this.permissionId = permissionId;
    }
}