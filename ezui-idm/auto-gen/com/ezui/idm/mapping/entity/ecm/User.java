package com.ezui.idm.mapping.entity.ecm;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private String id;

    private String loginId;

    private String username;

    private Integer revision;

    private String password;

    private String salt;

    private String email;

    private String defaultTitleCode;

    private String defaultJobTitle;

    private String defaultDeptNo;

    private String defaultDeptName;

    private String pictureId;

    private String locked;

    private String available;

    private Date createdDate;

    private Date modifiedDate;

    private String createdBy;

    private String modifiedBy;

    private String userSource;

    private String workflowDisabled;

    private String userLdapDn;

    private String userDelegation;

    private Integer failedAuthAttempt;

    private String userDescription;

    private String defaultBranchNo;

    private String defaultBranchName;

    private Date rowLockDate;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId == null ? null : loginId.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getDefaultTitleCode() {
        return defaultTitleCode;
    }

    public void setDefaultTitleCode(String defaultTitleCode) {
        this.defaultTitleCode = defaultTitleCode == null ? null : defaultTitleCode.trim();
    }

    public String getDefaultJobTitle() {
        return defaultJobTitle;
    }

    public void setDefaultJobTitle(String defaultJobTitle) {
        this.defaultJobTitle = defaultJobTitle == null ? null : defaultJobTitle.trim();
    }

    public String getDefaultDeptNo() {
        return defaultDeptNo;
    }

    public void setDefaultDeptNo(String defaultDeptNo) {
        this.defaultDeptNo = defaultDeptNo == null ? null : defaultDeptNo.trim();
    }

    public String getDefaultDeptName() {
        return defaultDeptName;
    }

    public void setDefaultDeptName(String defaultDeptName) {
        this.defaultDeptName = defaultDeptName == null ? null : defaultDeptName.trim();
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId == null ? null : pictureId.trim();
    }

    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked == null ? null : locked.trim();
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available == null ? null : available.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }

    public String getUserSource() {
        return userSource;
    }

    public void setUserSource(String userSource) {
        this.userSource = userSource == null ? null : userSource.trim();
    }

    public String getWorkflowDisabled() {
        return workflowDisabled;
    }

    public void setWorkflowDisabled(String workflowDisabled) {
        this.workflowDisabled = workflowDisabled == null ? null : workflowDisabled.trim();
    }

    public String getUserLdapDn() {
        return userLdapDn;
    }

    public void setUserLdapDn(String userLdapDn) {
        this.userLdapDn = userLdapDn == null ? null : userLdapDn.trim();
    }

    public String getUserDelegation() {
        return userDelegation;
    }

    public void setUserDelegation(String userDelegation) {
        this.userDelegation = userDelegation == null ? null : userDelegation.trim();
    }

    public Integer getFailedAuthAttempt() {
        return failedAuthAttempt;
    }

    public void setFailedAuthAttempt(Integer failedAuthAttempt) {
        this.failedAuthAttempt = failedAuthAttempt;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription == null ? null : userDescription.trim();
    }

    public String getDefaultBranchNo() {
        return defaultBranchNo;
    }

    public void setDefaultBranchNo(String defaultBranchNo) {
        this.defaultBranchNo = defaultBranchNo == null ? null : defaultBranchNo.trim();
    }

    public String getDefaultBranchName() {
        return defaultBranchName;
    }

    public void setDefaultBranchName(String defaultBranchName) {
        this.defaultBranchName = defaultBranchName == null ? null : defaultBranchName.trim();
    }

    public Date getRowLockDate() {
        return rowLockDate;
    }

    public void setRowLockDate(Date rowLockDate) {
        this.rowLockDate = rowLockDate;
    }
}