package com.ezui.idm.mapping.entity.ecm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andLoginIdIsNull() {
            addCriterion("login_id is null");
            return (Criteria) this;
        }

        public Criteria andLoginIdIsNotNull() {
            addCriterion("login_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoginIdEqualTo(String value) {
            addCriterion("login_id =", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotEqualTo(String value) {
            addCriterion("login_id <>", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdGreaterThan(String value) {
            addCriterion("login_id >", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdGreaterThanOrEqualTo(String value) {
            addCriterion("login_id >=", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdLessThan(String value) {
            addCriterion("login_id <", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdLessThanOrEqualTo(String value) {
            addCriterion("login_id <=", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdLike(String value) {
            addCriterion("login_id like", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotLike(String value) {
            addCriterion("login_id not like", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdIn(List<String> values) {
            addCriterion("login_id in", values, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotIn(List<String> values) {
            addCriterion("login_id not in", values, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdBetween(String value1, String value2) {
            addCriterion("login_id between", value1, value2, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotBetween(String value1, String value2) {
            addCriterion("login_id not between", value1, value2, "loginId");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("username is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("username is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("username =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("username <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("username >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("username >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("username <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("username <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("username like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("username not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("username in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("username not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("username between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("username not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andRevisionIsNull() {
            addCriterion("revision is null");
            return (Criteria) this;
        }

        public Criteria andRevisionIsNotNull() {
            addCriterion("revision is not null");
            return (Criteria) this;
        }

        public Criteria andRevisionEqualTo(Integer value) {
            addCriterion("revision =", value, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionNotEqualTo(Integer value) {
            addCriterion("revision <>", value, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionGreaterThan(Integer value) {
            addCriterion("revision >", value, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionGreaterThanOrEqualTo(Integer value) {
            addCriterion("revision >=", value, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionLessThan(Integer value) {
            addCriterion("revision <", value, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionLessThanOrEqualTo(Integer value) {
            addCriterion("revision <=", value, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionIn(List<Integer> values) {
            addCriterion("revision in", values, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionNotIn(List<Integer> values) {
            addCriterion("revision not in", values, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionBetween(Integer value1, Integer value2) {
            addCriterion("revision between", value1, value2, "revision");
            return (Criteria) this;
        }

        public Criteria andRevisionNotBetween(Integer value1, Integer value2) {
            addCriterion("revision not between", value1, value2, "revision");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andSaltIsNull() {
            addCriterion("salt is null");
            return (Criteria) this;
        }

        public Criteria andSaltIsNotNull() {
            addCriterion("salt is not null");
            return (Criteria) this;
        }

        public Criteria andSaltEqualTo(String value) {
            addCriterion("salt =", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotEqualTo(String value) {
            addCriterion("salt <>", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltGreaterThan(String value) {
            addCriterion("salt >", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltGreaterThanOrEqualTo(String value) {
            addCriterion("salt >=", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLessThan(String value) {
            addCriterion("salt <", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLessThanOrEqualTo(String value) {
            addCriterion("salt <=", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLike(String value) {
            addCriterion("salt like", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotLike(String value) {
            addCriterion("salt not like", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltIn(List<String> values) {
            addCriterion("salt in", values, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotIn(List<String> values) {
            addCriterion("salt not in", values, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltBetween(String value1, String value2) {
            addCriterion("salt between", value1, value2, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotBetween(String value1, String value2) {
            addCriterion("salt not between", value1, value2, "salt");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeIsNull() {
            addCriterion("default_title_code is null");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeIsNotNull() {
            addCriterion("default_title_code is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeEqualTo(String value) {
            addCriterion("default_title_code =", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeNotEqualTo(String value) {
            addCriterion("default_title_code <>", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeGreaterThan(String value) {
            addCriterion("default_title_code >", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeGreaterThanOrEqualTo(String value) {
            addCriterion("default_title_code >=", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeLessThan(String value) {
            addCriterion("default_title_code <", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeLessThanOrEqualTo(String value) {
            addCriterion("default_title_code <=", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeLike(String value) {
            addCriterion("default_title_code like", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeNotLike(String value) {
            addCriterion("default_title_code not like", value, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeIn(List<String> values) {
            addCriterion("default_title_code in", values, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeNotIn(List<String> values) {
            addCriterion("default_title_code not in", values, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeBetween(String value1, String value2) {
            addCriterion("default_title_code between", value1, value2, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultTitleCodeNotBetween(String value1, String value2) {
            addCriterion("default_title_code not between", value1, value2, "defaultTitleCode");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleIsNull() {
            addCriterion("default_job_title is null");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleIsNotNull() {
            addCriterion("default_job_title is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleEqualTo(String value) {
            addCriterion("default_job_title =", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleNotEqualTo(String value) {
            addCriterion("default_job_title <>", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleGreaterThan(String value) {
            addCriterion("default_job_title >", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleGreaterThanOrEqualTo(String value) {
            addCriterion("default_job_title >=", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleLessThan(String value) {
            addCriterion("default_job_title <", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleLessThanOrEqualTo(String value) {
            addCriterion("default_job_title <=", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleLike(String value) {
            addCriterion("default_job_title like", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleNotLike(String value) {
            addCriterion("default_job_title not like", value, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleIn(List<String> values) {
            addCriterion("default_job_title in", values, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleNotIn(List<String> values) {
            addCriterion("default_job_title not in", values, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleBetween(String value1, String value2) {
            addCriterion("default_job_title between", value1, value2, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultJobTitleNotBetween(String value1, String value2) {
            addCriterion("default_job_title not between", value1, value2, "defaultJobTitle");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoIsNull() {
            addCriterion("default_dept_no is null");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoIsNotNull() {
            addCriterion("default_dept_no is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoEqualTo(String value) {
            addCriterion("default_dept_no =", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoNotEqualTo(String value) {
            addCriterion("default_dept_no <>", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoGreaterThan(String value) {
            addCriterion("default_dept_no >", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoGreaterThanOrEqualTo(String value) {
            addCriterion("default_dept_no >=", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoLessThan(String value) {
            addCriterion("default_dept_no <", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoLessThanOrEqualTo(String value) {
            addCriterion("default_dept_no <=", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoLike(String value) {
            addCriterion("default_dept_no like", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoNotLike(String value) {
            addCriterion("default_dept_no not like", value, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoIn(List<String> values) {
            addCriterion("default_dept_no in", values, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoNotIn(List<String> values) {
            addCriterion("default_dept_no not in", values, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoBetween(String value1, String value2) {
            addCriterion("default_dept_no between", value1, value2, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNoNotBetween(String value1, String value2) {
            addCriterion("default_dept_no not between", value1, value2, "defaultDeptNo");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameIsNull() {
            addCriterion("default_dept_name is null");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameIsNotNull() {
            addCriterion("default_dept_name is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameEqualTo(String value) {
            addCriterion("default_dept_name =", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameNotEqualTo(String value) {
            addCriterion("default_dept_name <>", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameGreaterThan(String value) {
            addCriterion("default_dept_name >", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameGreaterThanOrEqualTo(String value) {
            addCriterion("default_dept_name >=", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameLessThan(String value) {
            addCriterion("default_dept_name <", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameLessThanOrEqualTo(String value) {
            addCriterion("default_dept_name <=", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameLike(String value) {
            addCriterion("default_dept_name like", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameNotLike(String value) {
            addCriterion("default_dept_name not like", value, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameIn(List<String> values) {
            addCriterion("default_dept_name in", values, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameNotIn(List<String> values) {
            addCriterion("default_dept_name not in", values, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameBetween(String value1, String value2) {
            addCriterion("default_dept_name between", value1, value2, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andDefaultDeptNameNotBetween(String value1, String value2) {
            addCriterion("default_dept_name not between", value1, value2, "defaultDeptName");
            return (Criteria) this;
        }

        public Criteria andPictureIdIsNull() {
            addCriterion("picture_id is null");
            return (Criteria) this;
        }

        public Criteria andPictureIdIsNotNull() {
            addCriterion("picture_id is not null");
            return (Criteria) this;
        }

        public Criteria andPictureIdEqualTo(String value) {
            addCriterion("picture_id =", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotEqualTo(String value) {
            addCriterion("picture_id <>", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdGreaterThan(String value) {
            addCriterion("picture_id >", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdGreaterThanOrEqualTo(String value) {
            addCriterion("picture_id >=", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdLessThan(String value) {
            addCriterion("picture_id <", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdLessThanOrEqualTo(String value) {
            addCriterion("picture_id <=", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdLike(String value) {
            addCriterion("picture_id like", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotLike(String value) {
            addCriterion("picture_id not like", value, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdIn(List<String> values) {
            addCriterion("picture_id in", values, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotIn(List<String> values) {
            addCriterion("picture_id not in", values, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdBetween(String value1, String value2) {
            addCriterion("picture_id between", value1, value2, "pictureId");
            return (Criteria) this;
        }

        public Criteria andPictureIdNotBetween(String value1, String value2) {
            addCriterion("picture_id not between", value1, value2, "pictureId");
            return (Criteria) this;
        }

        public Criteria andLockedIsNull() {
            addCriterion("locked is null");
            return (Criteria) this;
        }

        public Criteria andLockedIsNotNull() {
            addCriterion("locked is not null");
            return (Criteria) this;
        }

        public Criteria andLockedEqualTo(String value) {
            addCriterion("locked =", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotEqualTo(String value) {
            addCriterion("locked <>", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedGreaterThan(String value) {
            addCriterion("locked >", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedGreaterThanOrEqualTo(String value) {
            addCriterion("locked >=", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedLessThan(String value) {
            addCriterion("locked <", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedLessThanOrEqualTo(String value) {
            addCriterion("locked <=", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedLike(String value) {
            addCriterion("locked like", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotLike(String value) {
            addCriterion("locked not like", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedIn(List<String> values) {
            addCriterion("locked in", values, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotIn(List<String> values) {
            addCriterion("locked not in", values, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedBetween(String value1, String value2) {
            addCriterion("locked between", value1, value2, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotBetween(String value1, String value2) {
            addCriterion("locked not between", value1, value2, "locked");
            return (Criteria) this;
        }

        public Criteria andAvailableIsNull() {
            addCriterion("available is null");
            return (Criteria) this;
        }

        public Criteria andAvailableIsNotNull() {
            addCriterion("available is not null");
            return (Criteria) this;
        }

        public Criteria andAvailableEqualTo(String value) {
            addCriterion("available =", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableNotEqualTo(String value) {
            addCriterion("available <>", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableGreaterThan(String value) {
            addCriterion("available >", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableGreaterThanOrEqualTo(String value) {
            addCriterion("available >=", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableLessThan(String value) {
            addCriterion("available <", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableLessThanOrEqualTo(String value) {
            addCriterion("available <=", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableLike(String value) {
            addCriterion("available like", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableNotLike(String value) {
            addCriterion("available not like", value, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableIn(List<String> values) {
            addCriterion("available in", values, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableNotIn(List<String> values) {
            addCriterion("available not in", values, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableBetween(String value1, String value2) {
            addCriterion("available between", value1, value2, "available");
            return (Criteria) this;
        }

        public Criteria andAvailableNotBetween(String value1, String value2) {
            addCriterion("available not between", value1, value2, "available");
            return (Criteria) this;
        }

        public Criteria andCreatedDateIsNull() {
            addCriterion("created_date is null");
            return (Criteria) this;
        }

        public Criteria andCreatedDateIsNotNull() {
            addCriterion("created_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedDateEqualTo(Date value) {
            addCriterion("created_date =", value, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateNotEqualTo(Date value) {
            addCriterion("created_date <>", value, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateGreaterThan(Date value) {
            addCriterion("created_date >", value, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateGreaterThanOrEqualTo(Date value) {
            addCriterion("created_date >=", value, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateLessThan(Date value) {
            addCriterion("created_date <", value, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateLessThanOrEqualTo(Date value) {
            addCriterion("created_date <=", value, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateIn(List<Date> values) {
            addCriterion("created_date in", values, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateNotIn(List<Date> values) {
            addCriterion("created_date not in", values, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateBetween(Date value1, Date value2) {
            addCriterion("created_date between", value1, value2, "createdDate");
            return (Criteria) this;
        }

        public Criteria andCreatedDateNotBetween(Date value1, Date value2) {
            addCriterion("created_date not between", value1, value2, "createdDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateIsNull() {
            addCriterion("modified_date is null");
            return (Criteria) this;
        }

        public Criteria andModifiedDateIsNotNull() {
            addCriterion("modified_date is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedDateEqualTo(Date value) {
            addCriterion("modified_date =", value, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateNotEqualTo(Date value) {
            addCriterion("modified_date <>", value, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateGreaterThan(Date value) {
            addCriterion("modified_date >", value, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateGreaterThanOrEqualTo(Date value) {
            addCriterion("modified_date >=", value, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateLessThan(Date value) {
            addCriterion("modified_date <", value, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateLessThanOrEqualTo(Date value) {
            addCriterion("modified_date <=", value, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateIn(List<Date> values) {
            addCriterion("modified_date in", values, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateNotIn(List<Date> values) {
            addCriterion("modified_date not in", values, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateBetween(Date value1, Date value2) {
            addCriterion("modified_date between", value1, value2, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andModifiedDateNotBetween(Date value1, Date value2) {
            addCriterion("modified_date not between", value1, value2, "modifiedDate");
            return (Criteria) this;
        }

        public Criteria andCreatedByIsNull() {
            addCriterion("created_by is null");
            return (Criteria) this;
        }

        public Criteria andCreatedByIsNotNull() {
            addCriterion("created_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedByEqualTo(String value) {
            addCriterion("created_by =", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotEqualTo(String value) {
            addCriterion("created_by <>", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByGreaterThan(String value) {
            addCriterion("created_by >", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
            addCriterion("created_by >=", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLessThan(String value) {
            addCriterion("created_by <", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLessThanOrEqualTo(String value) {
            addCriterion("created_by <=", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLike(String value) {
            addCriterion("created_by like", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotLike(String value) {
            addCriterion("created_by not like", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByIn(List<String> values) {
            addCriterion("created_by in", values, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotIn(List<String> values) {
            addCriterion("created_by not in", values, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByBetween(String value1, String value2) {
            addCriterion("created_by between", value1, value2, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotBetween(String value1, String value2) {
            addCriterion("created_by not between", value1, value2, "createdBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByIsNull() {
            addCriterion("modified_by is null");
            return (Criteria) this;
        }

        public Criteria andModifiedByIsNotNull() {
            addCriterion("modified_by is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedByEqualTo(String value) {
            addCriterion("modified_by =", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotEqualTo(String value) {
            addCriterion("modified_by <>", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByGreaterThan(String value) {
            addCriterion("modified_by >", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByGreaterThanOrEqualTo(String value) {
            addCriterion("modified_by >=", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByLessThan(String value) {
            addCriterion("modified_by <", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByLessThanOrEqualTo(String value) {
            addCriterion("modified_by <=", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByLike(String value) {
            addCriterion("modified_by like", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotLike(String value) {
            addCriterion("modified_by not like", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByIn(List<String> values) {
            addCriterion("modified_by in", values, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotIn(List<String> values) {
            addCriterion("modified_by not in", values, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByBetween(String value1, String value2) {
            addCriterion("modified_by between", value1, value2, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotBetween(String value1, String value2) {
            addCriterion("modified_by not between", value1, value2, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andUserSourceIsNull() {
            addCriterion("user_source is null");
            return (Criteria) this;
        }

        public Criteria andUserSourceIsNotNull() {
            addCriterion("user_source is not null");
            return (Criteria) this;
        }

        public Criteria andUserSourceEqualTo(String value) {
            addCriterion("user_source =", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceNotEqualTo(String value) {
            addCriterion("user_source <>", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceGreaterThan(String value) {
            addCriterion("user_source >", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceGreaterThanOrEqualTo(String value) {
            addCriterion("user_source >=", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceLessThan(String value) {
            addCriterion("user_source <", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceLessThanOrEqualTo(String value) {
            addCriterion("user_source <=", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceLike(String value) {
            addCriterion("user_source like", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceNotLike(String value) {
            addCriterion("user_source not like", value, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceIn(List<String> values) {
            addCriterion("user_source in", values, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceNotIn(List<String> values) {
            addCriterion("user_source not in", values, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceBetween(String value1, String value2) {
            addCriterion("user_source between", value1, value2, "userSource");
            return (Criteria) this;
        }

        public Criteria andUserSourceNotBetween(String value1, String value2) {
            addCriterion("user_source not between", value1, value2, "userSource");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledIsNull() {
            addCriterion("workflow_disabled is null");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledIsNotNull() {
            addCriterion("workflow_disabled is not null");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledEqualTo(String value) {
            addCriterion("workflow_disabled =", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledNotEqualTo(String value) {
            addCriterion("workflow_disabled <>", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledGreaterThan(String value) {
            addCriterion("workflow_disabled >", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledGreaterThanOrEqualTo(String value) {
            addCriterion("workflow_disabled >=", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledLessThan(String value) {
            addCriterion("workflow_disabled <", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledLessThanOrEqualTo(String value) {
            addCriterion("workflow_disabled <=", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledLike(String value) {
            addCriterion("workflow_disabled like", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledNotLike(String value) {
            addCriterion("workflow_disabled not like", value, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledIn(List<String> values) {
            addCriterion("workflow_disabled in", values, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledNotIn(List<String> values) {
            addCriterion("workflow_disabled not in", values, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledBetween(String value1, String value2) {
            addCriterion("workflow_disabled between", value1, value2, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andWorkflowDisabledNotBetween(String value1, String value2) {
            addCriterion("workflow_disabled not between", value1, value2, "workflowDisabled");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnIsNull() {
            addCriterion("user_ldap_dn is null");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnIsNotNull() {
            addCriterion("user_ldap_dn is not null");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnEqualTo(String value) {
            addCriterion("user_ldap_dn =", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnNotEqualTo(String value) {
            addCriterion("user_ldap_dn <>", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnGreaterThan(String value) {
            addCriterion("user_ldap_dn >", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnGreaterThanOrEqualTo(String value) {
            addCriterion("user_ldap_dn >=", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnLessThan(String value) {
            addCriterion("user_ldap_dn <", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnLessThanOrEqualTo(String value) {
            addCriterion("user_ldap_dn <=", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnLike(String value) {
            addCriterion("user_ldap_dn like", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnNotLike(String value) {
            addCriterion("user_ldap_dn not like", value, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnIn(List<String> values) {
            addCriterion("user_ldap_dn in", values, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnNotIn(List<String> values) {
            addCriterion("user_ldap_dn not in", values, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnBetween(String value1, String value2) {
            addCriterion("user_ldap_dn between", value1, value2, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserLdapDnNotBetween(String value1, String value2) {
            addCriterion("user_ldap_dn not between", value1, value2, "userLdapDn");
            return (Criteria) this;
        }

        public Criteria andUserDelegationIsNull() {
            addCriterion("user_delegation is null");
            return (Criteria) this;
        }

        public Criteria andUserDelegationIsNotNull() {
            addCriterion("user_delegation is not null");
            return (Criteria) this;
        }

        public Criteria andUserDelegationEqualTo(String value) {
            addCriterion("user_delegation =", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationNotEqualTo(String value) {
            addCriterion("user_delegation <>", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationGreaterThan(String value) {
            addCriterion("user_delegation >", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationGreaterThanOrEqualTo(String value) {
            addCriterion("user_delegation >=", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationLessThan(String value) {
            addCriterion("user_delegation <", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationLessThanOrEqualTo(String value) {
            addCriterion("user_delegation <=", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationLike(String value) {
            addCriterion("user_delegation like", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationNotLike(String value) {
            addCriterion("user_delegation not like", value, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationIn(List<String> values) {
            addCriterion("user_delegation in", values, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationNotIn(List<String> values) {
            addCriterion("user_delegation not in", values, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationBetween(String value1, String value2) {
            addCriterion("user_delegation between", value1, value2, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andUserDelegationNotBetween(String value1, String value2) {
            addCriterion("user_delegation not between", value1, value2, "userDelegation");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptIsNull() {
            addCriterion("failed_auth_attempt is null");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptIsNotNull() {
            addCriterion("failed_auth_attempt is not null");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptEqualTo(Integer value) {
            addCriterion("failed_auth_attempt =", value, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptNotEqualTo(Integer value) {
            addCriterion("failed_auth_attempt <>", value, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptGreaterThan(Integer value) {
            addCriterion("failed_auth_attempt >", value, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptGreaterThanOrEqualTo(Integer value) {
            addCriterion("failed_auth_attempt >=", value, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptLessThan(Integer value) {
            addCriterion("failed_auth_attempt <", value, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptLessThanOrEqualTo(Integer value) {
            addCriterion("failed_auth_attempt <=", value, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptIn(List<Integer> values) {
            addCriterion("failed_auth_attempt in", values, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptNotIn(List<Integer> values) {
            addCriterion("failed_auth_attempt not in", values, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptBetween(Integer value1, Integer value2) {
            addCriterion("failed_auth_attempt between", value1, value2, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andFailedAuthAttemptNotBetween(Integer value1, Integer value2) {
            addCriterion("failed_auth_attempt not between", value1, value2, "failedAuthAttempt");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionIsNull() {
            addCriterion("user_description is null");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionIsNotNull() {
            addCriterion("user_description is not null");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionEqualTo(String value) {
            addCriterion("user_description =", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionNotEqualTo(String value) {
            addCriterion("user_description <>", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionGreaterThan(String value) {
            addCriterion("user_description >", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("user_description >=", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionLessThan(String value) {
            addCriterion("user_description <", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionLessThanOrEqualTo(String value) {
            addCriterion("user_description <=", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionLike(String value) {
            addCriterion("user_description like", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionNotLike(String value) {
            addCriterion("user_description not like", value, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionIn(List<String> values) {
            addCriterion("user_description in", values, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionNotIn(List<String> values) {
            addCriterion("user_description not in", values, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionBetween(String value1, String value2) {
            addCriterion("user_description between", value1, value2, "userDescription");
            return (Criteria) this;
        }

        public Criteria andUserDescriptionNotBetween(String value1, String value2) {
            addCriterion("user_description not between", value1, value2, "userDescription");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoIsNull() {
            addCriterion("default_branch_no is null");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoIsNotNull() {
            addCriterion("default_branch_no is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoEqualTo(String value) {
            addCriterion("default_branch_no =", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoNotEqualTo(String value) {
            addCriterion("default_branch_no <>", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoGreaterThan(String value) {
            addCriterion("default_branch_no >", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoGreaterThanOrEqualTo(String value) {
            addCriterion("default_branch_no >=", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoLessThan(String value) {
            addCriterion("default_branch_no <", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoLessThanOrEqualTo(String value) {
            addCriterion("default_branch_no <=", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoLike(String value) {
            addCriterion("default_branch_no like", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoNotLike(String value) {
            addCriterion("default_branch_no not like", value, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoIn(List<String> values) {
            addCriterion("default_branch_no in", values, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoNotIn(List<String> values) {
            addCriterion("default_branch_no not in", values, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoBetween(String value1, String value2) {
            addCriterion("default_branch_no between", value1, value2, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNoNotBetween(String value1, String value2) {
            addCriterion("default_branch_no not between", value1, value2, "defaultBranchNo");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameIsNull() {
            addCriterion("default_branch_name is null");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameIsNotNull() {
            addCriterion("default_branch_name is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameEqualTo(String value) {
            addCriterion("default_branch_name =", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameNotEqualTo(String value) {
            addCriterion("default_branch_name <>", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameGreaterThan(String value) {
            addCriterion("default_branch_name >", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameGreaterThanOrEqualTo(String value) {
            addCriterion("default_branch_name >=", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameLessThan(String value) {
            addCriterion("default_branch_name <", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameLessThanOrEqualTo(String value) {
            addCriterion("default_branch_name <=", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameLike(String value) {
            addCriterion("default_branch_name like", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameNotLike(String value) {
            addCriterion("default_branch_name not like", value, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameIn(List<String> values) {
            addCriterion("default_branch_name in", values, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameNotIn(List<String> values) {
            addCriterion("default_branch_name not in", values, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameBetween(String value1, String value2) {
            addCriterion("default_branch_name between", value1, value2, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andDefaultBranchNameNotBetween(String value1, String value2) {
            addCriterion("default_branch_name not between", value1, value2, "defaultBranchName");
            return (Criteria) this;
        }

        public Criteria andRowLockDateIsNull() {
            addCriterion("row_lock_date is null");
            return (Criteria) this;
        }

        public Criteria andRowLockDateIsNotNull() {
            addCriterion("row_lock_date is not null");
            return (Criteria) this;
        }

        public Criteria andRowLockDateEqualTo(Date value) {
            addCriterion("row_lock_date =", value, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateNotEqualTo(Date value) {
            addCriterion("row_lock_date <>", value, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateGreaterThan(Date value) {
            addCriterion("row_lock_date >", value, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateGreaterThanOrEqualTo(Date value) {
            addCriterion("row_lock_date >=", value, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateLessThan(Date value) {
            addCriterion("row_lock_date <", value, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateLessThanOrEqualTo(Date value) {
            addCriterion("row_lock_date <=", value, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateIn(List<Date> values) {
            addCriterion("row_lock_date in", values, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateNotIn(List<Date> values) {
            addCriterion("row_lock_date not in", values, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateBetween(Date value1, Date value2) {
            addCriterion("row_lock_date between", value1, value2, "rowLockDate");
            return (Criteria) this;
        }

        public Criteria andRowLockDateNotBetween(Date value1, Date value2) {
            addCriterion("row_lock_date not between", value1, value2, "rowLockDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}