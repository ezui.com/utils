package com.ezui.idm.mapping.entity.ecm;

import java.io.Serializable;

public class UserXGroupKey implements Serializable {
    private String userId;

    private String groupId;

    private static final long serialVersionUID = 1L;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }
}