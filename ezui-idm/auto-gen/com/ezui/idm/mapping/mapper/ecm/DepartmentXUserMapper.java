package com.ezui.idm.mapping.mapper.ecm;

import com.ezui.idm.mapping.entity.ecm.DepartmentXUser;
import com.ezui.idm.mapping.entity.ecm.DepartmentXUserExample;
import com.ezui.idm.mapping.entity.ecm.DepartmentXUserKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface DepartmentXUserMapper {
    long countByExample(DepartmentXUserExample example);

    int deleteByExample(DepartmentXUserExample example);

    int deleteByPrimaryKey(DepartmentXUserKey key);

    int insert(DepartmentXUser record);

    int insertSelective(DepartmentXUser record);

    List<DepartmentXUser> selectByExampleWithRowbounds(DepartmentXUserExample example, RowBounds rowBounds);

    List<DepartmentXUser> selectByExample(DepartmentXUserExample example);

    DepartmentXUser selectByPrimaryKey(DepartmentXUserKey key);

    int updateByExampleSelective(@Param("record") DepartmentXUser record, @Param("example") DepartmentXUserExample example);

    int updateByExample(@Param("record") DepartmentXUser record, @Param("example") DepartmentXUserExample example);

    int updateByPrimaryKeySelective(DepartmentXUser record);

    int updateByPrimaryKey(DepartmentXUser record);
}