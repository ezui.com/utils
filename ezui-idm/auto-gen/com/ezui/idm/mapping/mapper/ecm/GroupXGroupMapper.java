package com.ezui.idm.mapping.mapper.ecm;

import com.ezui.idm.mapping.entity.ecm.GroupXGroupExample;
import com.ezui.idm.mapping.entity.ecm.GroupXGroupKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface GroupXGroupMapper {
    long countByExample(GroupXGroupExample example);

    int deleteByExample(GroupXGroupExample example);

    int deleteByPrimaryKey(GroupXGroupKey key);

    int insert(GroupXGroupKey record);

    int insertSelective(GroupXGroupKey record);

    List<GroupXGroupKey> selectByExampleWithRowbounds(GroupXGroupExample example, RowBounds rowBounds);

    List<GroupXGroupKey> selectByExample(GroupXGroupExample example);

    int updateByExampleSelective(@Param("record") GroupXGroupKey record, @Param("example") GroupXGroupExample example);

    int updateByExample(@Param("record") GroupXGroupKey record, @Param("example") GroupXGroupExample example);
}