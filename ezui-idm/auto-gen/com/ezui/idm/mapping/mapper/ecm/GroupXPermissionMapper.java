package com.ezui.idm.mapping.mapper.ecm;

import com.ezui.idm.mapping.entity.ecm.GroupXPermissionExample;
import com.ezui.idm.mapping.entity.ecm.GroupXPermissionKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface GroupXPermissionMapper {
    long countByExample(GroupXPermissionExample example);

    int deleteByExample(GroupXPermissionExample example);

    int deleteByPrimaryKey(GroupXPermissionKey key);

    int insert(GroupXPermissionKey record);

    int insertSelective(GroupXPermissionKey record);

    List<GroupXPermissionKey> selectByExampleWithRowbounds(GroupXPermissionExample example, RowBounds rowBounds);

    List<GroupXPermissionKey> selectByExample(GroupXPermissionExample example);

    int updateByExampleSelective(@Param("record") GroupXPermissionKey record, @Param("example") GroupXPermissionExample example);

    int updateByExample(@Param("record") GroupXPermissionKey record, @Param("example") GroupXPermissionExample example);
}