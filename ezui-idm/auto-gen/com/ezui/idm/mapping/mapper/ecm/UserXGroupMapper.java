package com.ezui.idm.mapping.mapper.ecm;

import com.ezui.idm.mapping.entity.ecm.UserXGroupExample;
import com.ezui.idm.mapping.entity.ecm.UserXGroupKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface UserXGroupMapper {
    long countByExample(UserXGroupExample example);

    int deleteByExample(UserXGroupExample example);

    int deleteByPrimaryKey(UserXGroupKey key);

    int insert(UserXGroupKey record);

    int insertSelective(UserXGroupKey record);

    List<UserXGroupKey> selectByExampleWithRowbounds(UserXGroupExample example, RowBounds rowBounds);

    List<UserXGroupKey> selectByExample(UserXGroupExample example);

    int updateByExampleSelective(@Param("record") UserXGroupKey record, @Param("example") UserXGroupExample example);

    int updateByExample(@Param("record") UserXGroupKey record, @Param("example") UserXGroupExample example);
}