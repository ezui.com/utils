package com.ezui.idm.dao;

import com.ezui.idm.entity.Entity;

public interface DataManager<EntityImpl extends Entity> {
	  
	  EntityImpl create();

	  EntityImpl findById(String entityId);
	  
	  void insert(EntityImpl entity);
	  
	  EntityImpl update(EntityImpl entity);

	  void delete(String id);

	  void delete(EntityImpl entity);
}
