package com.ezui.idm.app.rest;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ezui.app.exception.BadRequestException;
import com.ezui.app.rest.model.ResultListDataRepresentation;
import com.ezui.idm.app.rest.request.GroupQueryRequest;
import com.ezui.idm.app.rest.request.UserQueryRequest;
import com.ezui.idm.app.rest.response.GroupRepresentation;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.mapping.param.EzGroupQueryImpl;
import com.ezui.idm.mapping.param.EzUserQueryImpl;
import com.ezui.idm.service.IdentityService;

@RestController
public class EzIdmGroupsResource {
	private static final int DEFAULT_PAGE_SIZE = 100;
	@Inject
	protected IdentityService identityService;

	@RequestMapping(value = "/rest/idm/groups", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public ResultListDataRepresentation list(@RequestBody GroupQueryRequest form) {
		if (form == null) {
			throw new BadRequestException("No request found");
		}
		if (form.getSize() <= 0) {
			form.setSize(DEFAULT_PAGE_SIZE);
		}
		if (form.getStart() < 1 || form.getSize() < 1) {
			throw new BadRequestException("參數錯誤");// Collections.EMPTY_LIST;
		}
		EzGroupQueryImpl query = new EzGroupQueryImpl();
		query.setReadUncommited(true);// 提高查詢效率
		query.setFirstRow(form.getStart());
		query.setLastRow(form.getStart() + form.getSize());
		if (form.getSortBy() == null || form.getSortBy().trim().length() == 0) {
			query.setOrderByColumns("RES.ID");
		} else {
			query.setOrderByColumns(form.getSortBy());
		}
		if (!StringUtils.isEmpty(form.getId())) {
			query.setId(form.getId().trim());
		}
		if (!StringUtils.isEmpty(form.getGroupName())) {
			query.setGroupName(form.getGroupName().trim());
		}
		if (!StringUtils.isEmpty(form.getGroupNameLike())) {
			query.setGroupNameLike("%" + form.getGroupNameLike().trim() + "%");
		}
		List<IEzGroup> data = identityService.getGroupService().selectByPagingQueryCriteria(query);

		ResultListDataRepresentation result = new ResultListDataRepresentation(data);

		Long totalCount = Long.valueOf(data.size());
		if (data.size() == form.getSize()) {
			totalCount = identityService.getGroupService().countByQueryCriteria(query);
			result.setTotal(Long.valueOf(totalCount.intValue()));
		} else {
			if (form.getPage() == 1) {
				result.setTotal(totalCount);
			} else {
				result.setTotal(totalCount + form.getPage() * form.getSize());
			}
		}
		result.setPaginateRequest(form);

		return result;
	}

	@RequestMapping(value = "/rest/idm/groups/{groupId}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public GroupRepresentation selectOne(@PathVariable String groupId) {
		IEzGroup group = identityService.getGroupService().getGroupDao().selectByPrimaryKey(groupId);
		return new GroupRepresentation(group);
	}

	@RequestMapping(value = "/rest/idm/groups/{groupId}/users", method = RequestMethod.POST)
	public ResultListDataRepresentation getGroupUsers(@PathVariable String groupId,
			@RequestBody UserQueryRequest form) {
		if (form == null) {
			throw new BadRequestException("No request found");
		}
		if (form.getSize() <= 0) {
			form.setSize(DEFAULT_PAGE_SIZE);
		}
		if (form.getStart() < 1 || form.getSize() < 1) {
			throw new BadRequestException("參數錯誤");// Collections.EMPTY_LIST;
		}
		EzUserQueryImpl query = new EzUserQueryImpl();
		query.setReadUncommited(true);// 提高查詢效率
		query.setFirstRow(form.getStart());
		query.setLastRow(form.getStart() + form.getSize());
		if (form.getSortBy() == null || form.getSortBy().trim().length() == 0) {
			query.setOrderByColumns("RES.ID");
		} else {
			query.setOrderByColumns(form.getSortBy());
		}
		if (!StringUtils.isEmpty(form.getId())) {
			query.setId(form.getId().trim());
		}
		if (!StringUtils.isEmpty(form.getUsername())) {
			query.setUsername(form.getUsername().trim());
		}
		if (!StringUtils.isEmpty(form.getUsernameLike())) {
			query.setUsernameLike("%" + form.getUsernameLike().trim() + "%");
		}
		if (!StringUtils.isEmpty(form.getGroupId())) {
			query.setGroupId(form.getGroupId().trim());
		}
		List<IEzUser> data = identityService.getUserService().selectByPagingQueryCriteria(query);

		ResultListDataRepresentation result = new ResultListDataRepresentation(data);

		Long totalCount = Long.valueOf(data.size());
		if (data.size() == form.getSize()) {
			totalCount = identityService.getUserService().countByQueryCriteria(query);
			result.setTotal(Long.valueOf(totalCount.intValue()));
		} else {
			if (form.getPage() == 1) {
				result.setTotal(totalCount);
			} else {
				result.setTotal(totalCount + form.getPage() * form.getSize());
			}
		}
		result.setPaginateRequest(form);

		return result;
	}

}
