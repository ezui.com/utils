package com.ezui.idm.app.rest.request;

import com.ezui.app.rest.model.PaginateRequest;

public class GroupQueryRequest extends PaginateRequest{
	private String id;
	private String groupName;
	private String groupNameLike;
	private String groupType;
	private String groupDescription;
	private String groupDescriptionLike;
	private Boolean available;
	private Boolean isPrivate;
	private String groupSource;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupNameLike() {
		return groupNameLike;
	}
	public void setGroupNameLike(String groupNameLike) {
		this.groupNameLike = groupNameLike;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public String getGroupDescriptionLike() {
		return groupDescriptionLike;
	}
	public void setGroupDescriptionLike(String groupDescriptionLike) {
		this.groupDescriptionLike = groupDescriptionLike;
	}
	public String getGroupSource() {
		return groupSource;
	}
	public void setGroupSource(String groupSource) {
		this.groupSource = groupSource;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Boolean getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
}
