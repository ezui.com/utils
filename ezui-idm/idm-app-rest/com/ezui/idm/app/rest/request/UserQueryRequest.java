package com.ezui.idm.app.rest.request;

import java.util.List;

import com.ezui.app.rest.model.PaginateRequest;

public class UserQueryRequest extends PaginateRequest{
	private String id;
	private String loginId;
	private String username;
	private String usernameLike;
	private String defaultDeptName;
	private String defaultDeptNameLike;
	private String defaultDeptNo;
	private String defaultDeptNoLike;
	private String email;
	private String emailLike;
	private String groupId;
	private List<String> groupIds;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsernameLike() {
		return usernameLike;
	}
	public void setUsernameLike(String usernameLike) {
		this.usernameLike = usernameLike;
	}
	public String getDefaultDeptName() {
		return defaultDeptName;
	}
	public void setDefaultDeptName(String defaultDeptName) {
		this.defaultDeptName = defaultDeptName;
	}
	public String getDefaultDeptNameLike() {
		return defaultDeptNameLike;
	}
	public void setDefaultDeptNameLike(String defaultDeptNameLike) {
		this.defaultDeptNameLike = defaultDeptNameLike;
	}
	public String getDefaultDeptNo() {
		return defaultDeptNo;
	}
	public void setDefaultDeptNo(String defaultDeptNo) {
		this.defaultDeptNo = defaultDeptNo;
	}
	public String getDefaultDeptNoLike() {
		return defaultDeptNoLike;
	}
	public void setDefaultDeptNoLike(String defaultDeptNoLike) {
		this.defaultDeptNoLike = defaultDeptNoLike;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailLike() {
		return emailLike;
	}
	public void setEmailLike(String emailLike) {
		this.emailLike = emailLike;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public List<String> getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(List<String> groupIds) {
		this.groupIds = groupIds;
	}
	
}
