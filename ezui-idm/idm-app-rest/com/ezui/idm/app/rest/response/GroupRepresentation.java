package com.ezui.idm.app.rest.response;

import com.ezui.idm.entity.IEzGroup;

public class GroupRepresentation extends AbstractResponse {
	protected String id;
	protected String name;
	protected String type;

	private IEzGroup group;
	public GroupRepresentation() {
	}

	public GroupRepresentation(IEzGroup group) {
		setId(group.getGroupName());
		setName(group.getGroupDisplayName());
		setType(group.getGroupType());
		setGroup(group);
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public IEzGroup getGroup() {
		return group;
	}

	public void setGroup(IEzGroup group) {
		this.group = group;
	}

}
