package com.ezui.idm.app.rest.response;

import java.math.BigDecimal;

import com.ezui.idm.entity.IEzPermission;

public class PermissionRepresentation extends AbstractResponse {
	private BigDecimal id;

	private String name;

	private Integer revision;

	private String aclType;

	private String url;

	private String percode;

	private BigDecimal parentId;

	private String parentIds;

	private String sortNo;

	private String available;
//	private IEzPermission permission;
	public PermissionRepresentation(IEzPermission permission) {
		this.setId(permission.getId());
		this.setName(permission.getName());
		this.setRevision(permission.getRevision());
		this.setAclType(permission.getAclType());
		this.setUrl(permission.getUrl());
		this.setPercode(permission.getPercode());
		this.setParentId(permission.getParentId());
		this.setParentIds(permission.getParentIds());
		this.setSortNo(permission.getSortNo());
		this.setAvailable(permission.getAvailable());
//		this.permission = permission;
	}
//	public IEzPermission getPermission() {
//		return permission;
//	}
//	public void setPermission(IEzPermission permission) {
//		this.permission = permission;
//	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRevision() {
		return revision;
	}
	public void setRevision(Integer revision) {
		this.revision = revision;
	}
	public String getAclType() {
		return aclType;
	}
	public void setAclType(String aclType) {
		this.aclType = aclType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPercode() {
		return percode;
	}
	public void setPercode(String percode) {
		this.percode = percode;
	}
	public BigDecimal getParentId() {
		return parentId;
	}
	public void setParentId(BigDecimal parentId) {
		this.parentId = parentId;
	}
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public String getSortNo() {
		return sortNo;
	}
	public void setSortNo(String sortNo) {
		this.sortNo = sortNo;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}

}
