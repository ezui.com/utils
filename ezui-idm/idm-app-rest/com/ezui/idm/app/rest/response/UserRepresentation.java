package com.ezui.idm.app.rest.response;

import java.util.ArrayList;
import java.util.List;

import com.ezui.idm.entity.IEzUser;

public class UserRepresentation extends AbstractResponse {
	protected String id;
	protected String firstName;
	protected String lastName;
	protected String email;
	protected String fullName;

	private IEzUser user;
	protected List<GroupRepresentation> groups = new ArrayList<GroupRepresentation>();
	protected List<PermissionRepresentation> permissions = new ArrayList<PermissionRepresentation>();
	
	public UserRepresentation() {

	}

	public UserRepresentation(IEzUser user) {
		if (user != null) {
			setId(user.getId());
			setFirstName(user.getFirstName());
			setLastName(user.getLastName());
			setFullName((user.getUsername()));
			setEmail(user.getEmail());
			user.setPassword(null);
			user.setSalt(null);
			this.setUser(user);
		}
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public List<GroupRepresentation> getGroups() {
		return groups;
	}
	public void setGroups(List<GroupRepresentation> groups) {
		this.groups = groups;
	}

	public IEzUser getUser() {
		return user;
	}

	public void setUser(IEzUser user) {
		this.user = user;
	}

	public List<PermissionRepresentation> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionRepresentation> permissions) {
		this.permissions = permissions;
	}

}
