package com.ezui.idm.dao;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.ezui.idm.entity.IEzDepartment;
import com.ezui.idm.entity.EzDepartmentXUser;
import com.ezui.idm.mapping.param.EzDepartmentQueryImpl;
import com.ezui.mybatis.MybatisAbstractDao;

public class EzDepartmentDao extends MybatisAbstractDao implements IEzDepartmentDao {

	@Override
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query, int firstRow, int limit, String orderBy) {
		SqlSession session = this.getSqlSession();
		if (firstRow == -1 || limit == -1) {
			return Collections.EMPTY_LIST;
		}
		query.setFirstRow(firstRow);
		query.setLastRow(firstRow + limit);
		if (orderBy == null || orderBy.trim().length() == 0) {
			query.setOrderByColumns("RES.dept_name asc");
		}
		return session.selectList(EzDepartmentDao.class.getName() + "." + "selectDepartmentByQuery", query);
	}

	@Override
	public long countDepartmentByQuery(EzDepartmentQueryImpl query) {
		SqlSession session = this.getSqlSession();
		return session.selectOne(EzDepartmentDao.class.getName() + "." + "countDepartmentByQuery", query);
	}

	@Override
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query) {
		SqlSession session = this.getSqlSession();
		String orderBy = query.getOrderByClause();
		if (orderBy == null || orderBy.trim().length() == 0) {
			query.setOrderByClause("RES.dept_name asc");
		}
		query.setReadUncommited(true);
		query.setDistinct(true);
		// query.setPrefix("ECM.");
		return session.selectList(EzDepartmentDao.class.getName() + "." + "selectByQueryCriteria", query);
	}
	@Override
	public IEzDepartment findDepartmentByDeptNo(String deptNo) {
		return this.getSqlSession().selectOne(EzDepartmentDao.class.getName() + "." + "selectByDeptNo", deptNo);
	}
	@Override
	public int updateByPrimaryKey(IEzDepartment entity) {
		return this.getSqlSession().update(EzDepartmentDao.class.getName() + "." + "updateByPrimaryKey", entity);
	}
	@Override
	public int insert(IEzDepartment entity) {
		return this.getSqlSession().insert(EzDepartmentDao.class.getName() + "." + "insert", entity);
	}
	@Override
	public int addRelationshipUserXDeparment(EzDepartmentXUser entity) {
		return this.getSqlSession().insert(EzDepartmentDao.class.getName() + "." + "addRelationshipUserXDeparment", entity);
	}
	@Override
	public int deleteRelationshipUserXDeparment(EzDepartmentXUser entity) {
		return this.getSqlSession().delete(EzDepartmentDao.class.getName() + "." + "deleteRelationshipUserXDeparment", entity);
	}
	@Override
	public int setRowLockedDisableFlag(Date rowLockDate, String deptSource) {
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("rowLockDate", rowLockDate);
		if (deptSource != null && deptSource.trim().length() > 0) {
			parameter.put("deptSource", deptSource);
		}
		return getSqlSession().update(EzDepartmentDao.class.getName() + ".setRowLockedDisableFlag", parameter);
	}
	@Override
	public int updateByQueryCriteriaSelective(EzDepartmentQueryImpl query) {
		return getSqlSession().update(EzDepartmentDao.class.getName() + ".updateByQueryCriteriaSelective", query);
	}

}
