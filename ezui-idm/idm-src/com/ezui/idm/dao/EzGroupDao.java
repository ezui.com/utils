package com.ezui.idm.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.ezui.idm.adapter.GroupDataManagerAdapterImpl;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.EzUserXGroup;
import com.ezui.idm.mapping.param.EzGroupQueryImpl;

public class EzGroupDao extends GroupDataManagerAdapterImpl implements IEzGroupDao {

	public EzGroupDao() {

	}
	public EzGroupDao(ProcessEngineConfigurationImpl config) {
		super(config);
	}
	@Override
	public IEzGroup selectByPrimaryKey(String groupId) {
		return getSqlSession().selectOne(EzGroupDao.class.getName() + ".selectByPrimaryKey", groupId);
	}
	@Override
	public IEzGroup findGroupByGroupName(String groupName) {
		return getSqlSession().selectOne(EzGroupDao.class.getName() + ".findGroupByGroupName", groupName);
	}
	@Override
	public List<IEzGroup> selectGroupsByUserId(String userId) {
		return getSqlSession().selectList(EzGroupDao.class.getName() + ".selectGroupsByUserId", userId);
	}
	@Override
	public List<IEzGroup> selectAllGroupsByUserId(String userId) {
		return getSqlSession().selectList(EzGroupDao.class.getName() + ".selectAllGroupsByUserId", userId);
	}
	@Override
	public int updateByPrimaryKey(IEzGroup group) {
		return getSqlSession().update(EzGroupDao.class.getName() + ".updateByPrimaryKey", group);
	}
	@Override
	public int insert(IEzGroup group) {
		return getSqlSession().insert(EzGroupDao.class.getName() + ".insert", group);
	}
	@Override
	public int addRelationshipUserXGroup(EzUserXGroup entity) {
		return getSqlSession().insert(EzGroupDao.class.getName() + ".addRelationshipUserXGroup", entity);
	}
	@Override
	public int deleteRelationshipUserXGroup(EzUserXGroup entity) {
		return getSqlSession().delete(EzGroupDao.class.getName() + ".deleteRelationshipUserXGroup", entity);
	}
	@Override
	public int setRowLockedDisableFlag(Date rowLockDate, String groupSource) {
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("rowLockDate", rowLockDate);
		if (groupSource != null && groupSource.trim().length() > 0) {
			parameter.put("groupSource", groupSource);
		}
		return getSqlSession().update(EzGroupDao.class.getName() + ".setRowLockedDisableFlag", parameter);
	}
	@Override
	public int updateByQueryCriteriaSelective(EzGroupQueryImpl query) {
		return getSqlSession().update(EzGroupDao.class.getName() + ".updateByQueryCriteriaSelective", query);
	}
	@Override
	public List<IEzGroup> selectByPagingQueryCriteria(EzGroupQueryImpl query){
		if(query.getFirstRow()<1) {
			query.setFirstRow(1);
		}
		if(query.getLastRow()<1) {
			query.setLastRow(Integer.MAX_VALUE);
		}
		return getSqlSession().selectList(EzGroupDao.class.getName() + ".selectByPagingQueryCriteria", query);
	}
	@Override
	public Long countByQueryCriteria(EzGroupQueryImpl query){
		return getSqlSession().selectOne(EzGroupDao.class.getName() + ".countByQueryCriteria", query);
	}

}
