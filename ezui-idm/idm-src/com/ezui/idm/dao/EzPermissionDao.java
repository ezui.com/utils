package com.ezui.idm.dao;

import java.util.List;

import com.ezui.idm.entity.IEzPermission;
import com.ezui.mybatis.MybatisAbstractDao;

public class EzPermissionDao extends MybatisAbstractDao implements IEzPermissionDao {
	@Override
	public int insert(IEzPermission entity) {
		return this.getSqlSession().insert(EzPermissionDao.class.getName() + "." + "insert", entity);
	}
	@Override
	public List<IEzPermission> selectPermissionsByUserId(String userId){
		return this.getSqlSession().selectList(EzPermissionDao.class.getName() + "." + "selectPermissionsByUserId", userId);
	}
}
