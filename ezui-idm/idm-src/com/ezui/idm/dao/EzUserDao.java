package com.ezui.idm.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.ezui.idm.adapter.UserDataManagerAdapterImpl;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.entity.EzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;

public class EzUserDao extends UserDataManagerAdapterImpl implements IEzUserDao {

	public EzUserDao() {

	}
	public EzUserDao(ProcessEngineConfigurationImpl config) {
		super(config);
	}
	@Override
	public List<IEzUser> getUsersInBranch(String deptNo) {
		return getSqlSession().selectList(EzUserDao.class.getName() + ".selectUsersInBranch", deptNo);
	}
	@Override
	public List<IEzUser> getUsersXRolesInBranch(String deptNo, String[] roles) {
		EzUserQueryImpl query=new EzUserQueryImpl();
		query.setDeptNo(deptNo);
		query.setRoles(roles);
		return getSqlSession().selectList(EzUserDao.class.getName() + ".selectUsersXRolesInBranch", query);
	}
	@Override
	public IEzUser create() {
		return new EzUser();
	}
	@Override
	public IEzUser findByLoginId(String loginId) {
		return getSqlSession().selectOne(EzUserDao.class.getName() + ".findByLoginId", loginId);
	}
	@Override
	public int updateByQueryCriteriaSelective(EzUserQueryImpl query) {
		return getSqlSession().update(EzUserDao.class.getName() + ".updateByQueryCriteriaSelective", query);
	}
	@Override
	public int insert(IEzUser user) {
		return getSqlSession().insert(EzUserDao.class.getName() + ".insert", user);
	}
	@Override
	public int bulkInsert(List<IEzUser> users) {
		return getSqlSession().insert(EzUserDao.class.getName() + ".bulkInsert", users);
	}
	@Override
	public int updateByPrimaryKey(IEzUser user) {
		return getSqlSession().update(EzUserDao.class.getName() + ".updateByPrimaryKey", user);
	}
	@Override
	public int setRowLockedDisableFlag(Date rowLockDate, String userSource) {
		Map<String, Object> parameter = new HashMap<String, Object>();
//		if (branchNo != null && branchNo.trim().length() > 0) {
//			parameter.put("defaultBranchNo", branchNo);
//		}
		parameter.put("rowLockDate", rowLockDate);
		if (userSource != null && userSource.trim().length() > 0) {
			parameter.put("userSource", userSource);
		}
		return getSqlSession().update(EzUserDao.class.getName() + ".setRowLockedDisableFlag", parameter);
	}
	@Override
	public List<IEzUser> selectByPagingQueryCriteria(EzUserQueryImpl query){
		if(query.getFirstRow()<1) {
			query.setFirstRow(1);
		}
		if(query.getLastRow()<1) {
			query.setLastRow(Integer.MAX_VALUE);
		}
		return getSqlSession().selectList(EzUserDao.class.getName() + ".selectByPagingQueryCriteria", query);
	}
	@Override
	public Long countByQueryCriteria(EzUserQueryImpl query){
		return getSqlSession().selectOne(EzUserDao.class.getName() + ".countByQueryCriteria", query);
	}
	@Override
	public List<IEzUser> selectByQueryCriteria(EzUserQueryImpl query){
		return getSqlSession().selectList(EzUserDao.class.getName() + ".selectByQueryCriteria", query);
	}
	@Override
	public IEzUser findById(String id){
		return getSqlSession().selectOne(EzUserDao.class.getName() + ".selectByPrimaryKey", id);
	}

}
