package com.ezui.idm.dao;

import java.util.Date;
import java.util.List;

import com.ezui.idm.entity.IEzDepartment;
import com.ezui.idm.entity.EzDepartmentXUser;
import com.ezui.idm.mapping.param.EzDepartmentQueryImpl;

public interface IEzDepartmentDao {
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query);
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query, int offset, int limit, String orderBy);
	public long countDepartmentByQuery(EzDepartmentQueryImpl query);
	public IEzDepartment findDepartmentByDeptNo(String deptNo);
	public int updateByPrimaryKey(IEzDepartment entity);
	public int insert(IEzDepartment entity);
	public int addRelationshipUserXDeparment(EzDepartmentXUser entity);
	public int deleteRelationshipUserXDeparment(EzDepartmentXUser entity);
	public int setRowLockedDisableFlag(Date rowLockDate, String deptSource);
	public int updateByQueryCriteriaSelective(EzDepartmentQueryImpl query);
}
