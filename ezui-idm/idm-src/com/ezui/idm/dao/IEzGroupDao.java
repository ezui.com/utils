package com.ezui.idm.dao;

import java.util.Date;
import java.util.List;

import com.ezui.idm.adapter.GroupDataManagerAdapter;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.EzUserXGroup;
import com.ezui.idm.mapping.param.EzGroupQueryImpl;

public interface IEzGroupDao extends GroupDataManagerAdapter {
	public IEzGroup selectByPrimaryKey(String groupId);
	public IEzGroup findGroupByGroupName(String groupName);
	public int updateByPrimaryKey(IEzGroup group);
	public int insert(IEzGroup group);
	public List<IEzGroup> selectGroupsByUserId(String userId);
	public List<IEzGroup> selectAllGroupsByUserId(String userId);
	public int addRelationshipUserXGroup(EzUserXGroup entity);
	public int deleteRelationshipUserXGroup(EzUserXGroup entity);
	public int setRowLockedDisableFlag(Date rowLockDate, String groupSource);
	public int updateByQueryCriteriaSelective(EzGroupQueryImpl query);
	public List<IEzGroup> selectByPagingQueryCriteria(EzGroupQueryImpl query);
	public Long countByQueryCriteria(EzGroupQueryImpl query);
}
