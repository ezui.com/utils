package com.ezui.idm.dao;

import java.util.List;

import com.ezui.idm.entity.IEzPermission;

public interface IEzPermissionDao {
	public int insert(IEzPermission entity);
	public List<IEzPermission> selectPermissionsByUserId(String userId);
}
