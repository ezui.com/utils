package com.ezui.idm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import com.ezui.idm.entity.EzPersistentToken;

public interface IEzPersistentTokenRepository extends JpaRepository<EzPersistentToken, String> {

	List<EzPersistentToken> findByUser(String user);

	List<EzPersistentToken> findByTokenDateBefore(Date date);

	@Modifying
	Long deleteByTokenDateBefore(Date date);
}
