package com.ezui.idm.dao;

import java.util.Date;
import java.util.List;

import com.ezui.idm.adapter.UserDataManagerAdapter;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;

public interface IEzUserDao extends UserDataManagerAdapter {
	public List<IEzUser> getUsersInBranch(String deptNo);
	public List<IEzUser> getUsersXRolesInBranch(String deptNo, String[] roles);
	public IEzUser findByLoginId(String loginId);
	public int updateByQueryCriteriaSelective(EzUserQueryImpl query);
	public int insert(IEzUser user);
	public int bulkInsert(List<IEzUser> users);
	public int updateByPrimaryKey(IEzUser user);
	public int setRowLockedDisableFlag(Date rowLockDate, String userSource);
	public List<IEzUser> selectByPagingQueryCriteria(EzUserQueryImpl query);
	public Long countByQueryCriteria(EzUserQueryImpl query);
	public List<IEzUser> selectByQueryCriteria(EzUserQueryImpl query);
}
