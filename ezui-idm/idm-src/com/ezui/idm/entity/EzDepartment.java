package com.ezui.idm.entity;

import java.io.Serializable;
import java.util.Date;

public class EzDepartment implements IEzDepartment, Serializable {
	private static final long serialVersionUID = -5681862434630354300L;

	private String id;

    private String deptNo;

    private String deptName;

    private int revision;

    private String deptType;

    private String deptDescription;

    private String parentId;

    private String parentIds;

    private String defaultRoles;

    private String available;

    private Date createdDate;

    private Date modifiedDate;

    private String createdBy;

    private String modifiedBy;

    private String telNo;

    private String faxNo;

    private String address;
    private Date rowLockDate;

    private String deptSource;

    @Override
	public String getId() {
        return id;
    }

    @Override
	public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Override
	public String getDeptNo() {
        return deptNo;
    }

    @Override
	public void setDeptNo(String deptNo) {
        this.deptNo = deptNo == null ? null : deptNo.trim();
    }

    @Override
	public String getDeptName() {
        return deptName;
    }

    @Override
	public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    @Override
	public int getRevision() {
        return revision;
    }

    @Override
	public void setRevision(int revision) {
        this.revision = revision;
    }

    @Override
	public String getDeptType() {
        return deptType;
    }

    @Override
	public void setDeptType(String deptType) {
        this.deptType = deptType == null ? null : deptType.trim();
    }

    @Override
	public String getDeptDescription() {
        return deptDescription;
    }

    @Override
	public void setDeptDescription(String deptDescription) {
        this.deptDescription = deptDescription == null ? null : deptDescription.trim();
    }

    @Override
	public String getParentId() {
        return parentId;
    }

    @Override
	public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    @Override
	public String getParentIds() {
        return parentIds;
    }

    @Override
	public void setParentIds(String parentIds) {
        this.parentIds = parentIds == null ? null : parentIds.trim();
    }

    @Override
	public String getDefaultRoles() {
        return defaultRoles;
    }

    @Override
	public void setDefaultRoles(String defaultRoles) {
        this.defaultRoles = defaultRoles == null ? null : defaultRoles.trim();
    }

    @Override
	public String getAvailable() {
        return available;
    }

    @Override
	public void setAvailable(String available) {
        this.available = available == null ? null : available.trim();
    }

    @Override
	public Date getCreatedDate() {
        return createdDate;
    }

    @Override
	public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
	public Date getModifiedDate() {
        return modifiedDate;
    }

    @Override
	public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
	public String getCreatedBy() {
        return createdBy;
    }

    @Override
	public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    @Override
	public String getModifiedBy() {
        return modifiedBy;
    }

    @Override
	public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }

    @Override
	public String getTelNo() {
        return telNo;
    }

    @Override
	public void setTelNo(String telNo) {
        this.telNo = telNo == null ? null : telNo.trim();
    }

    @Override
	public String getFaxNo() {
        return faxNo;
    }

    @Override
	public void setFaxNo(String faxNo) {
        this.faxNo = faxNo == null ? null : faxNo.trim();
    }

    @Override
	public String getAddress() {
        return address;
    }

    @Override
	public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }
    public Date getRowLockDate() {
        return rowLockDate;
    }

    public void setRowLockDate(Date rowLockDate) {
        this.rowLockDate = rowLockDate;
    }

    public String getDeptSource() {
        return deptSource;
    }

    public void setDeptSource(String deptSource) {
        this.deptSource = deptSource == null ? null : deptSource.trim();
    }

}
