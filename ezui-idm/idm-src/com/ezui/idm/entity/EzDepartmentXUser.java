package com.ezui.idm.entity;

import java.io.Serializable;

public class EzDepartmentXUser implements Serializable {
	private static final long serialVersionUID = -7959794061372566561L;

	private String deptId;

	private String userId;

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId == null ? null : deptId.trim();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}
	private String titleCode;

	private String jobTitle;

	public String getTitleCode() {
		return titleCode;
	}

	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode == null ? null : titleCode.trim();
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle == null ? null : jobTitle.trim();
	}
}