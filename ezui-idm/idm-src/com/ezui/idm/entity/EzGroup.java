package com.ezui.idm.entity;

import java.util.Date;

import com.ezui.idm.adapter.GroupEntityAdapterImpl;

public class EzGroup extends GroupEntityAdapterImpl implements IEzGroup{

	protected static final long serialVersionUID = -8915891056319850279L;
    protected String id;

    protected String groupName;

    protected int revision;

    protected String groupType;

    protected String groupDescription;

    protected String available;

    protected Date createdDate;

    protected Date modifiedDate;

    protected String createdBy;

    protected String modifiedBy;

    protected String ownerName;

    protected String isPrivate;

    protected String groupAdmin;

    protected String groupDisplayName;
    protected Date rowLockDate;

    protected String groupSource;

    @Override
	public String getId() {
        return id;
    }

    @Override
	public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Override
	public String getGroupName() {
        return groupName;
    }

    @Override
	public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    @Override
	public int getRevision() {
        return revision;
    }

    @Override
	public void setRevision(int revision) {
        this.revision = revision;
    }

    @Override
	public String getGroupType() {
        return groupType;
    }

    @Override
	public void setGroupType(String groupType) {
        this.groupType = groupType == null ? null : groupType.trim();
    }

    @Override
	public String getGroupDescription() {
        return groupDescription;
    }

    @Override
	public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription == null ? null : groupDescription.trim();
    }

    @Override
	public String getAvailable() {
        return available;
    }

    @Override
	public void setAvailable(String available) {
        this.available = available == null ? null : available.trim();
    }

    @Override
	public Date getCreatedDate() {
        return createdDate;
    }

    @Override
	public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
	public Date getModifiedDate() {
        return modifiedDate;
    }

    @Override
	public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
	public String getCreatedBy() {
        return createdBy;
    }

    @Override
	public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    @Override
	public String getModifiedBy() {
        return modifiedBy;
    }

    @Override
	public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }

    @Override
	public String getOwnerName() {
        return ownerName;
    }

    @Override
	public void setOwnerName(String ownerName) {
        this.ownerName = ownerName == null ? null : ownerName.trim();
    }

    @Override
	public String getIsPrivate() {
        return isPrivate;
    }

    @Override
	public void setIsPrivate(String isPrivate) {
        this.isPrivate = isPrivate == null ? null : isPrivate.trim();
    }

    @Override
	public String getGroupAdmin() {
        return groupAdmin;
    }

    @Override
	public void setGroupAdmin(String groupAdmin) {
        this.groupAdmin = groupAdmin == null ? null : groupAdmin.trim();
    }

    @Override
	public String getGroupDisplayName() {
        return groupDisplayName;
    }

    @Override
	public void setGroupDisplayName(String groupDisplayName) {
        this.groupDisplayName = groupDisplayName == null ? null : groupDisplayName.trim();
    }
    public Date getRowLockDate() {
        return rowLockDate;
    }

    public void setRowLockDate(Date rowLockDate) {
        this.rowLockDate = rowLockDate;
    }

    public String getGroupSource() {
        return groupSource;
    }

    public void setGroupSource(String groupSource) {
        this.groupSource = groupSource == null ? null : groupSource.trim();
    }

}
