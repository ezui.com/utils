package com.ezui.idm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EzPermission implements Serializable, IEzPermission {
	private static final long serialVersionUID = 9012406680439324690L;

	private BigDecimal id;

	private String name;

	private Integer revision;

	private String aclType;

	private String url;

	private String percode;

	private BigDecimal parentId;

	private String parentIds;

	private String sortNo;

	private String available;

	private Date createdDate;

	private Date modifiedDate;

	private String createdBy;

	private String modifiedBy;

	@Override
	public BigDecimal getId() {
		return id;
	}

	@Override
	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	@Override
	public Integer getRevision() {
		return revision;
	}

	@Override
	public void setRevision(Integer revision) {
		this.revision = revision;
	}

	@Override
	public String getAclType() {
		return aclType;
	}

	@Override
	public void setAclType(String aclType) {
		this.aclType = aclType == null ? null : aclType.trim();
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	@Override
	public String getPercode() {
		return percode;
	}

	@Override
	public void setPercode(String percode) {
		this.percode = percode == null ? null : percode.trim();
	}

	@Override
	public BigDecimal getParentId() {
		return parentId;
	}

	@Override
	public void setParentId(BigDecimal parentId) {
		this.parentId = parentId;
	}

	@Override
	public String getParentIds() {
		return parentIds;
	}

	@Override
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds == null ? null : parentIds.trim();
	}

	@Override
	public String getSortNo() {
		return sortNo;
	}

	@Override
	public void setSortNo(String sortNo) {
		this.sortNo = sortNo == null ? null : sortNo.trim();
	}

	@Override
	public String getAvailable() {
		return available;
	}

	@Override
	public void setAvailable(String available) {
		this.available = available == null ? null : available.trim();
	}

	@Override
	public Date getCreatedDate() {
		return createdDate;
	}

	@Override
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public Date getModifiedDate() {
		return modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy == null ? null : createdBy.trim();
	}

	@Override
	public String getModifiedBy() {
		return modifiedBy;
	}

	@Override
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
	}

}
