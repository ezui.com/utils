package com.ezui.idm.entity;

import java.util.Date;

import com.ezui.idm.adapter.UserEntityAdapterImpl;

public class EzUser extends UserEntityAdapterImpl implements IEzUser {
	protected static final long serialVersionUID = -230520218587410774L;
    protected String id;

    protected String loginId;

    protected String username;

    protected int revision;

    protected String password;

    protected String salt;

    protected String email;

    protected String defaultTitleCode;

    protected String defaultJobTitle;

    protected String defaultDeptNo;

    protected String defaultDeptName;

    protected String pictureId;

    protected String locked;

    protected String available;

    protected Date createdDate;

    protected Date modifiedDate;

    protected String createdBy;

    protected String modifiedBy;

    protected String userSource;

    protected String workflowDisabled;

    protected String userLdapDn;

    protected String userDelegation;

    protected Integer failedAuthAttempt;

    protected String userDescription;

    protected String defaultBranchNo;

    protected String defaultBranchName;

    protected Date rowLockDate;

    @Override
	public String getId() {
        return id;
    }

    @Override
	public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Override
	public String getLoginId() {
        return loginId;
    }

    @Override
	public void setLoginId(String loginId) {
        this.loginId = loginId == null ? null : loginId.trim();
    }

    @Override
	public String getUsername() {
        return username;
    }

    @Override
	public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    @Override
	public int getRevision() {
        return revision;
    }

    @Override
	public void setRevision(int revision) {
        this.revision = revision;
    }

    @Override
	public String getPassword() {
        return password;
    }

    @Override
	public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    @Override
	public String getSalt() {
        return salt;
    }

    @Override
	public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    @Override
	public String getEmail() {
        return email;
    }

    @Override
	public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    @Override
	public String getDefaultTitleCode() {
        return defaultTitleCode;
    }

    @Override
	public void setDefaultTitleCode(String defaultTitleCode) {
        this.defaultTitleCode = defaultTitleCode == null ? null : defaultTitleCode.trim();
    }

    @Override
	public String getDefaultJobTitle() {
        return defaultJobTitle;
    }

    @Override
	public void setDefaultJobTitle(String defaultJobTitle) {
        this.defaultJobTitle = defaultJobTitle == null ? null : defaultJobTitle.trim();
    }

    @Override
	public String getDefaultDeptNo() {
        return defaultDeptNo;
    }

    @Override
	public void setDefaultDeptNo(String defaultDeptNo) {
        this.defaultDeptNo = defaultDeptNo == null ? null : defaultDeptNo.trim();
    }

    @Override
	public String getDefaultDeptName() {
        return defaultDeptName;
    }

    @Override
	public void setDefaultDeptName(String defaultDeptName) {
        this.defaultDeptName = defaultDeptName == null ? null : defaultDeptName.trim();
    }

    @Override
	public String getPictureId() {
        return pictureId;
    }

    @Override
	public void setPictureId(String pictureId) {
        this.pictureId = pictureId == null ? null : pictureId.trim();
    }

    @Override
	public String getLocked() {
        return locked;
    }

    @Override
	public void setLocked(String locked) {
        this.locked = locked == null ? null : locked.trim();
    }

    @Override
	public String getAvailable() {
        return available;
    }

    @Override
	public void setAvailable(String available) {
        this.available = available == null ? null : available.trim();
    }

    @Override
	public Date getCreatedDate() {
        return createdDate;
    }

    @Override
	public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
	public Date getModifiedDate() {
        return modifiedDate;
    }

    @Override
	public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
	public String getCreatedBy() {
        return createdBy;
    }

    @Override
	public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    @Override
	public String getModifiedBy() {
        return modifiedBy;
    }

    @Override
	public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }

    @Override
	public String getUserSource() {
        return userSource;
    }

    @Override
	public void setUserSource(String userSource) {
        this.userSource = userSource == null ? null : userSource.trim();
    }

    @Override
	public String getWorkflowDisabled() {
        return workflowDisabled;
    }

    @Override
	public void setWorkflowDisabled(String workflowDisabled) {
        this.workflowDisabled = workflowDisabled == null ? null : workflowDisabled.trim();
    }

    @Override
	public String getUserLdapDn() {
        return userLdapDn;
    }

    @Override
	public void setUserLdapDn(String userLdapDn) {
        this.userLdapDn = userLdapDn == null ? null : userLdapDn.trim();
    }

    @Override
	public String getUserDelegation() {
        return userDelegation;
    }

    @Override
	public void setUserDelegation(String userDelegation) {
        this.userDelegation = userDelegation == null ? null : userDelegation.trim();
    }

    @Override
	public Integer getFailedAuthAttempt() {
        return failedAuthAttempt;
    }

    @Override
	public void setFailedAuthAttempt(Integer failedAuthAttempt) {
        this.failedAuthAttempt = failedAuthAttempt;
    }

    @Override
	public String getUserDescription() {
        return userDescription;
    }

    @Override
	public void setUserDescription(String userDescription) {
        this.userDescription = userDescription == null ? null : userDescription.trim();
    }

    @Override
	public String getDefaultBranchNo() {
        return defaultBranchNo;
    }

    @Override
	public void setDefaultBranchNo(String defaultBranchNo) {
        this.defaultBranchNo = defaultBranchNo == null ? null : defaultBranchNo.trim();
    }

    @Override
	public String getDefaultBranchName() {
        return defaultBranchName;
    }

    @Override
	public void setDefaultBranchName(String defaultBranchName) {
        this.defaultBranchName = defaultBranchName == null ? null : defaultBranchName.trim();
    }

    @Override
	public Date getRowLockDate() {
        return rowLockDate;
    }

    @Override
	public void setRowLockDate(Date rowLockDate) {
        this.rowLockDate = rowLockDate;
    }
}
