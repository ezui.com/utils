package com.ezui.idm.entity;

import java.io.Serializable;

public class EzUserXGroup implements Serializable {
	private static final long serialVersionUID = 1792960729776925378L;

	private String userId;

	private String groupId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId == null ? null : groupId.trim();
	}
}