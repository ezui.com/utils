package com.ezui.idm.entity;

import java.util.Date;

public interface IEzDepartment{

	public abstract String getId();

	public abstract void setId(String id);

	public abstract String getDeptNo();

	public abstract void setDeptNo(String deptNo);

	public abstract String getDeptName();

	public abstract void setDeptName(String deptName);

	public abstract int getRevision();

	public abstract void setRevision(int revision);

	public abstract String getDeptType();

	public abstract void setDeptType(String deptType);

	public abstract String getDeptDescription();

	public abstract void setDeptDescription(String deptDescription);

	public abstract String getParentId();

	public abstract void setParentId(String parentId);

	public abstract String getParentIds();

	public abstract void setParentIds(String parentIds);

	public abstract String getDefaultRoles();

	public abstract void setDefaultRoles(String defaultRoles);

	public abstract String getAvailable();

	public abstract void setAvailable(String available);

	public abstract Date getCreatedDate();

	public abstract void setCreatedDate(Date createdDate);

	public abstract Date getModifiedDate();

	public abstract void setModifiedDate(Date modifiedDate);

	public abstract String getCreatedBy();

	public abstract void setCreatedBy(String createdBy);

	public abstract String getModifiedBy();

	public abstract void setModifiedBy(String modifiedBy);

	public abstract String getTelNo();

	public abstract void setTelNo(String telNo);

	public abstract String getFaxNo();

	public abstract void setFaxNo(String faxNo);

	public abstract String getAddress();

	public abstract void setAddress(String address);
    public Date getRowLockDate();

    public void setRowLockDate(Date rowLockDate);

    public String getDeptSource();

    public void setDeptSource(String deptSource);

}