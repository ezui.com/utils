package com.ezui.idm.entity;

import java.util.Date;

import com.ezui.idm.adapter.GroupEntityAdapter;

public interface IEzGroup extends GroupEntityAdapter{

	public abstract String getId();

	public abstract void setId(String id);

	public abstract String getGroupName();

	public abstract void setGroupName(String groupName);

	public abstract int getRevision();

	public abstract void setRevision(int revision);

	public abstract String getGroupType();

	public abstract void setGroupType(String groupType);

	public abstract String getGroupDescription();

	public abstract void setGroupDescription(String groupDescription);

	public abstract String getAvailable();

	public abstract void setAvailable(String available);

	public abstract Date getCreatedDate();

	public abstract void setCreatedDate(Date createdDate);

	public abstract Date getModifiedDate();

	public abstract void setModifiedDate(Date modifiedDate);

	public abstract String getCreatedBy();

	public abstract void setCreatedBy(String createdBy);

	public abstract String getModifiedBy();

	public abstract void setModifiedBy(String modifiedBy);

	public abstract String getOwnerName();

	public abstract void setOwnerName(String ownerName);

	public abstract String getIsPrivate();

	public abstract void setIsPrivate(String isPrivate);

	public abstract String getGroupAdmin();

	public abstract void setGroupAdmin(String groupAdmin);

	public abstract String getGroupDisplayName();

	public abstract void setGroupDisplayName(String groupDisplayName);
    public Date getRowLockDate();

    public void setRowLockDate(Date rowLockDate);

    public String getGroupSource();

    public void setGroupSource(String groupSource);

}