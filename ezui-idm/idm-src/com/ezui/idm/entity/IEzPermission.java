package com.ezui.idm.entity;

import java.math.BigDecimal;
import java.util.Date;

public interface IEzPermission {

	public abstract BigDecimal getId();

	public abstract void setId(BigDecimal id);

	public abstract String getName();

	public abstract void setName(String name);

	public abstract Integer getRevision();

	public abstract void setRevision(Integer revision);

	public abstract String getAclType();

	public abstract void setAclType(String aclType);

	public abstract String getUrl();

	public abstract void setUrl(String url);

	public abstract String getPercode();

	public abstract void setPercode(String percode);

	public abstract BigDecimal getParentId();

	public abstract void setParentId(BigDecimal parentId);

	public abstract String getParentIds();

	public abstract void setParentIds(String parentIds);

	public abstract String getSortNo();

	public abstract void setSortNo(String sortNo);

	public abstract String getAvailable();

	public abstract void setAvailable(String available);

	public abstract Date getCreatedDate();

	public abstract void setCreatedDate(Date createdDate);

	public abstract Date getModifiedDate();

	public abstract void setModifiedDate(Date modifiedDate);

	public abstract String getCreatedBy();

	public abstract void setCreatedBy(String createdBy);

	public abstract String getModifiedBy();

	public abstract void setModifiedBy(String modifiedBy);

}