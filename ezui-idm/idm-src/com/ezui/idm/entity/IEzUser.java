package com.ezui.idm.entity;

import java.util.Date;

import com.ezui.idm.adapter.UserEntityAdapter;
import com.ezui.security.spring.UserSource;

public interface IEzUser extends UserEntityAdapter, UserSource{

	public abstract String getId();

	public abstract void setId(String id);

	public abstract String getLoginId();

	public abstract void setLoginId(String loginId);

	public abstract String getUsername();

	public abstract void setUsername(String username);

	public abstract int getRevision();

	public abstract void setRevision(int revision);

	public abstract String getPassword();

	public abstract void setPassword(String password);

	public abstract String getSalt();

	public abstract void setSalt(String salt);

	public abstract String getEmail();

	public abstract void setEmail(String email);

	public abstract String getDefaultTitleCode();

	public abstract void setDefaultTitleCode(String defaultTitleCode);

	public abstract String getDefaultJobTitle();

	public abstract void setDefaultJobTitle(String defaultJobTitle);

	public abstract String getDefaultDeptNo();

	public abstract void setDefaultDeptNo(String defaultDeptNo);

	public abstract String getDefaultDeptName();

	public abstract void setDefaultDeptName(String defaultDeptName);

	public abstract String getPictureId();

	public abstract void setPictureId(String pictureId);

	public abstract String getLocked();

	public abstract void setLocked(String locked);

	public abstract String getAvailable();

	public abstract void setAvailable(String available);

	public abstract Date getCreatedDate();

	public abstract void setCreatedDate(Date createdDate);

	public abstract Date getModifiedDate();

	public abstract void setModifiedDate(Date modifiedDate);

	public abstract String getCreatedBy();

	public abstract void setCreatedBy(String createdBy);

	public abstract String getModifiedBy();

	public abstract void setModifiedBy(String modifiedBy);

	public abstract String getUserSource();

	public abstract void setUserSource(String userSource);

	public abstract String getWorkflowDisabled();

	public abstract void setWorkflowDisabled(String workflowDisabled);

	public abstract String getUserLdapDn();

	public abstract void setUserLdapDn(String userLdapDn);

	public abstract String getUserDelegation();

	public abstract void setUserDelegation(String userDelegation);

	public abstract Integer getFailedAuthAttempt();

	public abstract void setFailedAuthAttempt(Integer failedAuthAttempt);

	public abstract String getUserDescription();

	public abstract void setUserDescription(String userDescription);

	public abstract String getDefaultBranchNo();

	public abstract void setDefaultBranchNo(String defaultBranchNo);

	public abstract String getDefaultBranchName();

	public abstract void setDefaultBranchName(String defaultBranchName);

	public abstract Date getRowLockDate();

	public abstract void setRowLockDate(Date rowLockDate);

}