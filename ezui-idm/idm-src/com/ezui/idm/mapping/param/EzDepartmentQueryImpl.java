package com.ezui.idm.mapping.param;

import java.util.Date;
import java.util.List;

import com.ezui.dao.param.ListQueryParameterObject;
import com.ezui.idm.entity.IEzDepartment;

public class EzDepartmentQueryImpl extends ListQueryParameterObject {
	private static final long serialVersionUID = 6352042327418645786L;

	private String id;
	private String userId;
	private String deptNo;
    private String deptSource;
	private String deptName;
	private String deptNameLike;
    private boolean rowLockDateIsNull=false;
    private Date rowLockDate;
	private Integer revision;

	private String deptType;
	private List<String> deptTypes;

	private String deptDescription;
	private String deptDescriptionLike;

	private String parentId;
	private String parentIdIsNull;
	private String available;

	private Date createdDate;
	private Date createdDateGreaterEqual;
	private Date createdDateLessEqual;
	private Date createdDateGreater;
	private Date createdDateLess;

	private Date modifiedDate;
	private Date modifiedDateGreaterEqual;
	private Date modifiedDateLessEqual;
	private Date modifiedDateGreater;
	private Date modifiedDateLess;

	private String createdBy;
	private String createdByLike;

	private String modifiedBy;
	private String modifiedByLike;
	private IEzDepartment record;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptNameLike() {
		return deptNameLike;
	}
	public void setDeptNameLike(String deptNameLike) {
		this.deptNameLike = deptNameLike;
	}
	public Integer getRevision() {
		return revision;
	}
	public void setRevision(Integer revision) {
		this.revision = revision;
	}
	public String getDeptType() {
		return deptType;
	}
	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}
	public List<String> getDeptTypes() {
		return deptTypes;
	}
	public void setDeptTypes(List<String> deptTypes) {
		this.deptTypes = deptTypes;
	}
	public String getDeptDescription() {
		return deptDescription;
	}
	public void setDeptDescription(String deptDescription) {
		this.deptDescription = deptDescription;
	}
	public String getDeptDescriptionLike() {
		return deptDescriptionLike;
	}
	public void setDeptDescriptionLike(String deptDescriptionLike) {
		this.deptDescriptionLike = deptDescriptionLike;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentIdIsNull() {
		return parentIdIsNull;
	}
	public void setParentIdIsNull(String parentIdIsNull) {
		this.parentIdIsNull = parentIdIsNull;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getCreatedDateGreaterEqual() {
		return createdDateGreaterEqual;
	}
	public void setCreatedDateGreaterEqual(Date createdDateGreaterEqual) {
		this.createdDateGreaterEqual = createdDateGreaterEqual;
	}
	public Date getCreatedDateLessEqual() {
		return createdDateLessEqual;
	}
	public void setCreatedDateLessEqual(Date createdDateLessEqual) {
		this.createdDateLessEqual = createdDateLessEqual;
	}
	public Date getCreatedDateGreater() {
		return createdDateGreater;
	}
	public void setCreatedDateGreater(Date createdDateGreater) {
		this.createdDateGreater = createdDateGreater;
	}
	public Date getCreatedDateLess() {
		return createdDateLess;
	}
	public void setCreatedDateLess(Date createdDateLess) {
		this.createdDateLess = createdDateLess;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Date getModifiedDateGreaterEqual() {
		return modifiedDateGreaterEqual;
	}
	public void setModifiedDateGreaterEqual(Date modifiedDateGreaterEqual) {
		this.modifiedDateGreaterEqual = modifiedDateGreaterEqual;
	}
	public Date getModifiedDateLessEqual() {
		return modifiedDateLessEqual;
	}
	public void setModifiedDateLessEqual(Date modifiedDateLessEqual) {
		this.modifiedDateLessEqual = modifiedDateLessEqual;
	}
	public Date getModifiedDateGreater() {
		return modifiedDateGreater;
	}
	public void setModifiedDateGreater(Date modifiedDateGreater) {
		this.modifiedDateGreater = modifiedDateGreater;
	}
	public Date getModifiedDateLess() {
		return modifiedDateLess;
	}
	public void setModifiedDateLess(Date modifiedDateLess) {
		this.modifiedDateLess = modifiedDateLess;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedByLike() {
		return createdByLike;
	}
	public void setCreatedByLike(String createdByLike) {
		this.createdByLike = createdByLike;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedByLike() {
		return modifiedByLike;
	}
	public void setModifiedByLike(String modifiedByLike) {
		this.modifiedByLike = modifiedByLike;
	}
	public IEzDepartment getRecord() {
		return record;
	}
	public void setRecord(IEzDepartment record) {
		this.record = record;
	}
	public String getDeptSource() {
		return deptSource;
	}
	public void setDeptSource(String deptSource) {
		this.deptSource = deptSource;
	}
	public boolean isRowLockDateIsNull() {
		return rowLockDateIsNull;
	}
	public void setRowLockDateIsNull(boolean rowLockDateIsNull) {
		this.rowLockDateIsNull = rowLockDateIsNull;
	}
	public Date getRowLockDate() {
		return rowLockDate;
	}
	public void setRowLockDate(Date rowLockDate) {
		this.rowLockDate = rowLockDate;
	}
	
}
