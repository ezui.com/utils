package com.ezui.idm.mapping.param;

import java.util.Date;

public interface EzGroupQuery {
	public String getId();
	public void setId(String id);
	public String getGroupName();
	public void setGroupName(String groupName);
	public String getGroupNameLike();
	public void setGroupNameLike(String groupNameLike);
	public String getGroupType();
	public void setGroupType(String groupType);
	public String getGroupDescription();
	public void setGroupDescription(String groupDescription);
	public String getGroupDescriptionLike();
	public void setGroupDescriptionLike(String groupDescriptionLike);
	public Date getCreatedDate();
	public void setCreatedDate(Date createdDate);
	public Date getCreatedDateGreaterEqual();
	public void setCreatedDateGreaterEqual(Date createdDateGreaterEqual);
	public Date getCreatedDateLessEqual();
	public void setCreatedDateLessEqual(Date createdDateLessEqual);
	public Date getCreatedDateGreater();
	public void setCreatedDateGreater(Date createdDateGreater);
	public Date getCreatedDateLess();
	public void setCreatedDateLess(Date createdDateLess);
	public Date getModifiedDate();
	public void setModifiedDate(Date modifiedDate);
	public Date getModifiedDateGreaterEqual();
	public void setModifiedDateGreaterEqual(Date modifiedDateGreaterEqual);
	public Date getModifiedDateLessEqual();
	public void setModifiedDateLessEqual(Date modifiedDateLessEqual);
	public Date getModifiedDateGreater();
	public void setModifiedDateGreater(Date modifiedDateGreater);
	public Date getModifiedDateLess();
	public void setModifiedDateLess(Date modifiedDateLess);
	public String getCreatedBy();
	public void setCreatedBy(String createdBy);
	public String getCreatedByLike();
	public void setCreatedByLike(String createdByLike);
	public String getModifiedBy();
	public void setModifiedBy(String modifiedBy);
	public String getModifiedByLike();
	public void setModifiedByLike(String modifiedByLike);
	
	public String getUserId();
	public void setUserId(String userId);
	public boolean isDistinct();
	public void setDistinct(boolean distinct);
	public String getOrderByColumns();
	public void setOrderByColumns(String orderByColumns);
}
