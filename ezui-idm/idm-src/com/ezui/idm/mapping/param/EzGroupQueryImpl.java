package com.ezui.idm.mapping.param;

import java.util.Date;

import org.activiti.engine.impl.interceptor.CommandExecutor;

import com.ezui.idm.adapter.GroupQueryAdapterImpl;
import com.ezui.idm.entity.IEzGroup;

public class EzGroupQueryImpl extends GroupQueryAdapterImpl implements EzGroupQuery{

	private static final long serialVersionUID = -6700462168422130551L;
	public EzGroupQueryImpl() {
	}
	public EzGroupQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}
    private String id;
    private String groupSource;
    private String groupName;
    private String groupNameLike;
    private boolean rowLockDateIsNull=false;
    private Date rowLockDate;
    private String groupType;

    private String groupDescription;
	private String groupDescriptionLike;

	private Boolean available;
	private Boolean isPrivate;

    private Date createdDate;
    private Date createdDateGreaterEqual;
    private Date createdDateLessEqual;
    private Date createdDateGreater;
    private Date createdDateLess;

    private Date modifiedDate;
    private Date modifiedDateGreaterEqual;
    private Date modifiedDateLessEqual;
    private Date modifiedDateGreater;
    private Date modifiedDateLess;

    private String createdBy;
    private String createdByLike;

    private String modifiedBy;
    private String modifiedByLike;
    
	protected boolean distinct=false;
    protected String orderByColumns;
	protected boolean readUncommited=false;
	protected int firstRow;
	protected int lastRow;

	private IEzGroup record;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupNameLike() {
		return groupNameLike;
	}
	public void setGroupNameLike(String groupNameLike) {
		this.groupNameLike = groupNameLike;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public String getGroupDescriptionLike() {
		return groupDescriptionLike;
	}
	public void setGroupDescriptionLike(String groupDescriptionLike) {
		this.groupDescriptionLike = groupDescriptionLike;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getCreatedDateGreaterEqual() {
		return createdDateGreaterEqual;
	}
	public void setCreatedDateGreaterEqual(Date createdDateGreaterEqual) {
		this.createdDateGreaterEqual = createdDateGreaterEqual;
	}
	public Date getCreatedDateLessEqual() {
		return createdDateLessEqual;
	}
	public void setCreatedDateLessEqual(Date createdDateLessEqual) {
		this.createdDateLessEqual = createdDateLessEqual;
	}
	public Date getCreatedDateGreater() {
		return createdDateGreater;
	}
	public void setCreatedDateGreater(Date createdDateGreater) {
		this.createdDateGreater = createdDateGreater;
	}
	public Date getCreatedDateLess() {
		return createdDateLess;
	}
	public void setCreatedDateLess(Date createdDateLess) {
		this.createdDateLess = createdDateLess;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Date getModifiedDateGreaterEqual() {
		return modifiedDateGreaterEqual;
	}
	public void setModifiedDateGreaterEqual(Date modifiedDateGreaterEqual) {
		this.modifiedDateGreaterEqual = modifiedDateGreaterEqual;
	}
	public Date getModifiedDateLessEqual() {
		return modifiedDateLessEqual;
	}
	public void setModifiedDateLessEqual(Date modifiedDateLessEqual) {
		this.modifiedDateLessEqual = modifiedDateLessEqual;
	}
	public Date getModifiedDateGreater() {
		return modifiedDateGreater;
	}
	public void setModifiedDateGreater(Date modifiedDateGreater) {
		this.modifiedDateGreater = modifiedDateGreater;
	}
	public Date getModifiedDateLess() {
		return modifiedDateLess;
	}
	public void setModifiedDateLess(Date modifiedDateLess) {
		this.modifiedDateLess = modifiedDateLess;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedByLike() {
		return createdByLike;
	}
	public void setCreatedByLike(String createdByLike) {
		this.createdByLike = createdByLike;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedByLike() {
		return modifiedByLike;
	}
	public void setModifiedByLike(String modifiedByLike) {
		this.modifiedByLike = modifiedByLike;
	}
	
	private String userId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public boolean isDistinct() {
		return distinct;
	}
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}
	public String getOrderByColumns() {
		if (orderByColumns == null) {
			orderByColumns = "RES.id asc";
		}
		return orderByColumns;
	}
	public void setOrderByColumns(String orderByColumns) {
		this.orderByColumns = orderByColumns;
	}
	public IEzGroup getRecord() {
		return record;
	}
	public void setRecord(IEzGroup record) {
		this.record = record;
	}
	public String getGroupSource() {
		return groupSource;
	}
	public void setGroupSource(String groupSource) {
		this.groupSource = groupSource;
	}
	public boolean isRowLockDateIsNull() {
		return rowLockDateIsNull;
	}
	public void setRowLockDateIsNull(boolean rowLockDateIsNull) {
		this.rowLockDateIsNull = rowLockDateIsNull;
	}
	public Date getRowLockDate() {
		return rowLockDate;
	}
	public void setRowLockDate(Date rowLockDate) {
		this.rowLockDate = rowLockDate;
	}
	public boolean isReadUncommited() {
		return readUncommited;
	}
	public void setReadUncommited(boolean readUncommited) {
		this.readUncommited = readUncommited;
	}
	public int getFirstRow() {
		return firstRow;
	}
	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}
	public int getLastRow() {
		return lastRow;
	}
	public void setLastRow(int lastRow) {
		this.lastRow = lastRow;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Boolean getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
}
