package com.ezui.idm.mapping.param;

import com.ezui.dao.param.ListQueryParameterObject;
import com.ezui.idm.entity.IEzPermission;

public class EzPermissionQueryImpl extends ListQueryParameterObject {

	private static final long serialVersionUID = -3987912541447358649L;
	private IEzPermission record;
	public IEzPermission getRecord() {
		return record;
	}
	public void setRecord(IEzPermission record) {
		this.record = record;
	}
	
}
