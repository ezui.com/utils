package com.ezui.idm.mapping.param;

public interface EzUserQuery {
	public String getId();

	public void setId(String id);

	public String getLoginId();

	public void setLoginId(String loginId);

	public String getUsername();

	public void setUsername(String username);

	public String getUsernameLike();

	public void setUsernameLike(String usernameLike);
	
	public String getEmail();

	public void setEmail(String email);

	public String getEmailLike();

	public void setEmailLike(String emailLike);
	
	public String getGroupId();

	public void setGroupId(String groupId);
	public String getOrderByColumns();
	public void setOrderByColumns(String orderByColumns);
}
