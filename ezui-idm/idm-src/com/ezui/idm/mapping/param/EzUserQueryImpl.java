package com.ezui.idm.mapping.param;

import java.util.Date;
import java.util.List;

import org.activiti.engine.impl.interceptor.CommandExecutor;

import com.ezui.idm.adapter.UserQueryImplAdapter;
import com.ezui.idm.entity.IEzUser;

public class EzUserQueryImpl extends UserQueryImplAdapter {

	protected static final long serialVersionUID = -3464972413598332962L;
    protected String id;

    protected String loginId;

    protected String username;
    protected String usernameLike;
    protected String defaultDeptName;
    protected String defaultDeptNameLike;
    protected String defaultDeptNo;
    protected String defaultDeptNoLike;
    protected String email;
    protected String emailLike;
    protected String groupId;
    protected List<String> groupIds;
    protected IEzUser record;
    protected int revision;
    protected boolean rowLockDateIsNull=false;
    protected Date rowLockDate;
    protected String defaultBranchNo;
    protected String userSource;
    protected String deptNo;
    protected String[] roles;

	protected boolean distinct=false;
    protected String orderByColumns;
	protected boolean readUncommited=false;
	protected int firstRow;
	protected int lastRow;

	public EzUserQueryImpl() {
		super();
	}
	public EzUserQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsernameLike() {
		return usernameLike;
	}

	public void setUsernameLike(String usernameLike) {
		this.usernameLike = usernameLike;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailLike() {
		return emailLike;
	}

	public void setEmailLike(String emailLike) {
		this.emailLike = emailLike;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public IEzUser getRecord() {
		return record;
	}
	public void setRecord(IEzUser record) {
		this.record = record;
	}
	public int getRevision() {
		return revision;
	}
	public void setRevision(int revision) {
		this.revision = revision;
	}
	public boolean isRowLockDateIsNull() {
		return rowLockDateIsNull;
	}
	public void setRowLockDateIsNull(boolean rowLockDateIsNull) {
		this.rowLockDateIsNull = rowLockDateIsNull;
	}
	public Date getRowLockDate() {
		return rowLockDate;
	}
	public void setRowLockDate(Date rowLockDate) {
		this.rowLockDate = rowLockDate;
	}
	public String getDefaultBranchNo() {
		return defaultBranchNo;
	}
	public void setDefaultBranchNo(String defaultBranchNo) {
		this.defaultBranchNo = defaultBranchNo;
	}
	public String getDefaultDeptName() {
		return defaultDeptName;
	}
	public void setDefaultDeptName(String defaultDeptName) {
		this.defaultDeptName = defaultDeptName;
	}
	public String getDefaultDeptNameLike() {
		return defaultDeptNameLike;
	}
	public void setDefaultDeptNameLike(String defaultDeptNameLike) {
		this.defaultDeptNameLike = defaultDeptNameLike;
	}
	public String getDefaultDeptNo() {
		return defaultDeptNo;
	}
	public void setDefaultDeptNo(String defaultDeptNo) {
		this.defaultDeptNo = defaultDeptNo;
	}
	public String getDefaultDeptNoLike() {
		return defaultDeptNoLike;
	}
	public void setDefaultDeptNoLike(String defaultDeptNoLike) {
		this.defaultDeptNoLike = defaultDeptNoLike;
	}
	public String getUserSource() {
		return userSource;
	}
	public void setUserSource(String userSource) {
		this.userSource = userSource;
	}
	public String getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	public String getOrderByColumns() {
		if (orderByColumns == null) {
			orderByColumns = "RES.id asc";
		}
		return orderByColumns;
	}
	public void setOrderByColumns(String orderByColumns) {
		this.orderByColumns = orderByColumns;
	}
	public List<String> getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(List<String> groupIds) {
		this.groupIds = groupIds;
	}
	public boolean isDistinct() {
		return distinct;
	}
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}
	public int getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}

	public int getLastRow() {
		return lastRow;
	}

	public void setLastRow(int lastRow) {
		this.lastRow = lastRow;
	}
	public boolean isReadUncommited() {
		return readUncommited;
	}

	public void setReadUncommited(boolean readUncommited) {
		this.readUncommited = readUncommited;
	}
	
}
