package com.ezui.idm.service;

import java.util.Date;
import java.util.List;

import com.ezui.idm.dao.IEzDepartmentDao;
import com.ezui.idm.entity.EzDepartment;
import com.ezui.idm.entity.IEzDepartment;
import com.ezui.idm.entity.EzDepartmentXUser;
import com.ezui.idm.mapping.param.EzDepartmentQueryImpl;

public class EzDepartmentService implements IEzDepartmentService {
	private IEzDepartmentDao departmentDao;

	public IEzDepartmentDao getDepartmentDao() {
		return departmentDao;
	}

	public void setDepartmentDao(IEzDepartmentDao departmentDao) {
		this.departmentDao = departmentDao;
	}

	public List<IEzDepartment> listTopBranches(EzDepartmentQueryImpl query, String orderBy) {
		if (query == null) {
			query = new EzDepartmentQueryImpl();
		}
		query.setParentIdIsNull("1");
		return departmentDao.selectDepartmentByQuery(query);
	}

	@Override
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query, int offset, int limit, String orderBy) {
		return departmentDao.selectDepartmentByQuery(query, offset, limit, orderBy);
	}

	@Override
	public long countDepartmentByQuery(EzDepartmentQueryImpl query) {
		return departmentDao.countDepartmentByQuery(query);
	}

	@Override
	public IEzDepartment findDepartmentByDeptNo(String deptNo) {
		return departmentDao.findDepartmentByDeptNo(deptNo);
	}
	@Override
	public int updateByPrimaryKey(IEzDepartment entity) {
		return this.departmentDao.updateByPrimaryKey(entity);
	}
	@Override
	public int insert(IEzDepartment entity) {
		return this.departmentDao.insert(entity);
	}
	@Override
	public List<IEzDepartment> selectDepartmentsByUserId(String userId) {
		EzDepartmentQueryImpl query = new EzDepartmentQueryImpl();
		query.setUserId(userId);
		return departmentDao.selectDepartmentByQuery(query);
	}
	@Override
	public int addRelationshipUserXDeparment(EzDepartmentXUser entity) {
		return this.departmentDao.addRelationshipUserXDeparment(entity);
	}
	@Override
	public int deleteRelationshipUserXDeparment(EzDepartmentXUser entity) {
		return this.departmentDao.deleteRelationshipUserXDeparment(entity);
	}
	@Override
	public int bulkLockAllDept(Date rowLockDate, String deptSource) {
		EzDepartmentQueryImpl query = new EzDepartmentQueryImpl();
		IEzDepartment dept = new EzDepartment();
		dept.setRowLockDate(rowLockDate);
		query.setRecord(dept);
		query.setRowLockDateIsNull(true);
		query.setDeptSource(deptSource);
		return this.departmentDao.updateByQueryCriteriaSelective(query);
	}
	@Override
	public int setRowLockedDisableFlag(Date rowLockDate, String deptSource) {
		return this.departmentDao.setRowLockedDisableFlag(rowLockDate, deptSource);
	}

}
