package com.ezui.idm.service;

import java.util.Date;
import java.util.List;

import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.ezui.idm.adapter.GroupEntityManagerAdapterImpl;
import com.ezui.idm.dao.IEzGroupDao;
import com.ezui.idm.entity.EzGroup;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.EzUserXGroup;
import com.ezui.idm.mapping.param.EzGroupQueryImpl;

public class EzGroupService extends GroupEntityManagerAdapterImpl implements IEzGroupService {

	private IEzGroupDao groupDao;

	public EzGroupService() {
	}

	public EzGroupService(ProcessEngineConfigurationImpl config, IEzGroupDao ezGroupDao) {
		super(config, ezGroupDao);
		this.setGroupDao(ezGroupDao);
	}

	public IEzGroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(IEzGroupDao groupDao) {
		this.groupDao = groupDao;
	}

	@Override
	public IEzGroup findGroupByGroupName(String groupName) {
		return this.groupDao.findGroupByGroupName(groupName);
	}
	@Override
	public int updateByPrimaryKey(IEzGroup group) {
		return this.groupDao.updateByPrimaryKey(group);
	}
	@Override
	public int insert(IEzGroup group) {
		return this.groupDao.insert(group);
	}
	@Override
	public List<IEzGroup> selectGroupsByUserId(String userId) {
		return this.groupDao.selectGroupsByUserId(userId);
	}
	@Override
	public List<IEzGroup> selectAllGroupsByUserId(String userId) {
		return this.groupDao.selectAllGroupsByUserId(userId);
	}
	@Override
	public int addRelationshipUserXGroup(EzUserXGroup entity) {
		return this.groupDao.addRelationshipUserXGroup(entity);
	}
	@Override
	public int deleteRelationshipUserXGroup(EzUserXGroup entity) {
		return this.groupDao.deleteRelationshipUserXGroup(entity);
	}
	@Override
	public int bulkLockAllGroup(Date rowLockDate, String groupSource) {
		EzGroupQueryImpl query = new EzGroupQueryImpl();
		IEzGroup group = new EzGroup();
		group.setRowLockDate(rowLockDate);
		query.setRecord(group);
		query.setRowLockDateIsNull(true);
		query.setGroupSource(groupSource);
		return this.groupDao.updateByQueryCriteriaSelective(query);
	}
	@Override
	public int setRowLockedDisableFlag(Date rowLockDate, String groupSource) {
		return this.groupDao.setRowLockedDisableFlag(rowLockDate, groupSource);
	}
	@Override
	public List<IEzGroup> selectByPagingQueryCriteria(EzGroupQueryImpl query){
		return this.groupDao.selectByPagingQueryCriteria(query);
	}
	@Override
	public Long countByQueryCriteria(EzGroupQueryImpl query) {
		return this.groupDao.countByQueryCriteria(query);
	}

}
