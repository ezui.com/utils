package com.ezui.idm.service;

import java.util.List;

import com.ezui.idm.dao.IEzPermissionDao;
import com.ezui.idm.entity.IEzPermission;

public class EzPermissionService implements IEzPermissionService{
	private IEzPermissionDao permissionDao;
	public IEzPermissionDao getPermissionDao() {
		return permissionDao;
	}
	public void setPermissionDao(IEzPermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}
	@Override
	public int insert(IEzPermission entity){
		return this.permissionDao.insert(entity);
	}
	@Override
	public List<IEzPermission> selectPermissionsByUserId(String userId){
		return this.permissionDao.selectPermissionsByUserId(userId);
	}
}
