package com.ezui.idm.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ezui.idm.dao.IEzPersistentTokenRepository;

@Service
public class EzPersistentTokenCleanupService {
	private static final Logger logger = LoggerFactory.getLogger(EzPersistentTokenCleanupService.class);
	
	@Autowired
	protected Environment environment;
	
	@Autowired
	private IEzPersistentTokenRepository persistentTokenRepository;
	
	@Transactional
	//@Scheduled(cron="${ez.security.cookie.database-removal.cronExpression:0 0 1 * * ?}") // Default 01:00
	public void deleteObsoletePersistentTokens() {
		long maxAge = getTokenMaxAge();
		long now = new Date().getTime();
		Date maxDate = new Date(now - maxAge);
		Long deletedTokens = persistentTokenRepository.deleteByTokenDateBefore(maxDate);
		if (deletedTokens != null) {
			logger.info("Removed " + deletedTokens + " obsolete persisted tokens");
		}
	}
	
	protected long getTokenMaxAge() {
		Integer tokenMaxAgeSeconds = environment.getProperty("ez.security.cookie.database-removal.max-age", Integer.class);
		if (tokenMaxAgeSeconds == null) {
			tokenMaxAgeSeconds = environment.getProperty("security.cookie.max-age", Integer.class, 2678400); // Default 31 days
		}
		return (tokenMaxAgeSeconds.longValue() * 1000L) + 1;
	}

}
