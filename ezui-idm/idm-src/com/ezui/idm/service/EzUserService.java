package com.ezui.idm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.ezui.idm.adapter.UserEntityManagerAdapterImpl;
import com.ezui.idm.dao.IEzUserDao;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.entity.EzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;

public class EzUserService extends UserEntityManagerAdapterImpl implements IEzUserService {

	protected IEzUserDao userDao;

	public EzUserService() {

	}

	public EzUserService(ProcessEngineConfigurationImpl config, IEzUserDao userDao) {
		super(config, userDao);
		this.setUserDao(userDao);
	}

	public List<String> getAMLGroupNamesByLoginId(String loginId) {
		List<String> groupNames = new ArrayList<String>();
		List<Group> groups = this.findGroupsByUser(loginId);
		for (Group g : groups) {
			IEzGroup gp = (IEzGroup) g;
			String gn = gp.getGroupName().toLowerCase();
			if (gn.startsWith("aml_")) {
				groupNames.add(gp.getGroupName());
			}
		}
		return groupNames;
	}

	public IEzUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IEzUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public IEzUser findByLoginId(String loginId) {
		return this.userDao.findByLoginId(loginId);
	}

	@Override
	public int bulkLockAllUser(Date rowLockDate, String userSource) {
		EzUserQueryImpl query = new EzUserQueryImpl();
		IEzUser user = new EzUser();
		user.setRowLockDate(rowLockDate);
		query.setRecord(user);
		query.setRowLockDateIsNull(true);
		query.setUserSource(userSource);
		return this.userDao.updateByQueryCriteriaSelective(query);
	}
	@Override
	public int updateByQueryCriteriaSelective(EzUserQueryImpl query) {
		return this.userDao.updateByQueryCriteriaSelective(query);
	}
	@Override
	public int updateByIdSelective(IEzUser user) {
		EzUserQueryImpl query = new EzUserQueryImpl();
		query.setRecord(user);
		query.setId(user.getId());
		return this.userDao.updateByQueryCriteriaSelective(query);
	}
	@Override
	public int insert(IEzUser user) {
		return this.userDao.insert(user);
	}
	@Override
	public int bulkInsert(List<IEzUser> users) {
		return this.userDao.bulkInsert(users);
	}
	@Override
	public int setRowLockedDisableFlag(Date rowLockDate, String userSource) {
		return this.userDao.setRowLockedDisableFlag(rowLockDate, userSource);
	}
	@Override
	public int updateByPrimaryKey(IEzUser user) {
		return this.userDao.updateByPrimaryKey(user);
	}
	@Override
	public List<IEzUser> findByGroupNames(List<String> groupNames){
		EzUserQueryImpl query=new EzUserQueryImpl();
		query.setGroupIds(groupNames);
		return this.userDao.selectByQueryCriteria(query);
	}
	@Override
	public List<IEzUser> selectByPagingQueryCriteria(EzUserQueryImpl query){
		return this.userDao.selectByPagingQueryCriteria(query);
	}
	@Override
	public Long countByQueryCriteria(EzUserQueryImpl query) {
		return this.userDao.countByQueryCriteria(query);
	}

}
