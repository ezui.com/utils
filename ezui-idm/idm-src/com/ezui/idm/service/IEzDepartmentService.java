package com.ezui.idm.service;

import java.util.Date;
import java.util.List;

import com.ezui.idm.entity.IEzDepartment;
import com.ezui.idm.entity.EzDepartmentXUser;
import com.ezui.idm.mapping.param.EzDepartmentQueryImpl;

public interface IEzDepartmentService {
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query, int offset, int limit, String orderBy);
	public IEzDepartment findDepartmentByDeptNo(String deptNo);
	public long countDepartmentByQuery(EzDepartmentQueryImpl query);
	public List<IEzDepartment> listTopBranches(EzDepartmentQueryImpl query, String orderBy);
	public int updateByPrimaryKey(IEzDepartment entity);
	public int insert(IEzDepartment entity);
	public List<IEzDepartment> selectDepartmentsByUserId(String userId);
	public int addRelationshipUserXDeparment(EzDepartmentXUser entity);
	public int deleteRelationshipUserXDeparment(EzDepartmentXUser entity);
	public int bulkLockAllDept(Date rowLockDate, String deptSource);
	public int setRowLockedDisableFlag(Date rowLockDate, String deptSource);
}
