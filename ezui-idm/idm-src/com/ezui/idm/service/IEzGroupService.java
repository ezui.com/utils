package com.ezui.idm.service;

import java.util.Date;
import java.util.List;

import com.ezui.idm.adapter.GroupEntityManagerAdapter;
import com.ezui.idm.dao.IEzGroupDao;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.mapping.param.EzGroupQueryImpl;
import com.ezui.idm.entity.EzUserXGroup;

public interface IEzGroupService extends GroupEntityManagerAdapter {
	public IEzGroupDao getGroupDao();
	public void setGroupDao(IEzGroupDao ezGroupDao);
	public IEzGroup findGroupByGroupName(String groupName);
	public int updateByPrimaryKey(IEzGroup group);
	public int insert(IEzGroup group);
	public List<IEzGroup> selectGroupsByUserId(String userId);
	public List<IEzGroup> selectAllGroupsByUserId(String userId);
	public int addRelationshipUserXGroup(EzUserXGroup entity);
	public int deleteRelationshipUserXGroup(EzUserXGroup entity);
	public int bulkLockAllGroup(Date rowLockDate, String groupSource);
	public int setRowLockedDisableFlag(Date rowLockDate, String groupSource);
	public List<IEzGroup> selectByPagingQueryCriteria(EzGroupQueryImpl query);
	public Long countByQueryCriteria(EzGroupQueryImpl query);
}
