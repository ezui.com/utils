package com.ezui.idm.service;

import java.util.List;

import com.ezui.idm.entity.IEzPermission;

public interface IEzPermissionService {
	public int insert(IEzPermission entity);
	public List<IEzPermission> selectPermissionsByUserId(String userId);
}
