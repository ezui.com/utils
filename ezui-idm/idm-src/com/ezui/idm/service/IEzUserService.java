package com.ezui.idm.service;

import java.util.Date;
import java.util.List;

import com.ezui.idm.adapter.UserEntityManagerAdapter;
import com.ezui.idm.dao.IEzUserDao;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;

public abstract interface IEzUserService extends UserEntityManagerAdapter {
	public List<String> getAMLGroupNamesByLoginId(String loginId);
	public IEzUser findByLoginId(String loginId);
	public IEzUserDao getUserDao();
	public void setUserDao(IEzUserDao userDao);
	public int bulkLockAllUser(Date rowLockDate, String userSource);
	public int updateByQueryCriteriaSelective(EzUserQueryImpl query);
	public int updateByIdSelective(IEzUser user);
	public int insert(IEzUser user);
	public int bulkInsert(List<IEzUser> users);
	public int setRowLockedDisableFlag(Date rowLockDate, String userSource);
	public int updateByPrimaryKey(IEzUser user);
	public List<IEzUser> findByGroupNames(List<String> groupNames);
	public List<IEzUser> selectByPagingQueryCriteria(EzUserQueryImpl query);
	public Long countByQueryCriteria(EzUserQueryImpl query);
}
