package com.ezui.idm.service;

import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.entity.EzPersistentToken;

public interface IPersistentTokenService {
	EzPersistentToken getPersistentToken(String tokenId);

	EzPersistentToken getPersistentToken(String tokenId, boolean invalidateCacheEntry);

	EzPersistentToken saveAndFlush(EzPersistentToken persistentToken);

	void delete(EzPersistentToken persistentToken);

	public EzPersistentToken createToken(IEzUser user, String remoteAddress, String userAgent);
}
