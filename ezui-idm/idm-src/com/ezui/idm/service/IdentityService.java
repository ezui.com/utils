package com.ezui.idm.service;

import java.util.List;

import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.IEzPermission;
import com.ezui.idm.entity.IEzUser;

public interface IdentityService {
	public List<IEzGroup> selectGroupsByUserId(String userId);
	public List<IEzGroup> selectAllGroupsByUserId(String userId);
	public int addPermission(IEzPermission entity);
	public List<IEzPermission> selectPermissionsByUserId(String userId);
	public IEzUser findByLoginId(String loginId);
	public IEzUserService getUserService();
	public IEzGroupService getGroupService();
	public IEzPermissionService getPermissionService();
}
