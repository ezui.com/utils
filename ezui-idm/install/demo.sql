insert  into 'sys_permission'('id','name','type','url','percode','parentid','parentids','sortstring','available') values 
(1,'权限','','',NULL,0,'0/','0','1'),(11,'商品管理','menu','/item/queryItem.action',NULL,1,'0/1/','1.','1'),
(12,'商品新增','permission','/item/add.action','item:create',11,'0/1/11/','','1'),
(13,'商品修改','permission','/item/editItem.action','item:update',11,'0/1/11/','','1'),
(14,'商品删除','permission','','item:delete',11,'0/1/11/','','1'),
(15,'商品查询','permission','/item/queryItem.action','item:query',11,'0/1/15/',NULL,'1'),
(21,'用户管理','menu','/user/query.action','user:query',1,'0/1/','2.','1'),
(22,'用户新增','permission','','user:create',21,'0/1/21/','','1'),
(23,'用户修改','permission','','user:update',21,'0/1/21/','','1'),
(24,'用户删除','permission','','user:delete',21,'0/1/21/','','1');

/*Data for the table 'sys_role' */

insert  into 'sys_role'('id','name','available') values 
	('ebc8a441-c6f9-11e4-b137-0adc305c3f28','商品管理员','1'),
	('ebc9d647-c6f9-11e4-b137-0adc305c3f28','用户管理员','1');

/*Data for the table 'sys_role_permission' */

insert  into 'sys_role_permission'('id','sys_role_id','sys_permission_id') values 
	('ebc8a441-c6f9-11e4-b137-0adc305c3f21','ebc8a441-c6f9-11e4-b137-0adc305c','12'),
	('ebc8a441-c6f9-11e4-b137-0adc305c3f22','ebc8a441-c6f9-11e4-b137-0adc305c','11'),
	('ebc8a441-c6f9-11e4-b137-0adc305c3f24','ebc9d647-c6f9-11e4-b137-0adc305c','21'),
	('ebc8a441-c6f9-11e4-b137-0adc305c3f25','ebc8a441-c6f9-11e4-b137-0adc305c','15'),
	('ebc9d647-c6f9-11e4-b137-0adc305c3f23','ebc9d647-c6f9-11e4-b137-0adc305c','22'),
	('ebc9d647-c6f9-11e4-b137-0adc305c3f26','ebc8a441-c6f9-11e4-b137-0adc305c','13');

/*Data for the table 'EZ_ID_GROUP' */

insert  into [ECM].[EZ_ID_GROUP]([id]
      ,[group_name]
      ,[group_type]
      ,[group_description]
      ,[available]
      ,[created_date]
      ,[modified_date]
      ,[created_by]
      ,[modified_by]
      ,[is_private]) values 
	('administrator','administrator','role','Superusers','1','2018-11-01 00:00:00',null,null,null,1),
	('aml_role_1_1','aml_role_1_1','role','AML系統_分行專職人員','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_1','aml_role_1','role','AML系統_分行經辦','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_2','aml_role_2','role','AML系統_分行業務主管','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_3','aml_role_3','role','AML系統_分行洗錢督導主管','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_4','aml_role_4','role','AML系統_分行單位主管','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_5_1','aml_role_5_1','role','AML系統_總行專責單位','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_5_2','aml_role_5_2','role','AML系統_總行專責名單建置單位','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_5','aml_role_5','role','AML系統_洗防經辦','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_6','aml_role_6','role','AML系統_洗防副科長','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_7','aml_role_7','role','AML系統_洗防科長','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_8','aml_role_8','role','AML系統_洗防副理','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_9','aml_role_9','role','AML系統_洗防協理','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_10','aml_role_10','role','AML系統_法遵長','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_11','aml_role_11','role','AML系統_總行使用者','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_a','aml_role_a','role','AML系統_法遵部管理人員','1','2018-11-01 00:00:00',null,null,null),
	('aml_role_b','aml_role_b','role','AML系統_資訊部管理人員','1','2018-11-01 00:00:00',null,null,null)
	;

/*Data for the table 'EZ_ID_USER' */

  insert into ecm.EZ_ID_USER ("user_source","id"
      ,"login_id"
      ,"username"
      ,"password"
      ,"salt"
      ,"email"
      ,"default_title_code"
      ,"default_job_title"
      ,"default_dept_no"
      ,"default_dept_name"
      ,"picture_id"
      ,"locked"
      ,"available"
      ,"created_date"
      ,"modified_date"
      ,"created_by"
      ,"modified_by") values 
	('sasdemo','sasdemo','sasdemo','test','','sasdemo@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('admin','admin','admin','test','','admin@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0101@0030','lv0101@0030','lv0101@0030','test','','lv0101@ecm','','','0030','Demo分行',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv01@0030','lv01@0030','lv01@0030','test','','lv01@ecm','','','0030','Demo分行',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0201@0030','lv0201@0030','lv0201@0030','test','','lv0201@ecm','','','0030','Demo分行',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0301@0030','lv0301@0030','lv0301@0030','test','','lv0301@ecm','','','0030','Demo分行',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0401@0030','lv0401@0030','lv0401@0030','test','','lv0401@ecm','','','0030','Demo分行',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0501','lv0501','lv0501','test','','lv0501@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0502','lv0502','lv0502','test','','lv0502@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv05','lv05','lv05','test','','lv05@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0601','lv0601','lv0601','test','','lv0601@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0701','lv0701','lv0701','test','','lv0701@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0801','lv0801','lv0801','test','','lv0801@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv0901','lv0901','lv0901','test','','lv0901@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv1001','lv1001','lv1001','test','','lv1001@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lv1101','lv1101','lv1101','test','','lv1101@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lva01','lva01','lva01','test','','lva01@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null),
	('lvb01','lvb01','lvb01','test','','lvb01@ecm','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
	;
	
  insert into ecm.EZ_ID_USER ("id"
      ,"login_id"
      ,"username"
      ,"password"
      ,"salt"
      ,"email"
      ,"default_title_code"
      ,"default_job_title"
      ,"default_dept_no"
      ,"default_dept_name"
      ,"picture_id"
      ,"locked"
      ,"available"
      ,"created_date"
      ,"modified_date"
      ,"created_by"
      ,"modified_by") values 
 ('0560lv0101','0560lv0101','我是dept 0560 lv01 01','test','', '0560lv0101@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0102','0560lv0102','我是dept 0560 lv01 02','test','', '0560lv0102@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0201','0560lv0201','我是dept 0560 lv02 01','test','', '0560lv0201@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0202','0560lv0202','我是dept 0560 lv02 02','test','', '0560lv0202@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0301','0560lv0301','我是dept 0560 lv03 01','test','', '0560lv0301@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0302','0560lv0302','我是dept 0560 lv03 02','test','', '0560lv0302@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0401','0560lv0401','我是dept 0560 lv04 01','test','', '0560lv0401@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0560lv0402','0560lv0402','我是dept 0560 lv04 02','test','', '0560lv0402@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
,('0000lv0501','0000lv0501','我是dept 0000 lv05 01','test','', '0000lv0501@aml','','','','',null,'0','1','2018-11-01 00:00:00',null,null,null)
;
	
/*Data for the table 'EZ_ID_USER_X_GROUP' */

insert  into [ECM].[EZ_ID_USER_X_GROUP]([user_id]
      ,[group_id]) values 
	('admin','administrator'),
	('lv0101@0030','aml_role_1_1'),
	('lv01@0030','aml_role_1'),
	('lv0201@0030','aml_role_2'),
	('lv0301@0030','aml_role_3'),
	('lv0401@0030','aml_role_4'),
	('lv0501','aml_role_5_1'),
	('lv0502','aml_role_5_2'),
	('lv05','aml_role_5'),
	('lv0601','aml_role_6'),
	('lv0701','aml_role_7'),
	('lv0801','aml_role_8'),
	('lv0901','aml_role_9'),
	('lv1001','aml_role_10'),
	('lv1101','aml_role_11'),
	('lva01','aml_role_a'),
	('lvb01','aml_role_b');

insert  into [ECM].[EZ_ID_USER_X_GROUP]([user_id]
      ,[group_id]) values 
 ('0560lv0101','aml_role_1_1')
,('0560lv0102','aml_role_1')
,('0560lv0201','aml_role_2')
,('0560lv0202','aml_role_2')
,('0560lv0301','aml_role_3')
,('0560lv0302','aml_role_3')
,('0560lv0401','aml_role_4')
,('0560lv0402','aml_role_4')
,('0000lv0501','aml_role_5')
;

insert into [ECM].[EZ_ID_DEPARTMENT] ([id]
      ,[dept_no]
      ,[dept_name]
      ,[revision]
      ,[dept_type]
      ,[dept_description]
      ,[parent_id]
      ,[parent_ids]
      ,[default_roles]
      ,[available]
      ,[created_date]
      ,[modified_date]
      ,[created_by]
      ,[modified_by]
)
values
 ('0560','0560','營業部',1,'headquarter','總公司營業部',null,null,null,'1','2018-11-10',null,null,null);
 
 insert into [ECM].[EZ_ID_DEPARTMENT_X_USER] ( [user_id]
      ,[dept_id]
      ,[title_code]
      ,[job_title]
)
values
 ('lv0101@0030','0560',null,null)
,('lv01@0030','0560',null,null)
,('lv0201@0030','0560',null,null)
,('lv0301@0030','0560',null,null)
,('lv0401@0030','0560',null,null)
,('lv0501','0560',null,null)
,('lv0502','0560',null,null)
,('lv05','0560',null,null)
,('lv0601','0560',null,null)
,('lv0701','0560',null,null)
,('lv0801','0560',null,null)
,('lv0901','0560',null,null)
,('lv1001','0560',null,null)
,('lv1101','0560',null,null)
,('lva01','0560',null,null)
,('lvb01','0560',null,null)
,('0560lv0101','0560',null,null)
,('0560lv0102','0560',null,null)
,('0560lv0201','0560',null,null)
,('0560lv0202','0560',null,null)
,('0560lv0301','0560',null,null)
,('0560lv0302','0560',null,null)
,('0560lv0401','0560',null,null)
,('0560lv0402','0560',null,null)
;