/*Table structure for table "EZ_ID_PERMISSION" */
CREATE TABLE "ecm"."EZ_ID_PERMISSION" (
  "id" numeric(19, 0) IDENTITY(1,1) PRIMARY KEY,
  "name" varchar(128) NOT NULL,
  "revision" int NOT NULL DEFAULT 1,
  "acl_type" varchar(32) NOT NULL check (acl_type in ('application','menu','button','permission')),
  "url" varchar(128) DEFAULT NULL,
  "percode" varchar(128) DEFAULT NULL,
  "parent_id" numeric(19, 0) NOT NULL DEFAULT 0,
  "parent_ids" varchar(128) DEFAULT NULL,
  "sort_no" varchar(128) DEFAULT NULL,
  "available" char(1) NOT NULL DEFAULT '1' check (available in ('0','1')),
  "created_date" datetime NOT NULL,
  "modified_date" datetime DEFAULT NULL,
  "created_by" varchar(32) DEFAULT NULL,
  "modified_by" varchar(32) DEFAULT NULL
);
/*CREATE TABLE "ecm"."ez_id_permission" (
  "id" numeric(19, 0) IDENTITY(1,1) PRIMARY KEY COMMENT "主鍵",
  "name" varchar(128) NOT NULL COMMENT "資源名稱",
  "type" varchar(32) NOT NULL COMMENT "資源類型：application,menu,button,permission",
  "url" varchar(128) DEFAULT NULL COMMENT "訪問url地址",
  "percode" varchar(128) DEFAULT NULL COMMENT "權限代碼字串",
  "parent_id" numeric(19, 0) DEFAULT NULL COMMENT "父節點id",
  "parent_ids" varchar(128) DEFAULT NULL COMMENT "父節點id列,/串接",
  "sort_no" varchar(128) DEFAULT NULL COMMENT "排序號",
  "available" char(1) DEFAULT "1" COMMENT "是否可用,1：可用，0不可用",
  "created_date" datetime NOT NULL COMMENT "創建時間",
  "modified_date" datetime DEFAULT NULL COMMENT "修改時間",
  "created_by" varchar(32) DEFAULT NULL COMMENT "創建人員id",
  "modified_by" varchar(32) DEFAULT NULL COMMENT "修改人員id"
);*/

/*EXEC sp_updateextendedproperty 
@name = N"MS_Description", @value = "主鍵",
@level0type = N"Schema", @level0name = "ecm", 
@level1type = N"Table",  @level1name = "ez_id_permission", 
@level2type = N"Column", @level2name = "id";*/
 
/*Table structure for table "EZ_ID_GROUP" */

CREATE TABLE "ecm"."EZ_ID_GROUP" (
  "id" varchar(32) PRIMARY KEY,
  "group_name" varchar(128) NOT NULL,
  "revision" int NOT NULL DEFAULT 1,
  "group_type" varchar(20) NOT NULL check (group_type in ('group','role')),
  "group_description" varchar(255) DEFAULT NULL,
  "available" char(1) NOT NULL DEFAULT '1' check (available in ('0','1')),
  "created_date" datetime NOT NULL,
  "modified_date" datetime DEFAULT NULL,
  "created_by" varchar(32) DEFAULT NULL,
  "modified_by" varchar(32) DEFAULT NULL
);

/*CREATE TABLE "ecm"."ez_id_group" (
  "id" varchar(32) PRIMARY KEY COMMENT "主鍵",
  "group_name" varchar(128) NOT NULL comment "群組名稱",
  "revision" int comment "版本",
  "group_type" varchar(20) NOT NULL comment "群組類型:group,role",
  "group_description" varchar(255) NOT NULL,
  "available" char(1) DEFAULT "1" COMMENT "是否可用,1：可用，0不可用",
  "created_date" datetime NOT NULL COMMENT "創建時間",
  "modified_date" datetime DEFAULT NULL COMMENT "修改時間",
  "created_by" varchar(32) DEFAULT NULL COMMENT "創建人員id",
  "modified_by" varchar(32) DEFAULT NULL COMMENT "修改人員id"
);*/
 
/*Table structure for table "EZ_ID_GROUP_X_PERMISSION" */

CREATE TABLE "ecm"."EZ_ID_GROUP_X_PERMISSION" (
  "group_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_GROUP"(id),
  "permission_id" numeric(19, 0) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_PERMISSION"(id),
  CONSTRAINT PK_ROLE_X_PERMISSION PRIMARY KEY (group_id,permission_id)
);
/*CREATE TABLE "ecm"."ez_id_role_x_permission" (
  "role_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES ez_id_group(id) COMMENT "角色id",
  "permission_id" numeric(19, 0) NOT NULL FOREIGN KEY REFERENCES ez_id_permission(id) COMMENT "權限id",
  CONSTRAINT PK_ROLE_X_PERMISSION PRIMARY KEY (role_id,permission_id)
);*/
 
/*Table structure for table "ez_id_group_x_group" */

CREATE TABLE "ecm"."EZ_ID_GROUP_X_GROUP" (
  "parent_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_GROUP"(id),
  "group_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_GROUP"(id),
  CONSTRAINT PK_GROUP_X_GROUP PRIMARY KEY (group_id,parent_id)
);
 
/*CREATE TABLE "ecm"."ez_id_group_x_group" (
  "parent_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES ez_id_group(id) COMMENT "角色id",
  "group_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES ez_id_group(id) COMMENT "權限id",
  CONSTRAINT PK_GROUP_X_GROUP PRIMARY KEY (group_id,parent_id)
);*/
 
/*Table structure for table "EZ_ID_USER" */

CREATE TABLE "ecm"."EZ_ID_USER" (
  "id" varchar(32) PRIMARY KEY,
  "login_id" varchar(32) NOT NULL,
  "username" varchar(64) NOT NULL,
  "revision" int NOT NULL DEFAULT 1,
  "password" varchar(32) DEFAULT NULL,
  "salt" varchar(64) DEFAULT NULL,
  "email" varchar(100) DEFAULT NULL,
  "user_source" varchar(20) NOT NULL DEFAULT 'inline password' check (user_source in ('inline password','LDAP','ADWS')),
  "default_title_code" varchar(32) DEFAULT NULL,
  "default_job_title" varchar(60) DEFAULT NULL,
  "default_dept_no" varchar(100) DEFAULT NULL,
  "default_dept_name" varchar(240) DEFAULT NULL,
  "picture_id" varchar(64) DEFAULT NULL,
  "locked" char(1) NOT NULL DEFAULT '0' check (locked in ('0','1')),
  "available" char(1) NOT NULL DEFAULT '1' check (available in ('0','1')),
  "created_date" datetime NOT NULL,
  "modified_date" datetime DEFAULT NULL,
  "created_by" varchar(32) DEFAULT NULL,
  "modified_by" varchar(32) DEFAULT NULL,
  "workflow_disabled" char(1) NOT NULL DEFAULT '1' check (workflow_disabled in ('0','1')),
  "user_ldap_dn" varchar(255) DEFAULT NULL,
  "user_delegation" varchar(255) DEFAULT NULL,
  "failed_auth_attempt" int NOT NULL DEFAULT 0
);

ALTER TABLE "ecm"."EZ_ID_USER"
        ADD "user_source" varchar(20) NOT NULL
  check (user_source in ('inline password','LDAP','ADWS'))
    DEFAULT ('inline password')

ALTER TABLE "ecm"."EZ_ID_USER"
        ADD "workflow_disabled" char(1) NOT NULL
  check (workflow_disabled in ('0','1'))
    DEFAULT ('1')

ALTER TABLE "ecm"."EZ_ID_USER"
        ADD "user_ldap_dn" varchar(255) DEFAULT NULL

ALTER TABLE "ecm"."EZ_ID_USER"
        ADD "user_delegation" varchar(255) DEFAULT NULL

ALTER TABLE "ecm"."EZ_ID_USER"
        ADD "failed_auth_attempt" int NOT NULL DEFAULT 0

/*CREATE TABLE "ecm"."EZ_ID_USER" (
  "id" varchar(32) PRIMARY KEY COMMENT "主鍵",
  "login_id" varchar(32) NOT NULL COMMENT "登錄名稱",
  "username" varchar(64) NOT NULL COMMENT "顯示名稱",
  "password" varchar(32) DEFAULT NULL COMMENT "密碼",
  "salt" varchar(64) DEFAULT NULL COMMENT "鹽",
  "email" varchar(100) DEFAULT NULL COMMENT "email",
  "default_title_code" varchar(32) DEFAULT NULL COMMENT "職稱代碼",
  "default_job_title" varchar(60) DEFAULT NULL COMMENT "職稱",
  "default_dept_no" varchar(100) DEFAULT NULL COMMENT "所在部門代碼",
  "default_dept_name" varchar(240) DEFAULT NULL COMMENT "所在部門名稱",
  "picture_id" varchar(64) DEFAULT NULL COMMENT "圖片id",
  "locked" char(1) DEFAULT "0" COMMENT "是否鎖定，1：鎖定，0未鎖定",
  "available" char(1) DEFAULT "1" COMMENT "是否可用,1：可用，0不可用",
  "created_date" datetime NOT NULL COMMENT "創建時間",
  "modified_date" datetime DEFAULT NULL COMMENT "修改時間",
  "created_by" varchar(32) DEFAULT NULL COMMENT "創建人員id",
  "modified_by" varchar(32) DEFAULT NULL COMMENT "修改人員id"
);*/
 
/*Table structure for table "EZ_ID_USER_X_GROUP" */
CREATE TABLE "ecm"."EZ_ID_USER_X_GROUP" (
  "user_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_USER"(id),
  "group_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_GROUP"(id),
  CONSTRAINT PK_USER_X_GROUP PRIMARY KEY (user_id,group_id)
);
 
/*CREATE TABLE "ecm"."EZ_ID_USER_x_role" (
  "user_id" numeric(19, 0) NOT NULL FOREIGN KEY REFERENCES ez_id_user(id) COMMENT "Member id",
  "role_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES ez_id_group(id) COMMENT "角色id",
  CONSTRAINT PK_USER_X_GROUP PRIMARY KEY (user_id,role_id)
);*/

/*Table structure for table "EZ_ID_DEPARTMENT" */
CREATE TABLE "ecm"."EZ_ID_DEPARTMENT" (
  "id" varchar(32) PRIMARY KEY,
  "dept_no" varchar(32) NOT NULL,
  "dept_name" varchar(240) NOT NULL,
  "revision" int NOT NULL DEFAULT 1,
  "dept_type" varchar(20) NOT NULL  check (dept_type in ('headquarter','division','branch','department','unit')),
  "dept_description" varchar(600) NOT NULL,
  "parent_id" varchar(32) DEFAULT NULL,
  "parent_ids" varchar(128) DEFAULT NULL,
  "default_roles" varchar(225) DEFAULT NULL,
  "available" char(1) NOT NULL DEFAULT '1' check (available in ('0','1')),
  "created_date" datetime NOT NULL,
  "modified_date" datetime DEFAULT NULL,
  "created_by" varchar(32) DEFAULT NULL,
  "modified_by" varchar(32) DEFAULT NULL
);

/*CREATE TABLE "ecm"."ez_id_department" (
  "id" varchar(32) PRIMARY KEY COMMENT "主鍵",
  "dept_no" varchar(32) NOT NULL COMMENT "部門編碼,ldap=ou",
  "dept_name" varchar(240) NOT NULL COMMENT "部門名稱",
  "dept_type" varchar(20) NOT NULL COMMENT "組織類型:總公司,區域,分公司,部門",
  "dept_description" varchar(600) NOT NULL COMMENT "部門說明,可以是json格式",
  "parent_id" varchar(32) DEFAULT NULL FOREIGN KEY REFERENCES ez_id_department(id) COMMENT "主鍵",
  "default_roles" varchar(225) DEFAULT NULL COMMENT "默認分配的角色",
  "available" char(1) DEFAULT "1" COMMENT "是否可用,1：可用，0不可用"
);*/

/*Table structure for table "EZ_ID_DEPARTMENT_X_USER" */
CREATE TABLE "ecm"."EZ_ID_DEPARTMENT_X_USER" (
  "user_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_USER"(id),
  "dept_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES "ecm"."EZ_ID_DEPARTMENT"(id),
  "title_code" varchar(32) DEFAULT NULL,
  "job_title" varchar(60) DEFAULT NULL,
  CONSTRAINT PK_DEPARTMENT_X_USER PRIMARY KEY (user_id,dept_id,job_title)
);

/*CREATE TABLE "ecm"."ez_id_department_x_user" (
  "user_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES ez_id_user(id) COMMENT "人員 id",
  "dept_id" varchar(32) NOT NULL FOREIGN KEY REFERENCES ez_id_department(id) COMMENT "部門id",
  "title_code" varchar(32) DEFAULT NULL COMMENT "職稱代碼",
  "job_title" varchar(60) DEFAULT NULL COMMENT "職稱",
  CONSTRAINT PK_DEPARTMENT_X_USER PRIMARY KEY (user_id,dept_id,job_title)
);*/



