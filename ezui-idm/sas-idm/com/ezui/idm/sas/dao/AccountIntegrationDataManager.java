package com.ezui.idm.sas.dao;

import java.util.List;

import com.ezui.idm.dao.IEzUserDao;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.sas.mapping.param.AccountIntegrationQueryImpl;

public interface AccountIntegrationDataManager extends IEzUserDao {
	public Long countByQueryCriteria(AccountIntegrationQueryImpl query);

	public List<IEzUser> selectByQueryCriteria(AccountIntegrationQueryImpl query);
}
