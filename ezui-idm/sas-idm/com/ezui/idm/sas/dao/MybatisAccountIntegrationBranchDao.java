package com.ezui.idm.sas.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import com.ezui.idm.dao.EzDepartmentDao;
import com.ezui.idm.entity.IEzDepartment;
import com.ezui.idm.mapping.param.EzDepartmentQueryImpl;

@Component
public class MybatisAccountIntegrationBranchDao extends EzDepartmentDao
		implements AccountIntegrationBranchDao {

	@Override
	public long countDepartmentByQuery(EzDepartmentQueryImpl query) {
		SqlSession session = this.getSqlSession();
		return session.selectOne(MybatisAccountIntegrationBranchDao.class.getName() + "." + "countByQueryCriteria", query);
	}

	@Override
	public List<IEzDepartment> selectDepartmentByQuery(EzDepartmentQueryImpl query) {
		SqlSession session = this.getSqlSession();
		String orderBy = query.getOrderByColumns();
		if (orderBy == null || orderBy.trim().length() == 0) {
			query.setOrderByColumns("RES.dept_name asc");
		}
		query.setReadUncommited(true);
		query.setDistinct(true);
		return session.selectList(MybatisAccountIntegrationBranchDao.class.getName() + "." + "selectByQueryCriteria", query);
	}
}
