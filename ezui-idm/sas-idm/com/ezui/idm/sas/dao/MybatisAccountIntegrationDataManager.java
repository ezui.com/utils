package com.ezui.idm.sas.dao;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.ezui.idm.dao.EzUserDao;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.mapping.param.EzUserQueryImpl;
import com.ezui.idm.sas.entity.AccountIntegrationEntity;
import com.ezui.idm.sas.entity.AccountIntegrationEntityImpl;
import com.ezui.idm.sas.entity.AccountIntegrationRoleEntityImpl;
import com.ezui.idm.sas.mapping.param.AccountIntegrationQueryImpl;

public class MybatisAccountIntegrationDataManager extends EzUserDao implements AccountIntegrationDataManager {

	public MybatisAccountIntegrationDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends AccountIntegrationEntity> getManagedEntityClass() {
		return AccountIntegrationEntityImpl.class;
	}

	@Override
	public AccountIntegrationEntity create() {
		return new AccountIntegrationEntityImpl();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
		return getDbSqlSession().selectList("selectAccountIntegrationByQueryCriteria", query, page);
	}

	@Override
	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		return (Long) getDbSqlSession().selectOne("selectAccountIntegrationCountByQueryCriteria", query);
	}
	@Override
	public List<Group> findGroupsByUser(String userId) {
		return getSqlSession().selectList(AccountIntegrationRoleEntityImpl.class.getName() + ".selectAccountIntegrationRolesByUserId", userId);
	}

	@SuppressWarnings("unchecked")
	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		return getDbSqlSession().selectListWithRawParameter("selectAccountIntegrationByNativeQuery", parameterMap,
				firstResult, maxResults);
	}

	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		return (Long) getDbSqlSession().selectOne("selectAccountIntegrationCountByNativeQuery", parameterMap);
	}
	@Override
	public AccountIntegrationEntity findByLoginId(String loginId) {
		return getSqlSession().selectOne(AccountIntegrationEntityImpl.class.getName() + ".selectAccountIntegration", loginId);
	}
	@Override
	public AccountIntegrationEntity findById(String id){
		return getSqlSession().selectOne(AccountIntegrationEntityImpl.class.getName() + ".selectAccountIntegration", id);
	}
	@Override
	public Long countByQueryCriteria(EzUserQueryImpl query){
		return this.countByQueryCriteria(new AccountIntegrationQueryImpl(query));
	}
	@Override
	public List<IEzUser> selectByQueryCriteria(EzUserQueryImpl query){
		return this.selectByQueryCriteria(new AccountIntegrationQueryImpl(query));
	}
	@Override
	public Long countByQueryCriteria(AccountIntegrationQueryImpl query){
		return getSqlSession().selectOne(AccountIntegrationEntityImpl.class.getName() + ".selectAccountIntegrationCountByQueryCriteria", query);
	}
	@Override
	public List<IEzUser> selectByQueryCriteria(AccountIntegrationQueryImpl query){
		return getSqlSession().selectList(AccountIntegrationEntityImpl.class.getName() + ".selectAccountIntegrationByQueryCriteria", query);
	}
	@Override
	public List<IEzUser> getUsersInBranch(String deptNo) {
		AccountIntegrationQueryImpl query=new AccountIntegrationQueryImpl();
		query.setOu(deptNo);
		return this.selectByQueryCriteria(query);
	}
	@Override
	public List<IEzUser> getUsersXRolesInBranch(String deptNo, String[] roles) {
		AccountIntegrationQueryImpl query=new AccountIntegrationQueryImpl();
		query.setOu(deptNo);
		query.setRoles(roles);
		return this.selectByQueryCriteria(query);
	}
}
