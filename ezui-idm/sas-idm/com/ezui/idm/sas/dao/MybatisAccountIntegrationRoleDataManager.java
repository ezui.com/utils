package com.ezui.idm.sas.dao;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.GroupEntity;

import com.ezui.idm.dao.EzGroupDao;
import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.sas.entity.AccountIntegrationRoleEntityImpl;

public class MybatisAccountIntegrationRoleDataManager extends EzGroupDao
		implements AccountIntegrationRoleDataManager {

	public MybatisAccountIntegrationRoleDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
		super(processEngineConfiguration);
	}

	@Override
	public Class<? extends GroupEntity> getManagedEntityClass() {
		return AccountIntegrationRoleEntityImpl.class;
	}

	@Override
	public GroupEntity create() {
		return new AccountIntegrationRoleEntityImpl();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		return getDbSqlSession().selectList("selectAccountIntegrationRoleByQueryCriteria", query, page);
	}

	@Override
	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		return (Long) getDbSqlSession().selectOne("selectAccountIntegrationRoleCountByQueryCriteria", query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupsByUser(String userId) {
		return getDbSqlSession().selectList("selectAccountIntegrationRolesByUserId", userId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		return getDbSqlSession().selectListWithRawParameter("selectAccountIntegrationRoleByNativeQuery", parameterMap,
				firstResult, maxResults);
	}

	@Override
	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		return (Long) getDbSqlSession().selectOne("selectAccountIntegrationRoleCountByNativeQuery", parameterMap);
	}
	@Override
	public List<IEzGroup> selectGroupsByUserId(String userId) {
		return getSqlSession().selectList(AccountIntegrationRoleEntityImpl.class.getName() + ".selectAccountIntegrationRolesByUserId", userId);
	}
	@Override
	public List<IEzGroup> selectAllGroupsByUserId(String userId) {
		return getSqlSession().selectList(AccountIntegrationRoleEntityImpl.class.getName() + ".selectAccountIntegrationRolesByUserId", userId);
	}

}
