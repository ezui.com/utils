package com.ezui.idm.sas.entity;

import com.ezui.idm.entity.EzDepartment;

public class AccountIntegrationBranch extends EzDepartment implements IAccountIntegrationBranch {
    private String deptName;

    private String ou;

    private static final long serialVersionUID = 1L;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou == null ? null : ou.trim();
        this.setDeptNo(this.ou);
        this.setId(this.ou);
    }
}