package com.ezui.idm.sas.entity;

import com.ezui.idm.entity.IEzUser;

public interface AccountIntegrationEntity extends IEzUser {
	public String getEmpId();

	public void setEmpId(String empId);

	public String getDeptName();

	public void setDeptName(String deptName);

	public String getDeptNo();

	public void setDeptNo(String deptNo);

	public String getEmail();

	public void setEmail(String email);

	public String getEmpName();

	public void setEmpName(String empName);

	public String getEncrypw();

	public void setEncrypw(String encrypw);

	public String getOu();

	public void setOu(String ou);
}
