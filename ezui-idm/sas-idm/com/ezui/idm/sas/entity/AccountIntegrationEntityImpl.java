package com.ezui.idm.sas.entity;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.identity.Picture;
import org.activiti.engine.impl.persistence.entity.ByteArrayRef;

import com.ezui.idm.entity.EzUser;

public class AccountIntegrationEntityImpl extends EzUser implements AccountIntegrationEntity {
	private String empId;

	private String deptName;

	private String deptNo;

	private String email;

	private String empName;

	private String encrypw;

	private String ou;

	private static final long serialVersionUID = 1L;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId == null ? null : empId.trim();
		this.setId(empId);
		this.setLoginId(empId);
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName == null ? null : deptName.trim();
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo == null ? null : deptNo.trim();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	public String getEmpName() {
		if (empName == null) {
			if (firstName != null && firstName.length() > 0 && lastName != null && lastName.length() > 0) {
				return firstName + " " + lastName;
			} else if (firstName != null && firstName.length() > 0) {
				return firstName;
			} else if (lastName != null && lastName.length() > 0) {
				return lastName;
			} else {
				return "";
			}
		}
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName == null ? null : empName.trim();
		this.setUsername(empName);
	}

	public String getEncrypw() {
		return encrypw;
	}

	public void setEncrypw(String encrypw) {
		this.encrypw = encrypw == null ? null : encrypw.trim();
	}

	public String getOu() {
		return ou;
	}

	public void setOu(String ou) {
		this.ou = ou == null ? null : ou.trim();
		this.setDefaultBranchNo(ou);
	}

	// AbstractEntity
	@Override
	public String getId() {
		return empId;
	}

	@Override
	public void setId(String id) {
		this.id = id;
		this.empId = id;
	}

	// org.activiti.engine.impl.persistence.entity.UserEntity
	protected String firstName;
	protected String lastName;
	protected String password;
	protected ByteArrayRef pictureByteArrayRef;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		if (this.lastName == null && this.firstName == null) {
			return empName;
		} else {
			return this.lastName;
		}
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return encrypw;
	}

	public void setPassword(String password) {
		this.password = password;
		this.encrypw = password;
	}

	public ByteArrayRef getPictureByteArrayRef() {
		return pictureByteArrayRef;
	}

	public void setPictureByteArrayRef(ByteArrayRef pictureByteArrayRef) {
		this.pictureByteArrayRef = pictureByteArrayRef;
	}

	@Override
	public boolean isPictureSet() {
		return pictureByteArrayRef != null && pictureByteArrayRef.getId() != null;
	}

	@Override
	public Object getPersistentState() {
		Map<String, Object> persistentState = new HashMap<String, Object>();
		persistentState.put("firstName", firstName);
		persistentState.put("lastName", lastName);
		persistentState.put("email", email);
		persistentState.put("password", password);

		if (pictureByteArrayRef != null) {
			persistentState.put("pictureByteArrayId", pictureByteArrayRef.getId());
		}

		return persistentState;
	}

	public Picture getPicture() {
		if (pictureByteArrayRef != null && pictureByteArrayRef.getId() != null) {
			return new Picture(pictureByteArrayRef.getBytes(), pictureByteArrayRef.getName());
		}
		return null;
	}

	public void setPicture(Picture picture) {
		if (picture != null) {
			savePicture(picture);
		} else {
			deletePicture();
		}
	}

	protected void savePicture(Picture picture) {
		if (pictureByteArrayRef != null) {
			pictureByteArrayRef = new ByteArrayRef();
		}
		pictureByteArrayRef.setValue(picture.getMimeType(), picture.getBytes());
	}

	protected void deletePicture() {
		if (pictureByteArrayRef != null) {
			pictureByteArrayRef.delete();
		}
	}

	@Override
	public String getUserSource() {
		return "inline password";
	}
    @Override
	public String getUsername() {
        return empName;
    }

    @Override
	public void setUsername(String username) {
        this.empName = username == null ? null : username.trim();
    }

}
