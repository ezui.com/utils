package com.ezui.idm.sas.entity;

import java.util.HashMap;
import java.util.Map;

import com.ezui.idm.entity.EzGroup;

public class AccountIntegrationRoleEntityImpl extends EzGroup implements AccountIntegrationRoleEntity {
	private static final long serialVersionUID = 1047623407540608218L;
	private String empId;

	private String role;

	private String roleDesc;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId == null ? null : empId.trim();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role == null ? null : role.trim();
		this.setGroupName(role);
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc == null ? null : roleDesc.trim();
	}

	// AbstractEntity
	@Override
	public String getId() {
		return role;
	}

	@Override
	public void setId(String id) {
		this.id = id;
		this.role = id;
	}

	// GroupEntityImpl
	protected String name;
	protected String type = "security-role";

	public AccountIntegrationRoleEntityImpl() {
	}

	public Object getPersistentState() {
		Map<String, Object> persistentState = new HashMap<String, Object>();
		persistentState.put("name", name);
		persistentState.put("type", type);
		return persistentState;
	}

	public String getName() {
		return roleDesc;
	}

	public void setName(String name) {
		this.name = name;
		this.roleDesc = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
