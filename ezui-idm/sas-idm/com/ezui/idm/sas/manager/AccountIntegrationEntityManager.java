package com.ezui.idm.sas.manager;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;

import com.ezui.idm.sas.entity.AccountIntegrationEntity;
import com.ezui.idm.service.IEzUserService;

public interface AccountIntegrationEntityManager extends IEzUserService {
	  User createNewUser(String userId);

	  void updateUser(User updatedUser);

	  List<User> findUserByQueryCriteria(UserQueryImpl query, Page page);

	  long findUserCountByQueryCriteria(UserQueryImpl query);

	  List<Group> findGroupsByUser(String userId);

	  UserQuery createNewUserQuery();

	  Boolean checkPassword(String userId, String password);

	  List<User> findUsersByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults);

	  long findUserCountByNativeQuery(Map<String, Object> parameterMap);

	  boolean isNewUser(User user);

	  Picture getUserPicture(String userId);

	  void setUserPicture(String userId, Picture picture);
	  
	  void deletePicture(User user);
	  public AccountIntegrationEntity findById(String entityId);
}
