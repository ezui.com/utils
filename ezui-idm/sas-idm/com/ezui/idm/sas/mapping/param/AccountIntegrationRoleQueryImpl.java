package com.ezui.idm.sas.mapping.param;

import java.util.List;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.GroupQueryProperty;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;

public class AccountIntegrationRoleQueryImpl extends GroupQueryImpl {
	private String empId;

	private String role;

	private String roleDesc;
	private String roleDescLike;

	private static final long serialVersionUID = 1L;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId == null ? null : empId.trim();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role == null ? null : role.trim();
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc == null ? null : roleDesc.trim();
	}

	public String getRoleDescLike() {
		return roleDescLike;
	}

	public void setRoleDescLike(String roleDescLike) {
		this.roleDescLike = roleDescLike;
	}

	// GroupQueryImpl
	protected String id;
	protected String name;
	protected String nameLike;
	protected String type;
	protected String userId;
	protected String procDefId;

	public AccountIntegrationRoleQueryImpl() {
	}

	public AccountIntegrationRoleQueryImpl(CommandContext commandContext) {
		super(commandContext);
	}

	public AccountIntegrationRoleQueryImpl(CommandExecutor commandExecutor) {
		super(commandExecutor);
	}

	public GroupQuery groupId(String id) {
		if (id == null) {
			throw new ActivitiIllegalArgumentException("Provided id is null");
		}
		this.id = id;
		this.role = id;
		return this;
	}

	public GroupQuery groupName(String name) {
		if (name == null) {
			throw new ActivitiIllegalArgumentException("Provided name is null");
		}
		this.name = name;
		this.roleDesc = name;
		return this;
	}

	public GroupQuery groupNameLike(String nameLike) {
		if (nameLike == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided nameLike is null");
		}
		this.nameLike = nameLike;
		this.roleDescLike = nameLike;
		return this;
	}

	public GroupQuery groupType(String type) {
		if (type == null) {
			throw new ActivitiIllegalArgumentException("Provided type is null");
		}
		this.type = type;
		return this;
	}

	public GroupQuery groupMember(String userId) {
		if (userId == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided userId is null");
		}
		this.userId = userId;
		this.empId = userId;
		return this;
	}

	public GroupQuery potentialStarter(String procDefId) {
		if (procDefId == null) {
			throw new ActivitiIllegalArgumentException(
					"Provided processDefinitionId is null or empty");
		}
		this.procDefId = procDefId;
		return this;

	}

	// sorting ////////////////////////////////////////////////////////
	public static final GroupQueryProperty GROUP_ID = new GroupQueryProperty(
			"RES.ROLE");
	public static final GroupQueryProperty NAME = new GroupQueryProperty(
			"RES.ROLE_DESC");
	public static final GroupQueryProperty TYPE = new GroupQueryProperty(
			"RES.ROLE");

	public GroupQuery orderByGroupId() {
		return orderBy(GROUP_ID);
	}

	public GroupQuery orderByGroupName() {
		return orderBy(NAME);
	}

	public GroupQuery orderByGroupType() {
		return orderBy(TYPE);
	}

	public String getOrderBy() {
		if (orderBy == null) {
			return "RES.ROLE asc";
		} else {
			return orderBy;
		}
	}
	// results ////////////////////////////////////////////////////////
	@Override
	public long executeCount(CommandContext commandContext) {
		checkQueryOk();
		return commandContext.getGroupEntityManager()
				.findGroupCountByQueryCriteria(this);
	}

	@Override
	public List<Group> executeList(CommandContext commandContext, Page page) {
		checkQueryOk();
		return commandContext.getGroupEntityManager().findGroupByQueryCriteria(
				this, page);
	}

	// getters ////////////////////////////////////////////////////////

	public String getId() {
		return role;
	}

	public String getName() {
		return roleDesc;
	}

	public String getNameLike() {
		return roleDescLike;
	}

	public String getType() {
		return type;
	}

	public String getUserId() {
		return empId;
	}

	public String getProcDefId() {
		return procDefId;
	}
}
