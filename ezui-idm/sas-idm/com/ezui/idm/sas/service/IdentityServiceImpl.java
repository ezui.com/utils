package com.ezui.idm.sas.service;

import java.util.List;

import com.ezui.idm.entity.IEzGroup;
import com.ezui.idm.entity.IEzPermission;
import com.ezui.idm.entity.IEzUser;
import com.ezui.idm.service.IEzGroupService;
import com.ezui.idm.service.IEzPermissionService;
import com.ezui.idm.service.IEzUserService;
import com.ezui.idm.service.IdentityService;

public class IdentityServiceImpl implements IdentityService {
	private IEzUserService userService;
	private IEzGroupService groupService;
	private IEzPermissionService permissionService;

	@Override
	public List<IEzGroup> selectGroupsByUserId(String userId) {
		return this.groupService.selectGroupsByUserId(userId);
	}
	@Override
	public List<IEzGroup> selectAllGroupsByUserId(String userId) {// 包括遞歸查詢的子group
		return this.groupService.selectAllGroupsByUserId(userId);
	}
	@Override
	public int addPermission(IEzPermission entity) {
		return permissionService.insert(entity);
	}
	@Override
	public List<IEzPermission> selectPermissionsByUserId(String userId) {
		return this.permissionService.selectPermissionsByUserId(userId);
	}
	@Override
	public IEzUser findByLoginId(String loginId) {
		return this.userService.findByLoginId(loginId);
	}

	public IEzUserService getUserService() {
		return userService;
	}
	public void setUserService(IEzUserService userService) {
		this.userService = userService;
	}
	public IEzGroupService getGroupService() {
		return groupService;
	}
	public void setGroupService(IEzGroupService groupService) {
		this.groupService = groupService;
	}
	public IEzPermissionService getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(IEzPermissionService permissionService) {
		this.permissionService = permissionService;
	}

}
