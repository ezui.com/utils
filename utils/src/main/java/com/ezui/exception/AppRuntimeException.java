package com.ezui.exception;

/**
 * 通用exception
 * 
 * @company ezui.com
 * @author Xiandong Liu
 * @email xiandong@ezui.com
 * @date 2022年4月27日
 */
public class AppRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * 返回的异常信息
	 * 
	 * <pre>
	 * 通过restful接口返回到前端的信息
	 * </pre>
	 */
	protected String message;

	public AppRuntimeException() {
		super();
	}

	public AppRuntimeException(Throwable cause) {
		super(cause);
	}

	public AppRuntimeException(String message) {
		super(message);
		this.message = message;
	}

	public AppRuntimeException(String message, Throwable cause) {
		super(message, cause);
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public Throwable fillInStackTrace() {
		return this;
	}

}