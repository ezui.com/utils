package com.ezui.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Base64Util {
	public static void main(String[] args) throws IOException {
//		String base64Str=encodeToString("d:\\software\\2._OpenText_Product_Compatibility_Matrix_(Current_Maintenance).pdf",true);
//		System.out.println("data length:" + base64Str.length());
//        OutputStream out = new FileOutputStream("d:\\software\\test.txt");
//        out.write(base64Str.getBytes());
//        out.flush();
//        out.close();
//		
//        Base64Util.base64DecodeToFile(base64Str,true,"d:\\software\\test.pdf");
	}

	public static String encodeToString(String filepath, boolean compress) throws IOException {
		byte[] data = loadFile(filepath, compress);
		return Base64.getEncoder().encodeToString(data);
	}

	public static String encodeToString(InputStream in, boolean compress) throws IOException {
		byte[] data = encodeToByte(in, compress);
		return Base64.getEncoder().encodeToString(data);
	}

	/**
	 * 加载本地文件,并转换为byte数组
	 * 
	 * @return
	 * @throws IOException
	 */
	public static byte[] loadFile(String filepath, boolean compress) throws IOException {
		File file = new File(filepath);

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			return encodeToByte(fis, compress);
		} finally {
			try {
				if (fis != null) {
					fis.close();
					fis = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static byte[] encodeToByte(InputStream in, boolean compress) throws IOException {
		ByteArrayOutputStream baos = null;
		GZIPOutputStream gzip = null;
		byte[] data = null;

		try {
			baos = new ByteArrayOutputStream();
			if (compress) {
				gzip = new GZIPOutputStream(baos);
			}
			byte[] buffer = new byte[1024];
			int len = -1;
			while ((len = in.read(buffer)) != -1) {
				if (compress) {
					gzip.write(buffer, 0, len);
				} else {
					baos.write(buffer, 0, len);
				}
			}
			if (compress) {
				gzip.finish();
				gzip.flush();
				data = baos.toByteArray();
			} else {
				data = baos.toByteArray();
			}

		} finally {
			try {
				in.close();
				in = null;
				if (gzip != null) {
					gzip.close();
					gzip = null;
				}
				baos.close();
				baos = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	/**
	 * 对byte[]进行压缩
	 * 
	 * @param 要压缩的数据
	 * @return 压缩后的数据
	 * @throws IOException
	 */
	public static byte[] compress(byte[] data) throws IOException {

		GZIPOutputStream gzip = null;
		ByteArrayOutputStream baos = null;
		byte[] newData = null;

		try {
			baos = new ByteArrayOutputStream();
			gzip = new GZIPOutputStream(baos);

			gzip.write(data);
			gzip.finish();
			gzip.flush();

			newData = baos.toByteArray();
		} finally {
			try {
				gzip.close();
				baos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return newData;
	}

	/**
	 * 对byte[]进行压缩
	 * 
	 * @param 要压缩的数据
	 * @return 压缩后的数据
	 * @throws IOException
	 */
	public static byte[] uncompress(byte[] b) throws IOException {
		ByteArrayOutputStream out = null;
		ByteArrayInputStream in = null;
		try {
			out = new ByteArrayOutputStream();

			in = new ByteArrayInputStream(b);

			GZIPInputStream gin = new GZIPInputStream(in);

			byte[] buffer = new byte[256];

			int n;

			while ((n = gin.read(buffer)) >= 0) {

				out.write(buffer, 0, n);

			}

			return out.toByteArray();
		} finally {
			try {
				if (out != null) {
					out.close();
					out = null;
				}
				if (in != null) {
					in.close();
					in = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 本地文件转换成base64字符串
	 * 
	 * @param file 本地文件全路径 （注意：带文件名） (将文件转化为字节数组字符串，并对其进行Base64编码处理)
	 * @return
	 * @throws IOException
	 */
	public static String fileToBase64ByLocal(String file) throws IOException {
		byte[] data = null;

		// 读取文件字节数组
		InputStream in = new FileInputStream(file);
		data = new byte[in.available()];
		in.read(data);
		in.close();

		// 返回Base64编码过的字节数组字符串
		return Base64.getEncoder().encodeToString(data);
	}

	/**
	 * base64字符串转换成文件 (对字节数组字符串进行Base64解码并生成文件)
	 * 
	 * @param data     base64字符串
	 * @param filePath 指定文件存放路径 （注意：带文件名）
	 * @return
	 * @throws IOException
	 */
	public static byte[] base64Decode(String data, boolean uncompress) throws IOException {
		// Base64解码
		byte[] b = Base64.getDecoder().decode(data);
//           for (int i = 0; i < b.length; ++i) {
//               if (b[i] < 0) {// 调整异常数据
//                   b[i] += 256;
//               }
//           }
		if (uncompress) {
			b = uncompress(b);
		}
		return b;
	}

	public static void base64DecodeToFile(String data, boolean uncompress, String filePath) throws IOException {
		OutputStream out = new FileOutputStream(filePath);
		out.write(base64Decode(data, uncompress));
		out.flush();
		out.close();
	}
}
