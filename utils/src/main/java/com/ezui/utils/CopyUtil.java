package com.ezui.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CopyUtil {
	public static <T, S extends Object> T objectCopy(S old, Class<T> c)
			throws IllegalAccessException, InstantiationException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		T ver = c.getDeclaredConstructor().newInstance();
		Method[] ms = old.getClass().getMethods();
		for (int i = 0; i < ms.length; i++) {
			Method m = ms[i];
			String mn = m.getName();
			if (mn.startsWith("get") && m.getParameterCount() == 0) {
				try {
					Object v = m.invoke(old);
					Method setm = c.getMethod("set" + mn.substring(3), m.getReturnType());
					if (setm != null) {
						setm.invoke(ver, v);
					}
				} catch (Throwable e) {
				}
			}
		}
		return ver;
	}

}
