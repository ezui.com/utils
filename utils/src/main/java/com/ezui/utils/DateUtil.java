package com.ezui.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

@SuppressWarnings("all")
public class DateUtil {
	public static final String YYYYMMDD = "yyyyMMdd";
	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String YEAR_MONTH_DAY_SECOND = "yyyy-MM-dd HH:mm:ss";
	public static final String YEAR_MONTH_DAY_MINISECOND_GMT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	public static final String YEAR_MONTH_DAY_MINISECOND_UTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String YEAR_MONTH_DAY_SECOND_SLASH = "yyyy/MM/dd HH:mm:ss";
	public static final String YEAR_MONTH_DAY_SECOND_SCHINESE = "yyyy年MM月dd日 HH时mm分ss秒";
	public static final String YEAR_MONTH_DAY_SECOND_TCHINESE = "yyyy年MM月dd日 HH時mm分ss秒";
	public static final String YEAR_MONTH_DAY_MID_LINE = "yyyy-MM-dd";
	public static final String YEAR_MONTH_DAY_CHINESE = "yyyy年MM月dd日";

	/**
	 * 采用 ThreadLocal 避免 SimpleDateFormat 非线程安全的问题
	 * <p>
	 * Key —— 时间格式 Value —— 解析特定时间格式的 SimpleDateFormat
	 */
	private static ThreadLocal<Map<String, SimpleDateFormat>> sThreadLocal = new ThreadLocal<Map<String, SimpleDateFormat>>();

	/**
	 * 获取解析特定时间格式的 SimpleDateFormat
	 *
	 * @param pattern 时间格式
	 */
	private static SimpleDateFormat getDateFormat(String pattern) {
		Map<String, SimpleDateFormat> strDateFormatMap = sThreadLocal.get();

		if (strDateFormatMap == null) {
			strDateFormatMap = new HashMap<String, SimpleDateFormat>();
		}

		SimpleDateFormat simpleDateFormat = strDateFormatMap.get(pattern);
		if (simpleDateFormat == null) {
			simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
			strDateFormatMap.put(pattern, simpleDateFormat);
			sThreadLocal.set(strDateFormatMap);
		}

		return simpleDateFormat;
	}

	/**
	 * 时间格式化
	 *
	 * @param date：要格式化的时间
	 * @param pattern：要格式化的类型
	 */
	public static String formatDate(Date date, String pattern) {
		if (date == null || pattern == null) {
			return null;
		}

		return getDateFormat(pattern).format(date);
	}

	/**
	 * 解析時間
	 *
	 * @param date：日期
	 * @param pattern：要格式化的类型
	 */
	public static Date parse2GMT(String pattern, String date) throws ParseException {
		if (date == null || pattern == null) {
			return null;
		}

		SimpleDateFormat sdf = getDateFormat(pattern);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		return sdf.parse(date);
	}

	/**
	 * 解析時間
	 *
	 * @param date：日期
	 * @param pattern：要格式化的类型
	 */
	public static Date parse(String pattern, String date) throws ParseException {
		if (date == null || pattern == null) {
			return null;
		}

		return getDateFormat(pattern).parse(date);
	}

	/*
	 * 将民国年月日转成日期
	 */
	public static Date getTWyyymmdd(String yyymmdd) throws Exception {
		Date d = null;
		if (yyymmdd != null && yyymmdd.length() >= 6) {
			String yyy = yyymmdd.substring(0, yyymmdd.length() - 4);
			int yy = Integer.valueOf(yyy) + 1911;
			d = new SimpleDateFormat("yyyyMMdd").parse(yy + yyymmdd.substring(yyymmdd.length() - 4));
		}
		return d;
	}

	/**
	 * 将日期转成民国年月日
	 */
	public static String getTWyyymmdd(Date d) {
		String yyyymmdd = new SimpleDateFormat("yyyyMMdd").format(d);
		String yy = "000" + (Integer.valueOf(yyyymmdd.substring(0, yyyymmdd.length() - 4)) - 1911);
		return yy.substring(yy.length() - 3) + yyyymmdd.substring(yyyymmdd.length() - 4);
	}

	/**
	 * 将日期转成民国年/月/日 時:分:秒
	 */
	public static String getTWyyy_mm_dd_hh_mm_ss(Date d) {
		String yyyymmdd = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(d);
		String yy = "000" + (Integer.valueOf(yyyymmdd.substring(0, 4)) - 1911);
		return yy.substring(yy.length() - 3) + yyyymmdd.substring(4, yyyymmdd.length());
	}

	/*
	 * 获取下一个民国日
	 */
	public static String getTWNextDayyymmdd(String yyymmdd) throws Exception {
		Date d = getTWyyymmdd(yyymmdd);
		return getTWyyymmdd(getNextDay(d));
	}

	/*
	 * 获取下一日
	 */
	public static Date getNextDay(Date d) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/*
	 * 是否为今天
	 */
	public static boolean isToDay(Date d) {
		if (new SimpleDateFormat("yyyyMMdd").format(d).equals(new SimpleDateFormat("yyyyMMdd").format(new Date()))) {
			return true;
		} else {
			return false;
		}

	}

	public static String getDate_yyyy_mm_dd(String d, Integer h) throws Exception {
		if (d != null && d.length() > 0 && d.length() == 9) {
			int year = Integer.parseInt(d.substring(0, 3)) + 1911;
			String month = d.substring(4, 6);
			String day = d.substring(7, 9);
			StringBuilder date = new StringBuilder();
			String hour = "00";
			if (h != null) {
				hour = h < 10 ? "0" + h : String.valueOf(h);
			}
			date.append(year).append("/").append(month).append("/").append(day).append(" ").append(hour).append(":00:00");

			return date.toString();
		} else {
			return null;
		}
	}

	@SuppressWarnings("all")
	/**
	 * 轉民國
	 * 
	 * @param value
	 * @return
	 */
	public static String ToROC(Object value, String defaultFormat) {
		if (value == null || "".equals(value)) {
			return "";
		}
		String a = null;
		String date[] = null;
		if (value instanceof String) {
			String ox = (String) value;
			if (ox.length() >= 10) {
				a = (ox.substring(0, 10)).replaceAll("-", "/");
			} else {
				a = (ox.substring(0, 7)).replaceAll("-", "/");
			}
			if (ox.indexOf("/") == -1) {
				if (ox.length() > 10) {
					a = ox.substring(0, 4) + "/" + ox.substring(4, 6) + "/" + ox.substring(6, 8);
				}
			}
			date = a.split("/");
			date[0] = String.valueOf(Integer.parseInt(date[0]) - 1911);
			if (Integer.parseInt(date[0]) < 100 && a.length() >= 10)
				date[0] = "0" + date[0];
		} else if (value instanceof Date) {
			a = getDateFormat(defaultFormat).format(value);
			date = a.split("/");
			date[0] = String.valueOf(Integer.parseInt(date[0]) - 1911);
			if (Integer.parseInt(date[0]) < 100 && a.length() >= 10)
				date[0] = "0" + date[0];
		}
		if (a != null && date != null && a.length() < 8)// NOSONAR
			return "0" + date[0] + "/" + date[1];
		if (date != null)
			return date[0] + "/" + date[1] + "/" + date[2];
		return "";
	}

	public static Date getCurrentUTCTime() {
		// 1、取得本地时间：
		Calendar cal = Calendar.getInstance();
		// 2、取得时间偏移量：
		int zoneOffset = cal.get(java.util.Calendar.ZONE_OFFSET);
		// 3、取得夏令时差：
		int dstOffset = cal.get(java.util.Calendar.DST_OFFSET);
		// 4、从本地时间里扣除这些差量，即可以取得UTC时间：
		cal.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
		return cal.getTime();
	}

	public static String getCurrentUTCTimeStr() {
		SimpleDateFormat udateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_MINISECOND_UTC);
		return udateFormat.format(getCurrentUTCTime());
	}

	@SuppressWarnings("all")
	public static void main(String[] args) throws Exception {
//		System.out.println(getCurrentUTCTime());
//		
		System.out.println(getCurrentUTCTimeStr());
//		2022-06-22T14:27:48.931+08:00
		System.out.println(formatDate(new Date(), YEAR_MONTH_DAY_MINISECOND_GMT));
//		 final SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_MINISECOND_UTC,Locale.JAPAN);
//		 System.out.println(TimeZone.getDefault());
////		 dateFormat.setTimeZone(TimeZone.getDefault());
//		 System.out.println(TimeZone.getTimeZone("GMT"));
//		 dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
//		 System.out.println(dateFormat.parse("2018-11-22T00:00:00.000Z"));
//		final String dateTime = "2016-12-30 15:35:34";
//		for (int i = 0; i < 50; i++) {
//			new Thread(new Runnable() {
//				@Override
//				public void run() {
//					for (int i = 0; i < 50; i++) {
//						try {
//							System.out.println(Thread.currentThread().getName() + "\t Util:"
//									+ DateUtil.parse(DateUtil.YEAR_MONTH_DAY_SECOND, dateTime));
//							System.out.println(Thread.currentThread().getName() + "\t Util:"
//									+ DateUtil.formatDate(new Date(), DateUtil.YEAR_MONTH_DAY_SECOND));
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						// try {
//						// System.out.println(Thread.currentThread().getName() +
//						// "\t STAT:" + dateFormat.parse(dateTime));
//						// } catch (ParseException e) {
//						// e.printStackTrace();
//						// }
//					}
//				}
//			}).start();
//		}
//		try {
//			Thread.sleep(1000 * 2);
//		} catch (Exception e) {
//		}
	}
}
