package com.ezui.utils;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class EmailAccountAuthenticator extends Authenticator {
	private String user;
	private String pwd;

	public EmailAccountAuthenticator(String username, String userpwd) {
		user = username;
		pwd = userpwd;
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(user, pwd);
	}
}
