package com.ezui.utils;

import java.io.StringWriter;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

/**
 * @author
 */
public class EmailNotifyUtil {

	private static EmailNotifyUtil instance = null;

	public static EmailNotifyUtil getInstance() {
		if (instance == null) {
			instance = new EmailNotifyUtil();
		}
		return instance;
	}

	private EmailNotifyUtil() {
	}

	/**
	 * 
	 * @param templateFileName
	 * @param attributes
	 * 
	 * @return
	 * @throws Exception
	 */
	public String constructEmailBody(String templateFileName, Hashtable<String, Object> attributes) throws Exception {
		Velocity.init();
		VelocityContext context = new VelocityContext();

		Enumeration<String> e = attributes.keys();
		String element;
		while (e.hasMoreElements()) {
			element = e.nextElement();
			context.put(element, attributes.get(element));
		}

		StringWriter writer = new StringWriter();
		Velocity.mergeTemplate(templateFileName, "UTF-8", context, writer);

		return writer.toString();
	}

	/**
	 * 
	 * 
	 * @param propertiesFile
	 * @param templateFileName
	 * @param attributes
	 * 
	 * @return
	 * @throws Exception
	 */
	public String constructEmailBody(String propertieFile, String templateFileName, Hashtable<String, Object> attributes) throws Exception {
		if (templateFileName != null && templateFileName.length() == 0)
			return "";
		System.setProperty("file.separator", "/");
		Velocity.init(propertieFile);

		VelocityContext context = new VelocityContext();

		Enumeration<String> e = attributes.keys();
		String element;
		while (e.hasMoreElements()) {
			element = e.nextElement();
			context.put(element, attributes.get(element));
		}

		StringWriter writer = new StringWriter();
		Velocity.mergeTemplate(templateFileName, "UTF-8", context, writer);

		return writer.toString();

	}

}
