package com.ezui.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class FileEncodeUtil {

	private static final String algorithm = "DESede";

	private static final String algorithmMode = "DESede/CBC/PKCS5Padding";

	private static final String iv = "00000000";

	private SecretKey key;
	private Cipher cipher;
	private IvParameterSpec ivSpec;

	/**
	 * 建構式
	 * 
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 */
	public FileEncodeUtil(String desKey) throws NoSuchPaddingException, NoSuchAlgorithmException {
		key = new SecretKeySpec(desKey.getBytes(), algorithm);
		ivSpec = new IvParameterSpec(iv.getBytes());
		cipher = Cipher.getInstance(algorithmMode);
	}

	/**
	 * 初使化加密cipher
	 * 
	 * @return
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 */
	public boolean initEncrypt() throws InvalidKeyException, InvalidAlgorithmParameterException {
		boolean ret = false;
		cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
		ret = true;
		return ret;
	}

	/**
	 * 初使化解密cipher
	 * 
	 * @return
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 */
	public boolean initDecrypt() throws InvalidKeyException, InvalidAlgorithmParameterException {
		boolean ret = false;
		cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
		ret = true;
		return ret;
	}

	/**
	 * 提供UTF-8字串資料使用Base64編碼
	 * 
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String encodeBase64(String str) throws UnsupportedEncodingException {
		if (str == null) {
			return null;
		}
		return Base64.getEncoder().encodeToString(str.getBytes("UTF-8"));
	}

	/**
	 * 提供UTF-8字串資料使用Base64解碼
	 * 
	 * @param encStr
	 * @return
	 * @throws Exception
	 */
	public static String decodeBase64(String encStr) throws Exception {
		if (encStr == null) {
			return null;
		}
		return new String(Base64.getDecoder().decode(encStr), "UTF-8");
	}

	/**
	 * 檔案以 DES3 加密後回傳 BASE4 格式字串
	 * 
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public String encryptFile(File f) throws InvalidAlgorithmParameterException, InvalidKeyException,
			BadPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, IOException {
		if (initEncrypt()) {
			byte[] encrypt = cipher.doFinal(getFileHash(f, "SHA1"));
			return Base64.getEncoder().encodeToString(encrypt);
		} else {
			throw new InvalidKeyException("加密失敗！File:" + f.getAbsolutePath());
		}
	}

	/**
	 * 解密還原檔案至指定的路徑
	 * 
	 * @param base64File     加密後的檔案BASE64格式
	 * @param decodeFilePath 還原檔案至指定的路徑
	 * @throws Exception
	 */
	public void decryptFile(String base64File, String decodeFilePath) throws IOException,
			InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

		byte[] decrypted = null;

		try (FileOutputStream fos = new FileOutputStream(new File(decodeFilePath))) {

			byte[] byteCode = Base64.getDecoder().decode(base64File);
			if (initDecrypt()) {
				decrypted = cipher.doFinal(byteCode);

				fos.write(decrypted);

				fos.flush();
			} else {
				throw new InvalidKeyException("解密失敗！");
			}

		}
	}

	/**
	 * 取得檔案 Hash
	 * 
	 * @param f
	 * @param algorithmName
	 * @return
	 * @throws Exception
	 */
	private static byte[] getFileHash(File f, String algorithmName) throws IOException, NoSuchAlgorithmException {
		if (f != null && f.exists()) {
			// 取得檔案的 Hash
			try (InputStream fis = new FileInputStream(f)) {
				byte[] buffer = new byte[1024];
				MessageDigest md = MessageDigest.getInstance(algorithmName);
				int numRead;

				do {
					numRead = fis.read(buffer);
					if (numRead > 0) {
						md.update(buffer, 0, numRead);
					}
				} while (numRead != -1);

				return md.digest();
			}
		} else {
			throw new IOException("File is null or not existed!!");// NOSONAR
		}
	}

	/**
	 * 取得檔案 SHA1 digest
	 * 
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public static String getSHA1Digest(File f) throws IOException, NoSuchAlgorithmException {
		if (f != null && f.exists()) {
			// byte[] 轉換為 HEX 字串
			byte[] mdbytes = getFileHash(f, "SHA-1");
			return byteArray2Hex(mdbytes);
		} else {
			throw new IOException("File is null or not existed!");// NOSONAR
		}
	}

	/**
	 * 取得檔案 SHA256 digest
	 * 
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public static String getSHA256Digest(File f) throws NoSuchAlgorithmException, IOException {
		if (f != null && f.exists()) {
			// byte[] 轉換為 HEX 字串
			byte[] mdbytes = getFileHash(f, "SHA-256");
			return byteArray2Hex(mdbytes);
		} else {
			throw new IOException("File is null or not existed!");// NOSONAR
		}

	}

	/**
	 * 取得檔案 MD5 digest
	 * 
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public static String getMD5Digest(File f) throws IOException, NoSuchAlgorithmException {
		if (f != null && f.exists()) {
			// byte[] 轉換為 HEX 字串
			byte[] mdbytes = getFileHash(f, "MD5");
			return byteArray2Hex(mdbytes);
		} else {
			throw new IOException("File is null or not existed!");
		}
	}

	/**
	 * Convert byte array into hex.
	 * 
	 * @param hash
	 * @return
	 */
	public static String byteArray2Hex(byte[] hash) {
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < hash.length; i++) {
			sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	public static void main(String[] args) throws Exception {
//		File f = new File("D:/syncplicity/temp/7601.17514.101119-1850_x64fre_server_eval_en-us-GRMSXEVAL_EN_DVD.iso");// NOSONAR
		File f = new File("D:/work/temp/1047023603-0.pdf");// NOSONAR
		long begin = System.nanoTime();
		System.out.println("SHA-1 =>" + FileEncodeUtil.getSHA1Digest(f) + " (len: "// NOSONAR
				+ FileEncodeUtil.getSHA1Digest(f).length() + ")");// NOSONAR
		System.out.println("spent:" + (System.nanoTime() - begin));// NOSONAR
		begin = System.nanoTime();
		System.out.println("MD5   =>" + FileEncodeUtil.getMD5Digest(f) + " (len: "// NOSONAR
				+ FileEncodeUtil.getMD5Digest(f).length() + ")");// NOSONAR
		System.out.println("spent:" + (System.nanoTime() - begin));// NOSONAR
		begin = System.nanoTime();
		System.out.println("SHA-256 =>" + FileEncodeUtil.getSHA256Digest(f) + " (len: "// NOSONAR
				+ FileEncodeUtil.getSHA256Digest(f).length() + ")");// NOSONAR
		System.out.println("spent:" + (System.nanoTime() - begin));// NOSONAR
		System.exit(1);
	}

}
