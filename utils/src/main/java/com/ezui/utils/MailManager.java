package com.ezui.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailManager {
	public MailManager() throws IOException {
		InputStream inputStream = checkConfFile();
		properties = new Properties();
		properties.load(new BufferedInputStream(inputStream));

		String smtpServer = properties.getProperty("smtp.host");
		properties.setProperty("smtp.host", smtpServer);
		setDefault();
	}

	public MailManager(String mail_Header, String mail_Footer) throws IOException {
		InputStream inputStream = checkConfFile();
		properties = new Properties();
		properties.load(new BufferedInputStream(inputStream));

		String smtpServer = properties.getProperty("smtp.host");
		properties.setProperty("smtp.host", smtpServer);
		setDefault();
		if (mail_Header != null)
			mailHeader = mail_Header + "<br><br>";
		if (mail_Footer != null)
			mailFooter = "<br><br>" + mail_Footer;
	}

	private InputStream checkConfFile() {
		InputStream stream = getClass().getResourceAsStream(CONFIG_FILE_PATH);
		if (stream == null)
			throw new RuntimeException("Can't find properties file '" + CONFIG_FILE_PATH + "'");
		else
			return stream;
	}

	protected void initMailSession() {
		if (smtpHost == null)
			smtpHost = properties.getProperty("smtp.host");
		Properties props = System.getProperties();
		props.put("mail.host", smtpHost);
		props.put("mail.transport.protocol", "smtp");
		if (true == Boolean.valueOf(properties.getProperty("smtp.auth"))) {
			props.clear();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.host", smtpHost); // smtp主机名。
			props.put("mail.smtp.user", properties.getProperty("mail.user")); // 发送方邮件地址。
			props.put("mail.smtp.password", properties.getProperty("mail.pwd")); // 邮件密码。

			EmailAccountAuthenticator popA = new EmailAccountAuthenticator(properties.getProperty("mail.user"), properties.getProperty("mail.pwd"));// 邮件安全认证。
			mailSession = Session.getInstance(props, popA);
		} else
			mailSession = Session.getDefaultInstance(props, null);
	}

	public void sendMail(String toRecipients[], String ccRecipients[], String subject, String body, String attachedFiles[]) throws MessagingException {
		initMailSession();
		mailSession.setDebug(false);
		MimeMessage message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress(mailFrom));
		message.setSubject(subject == null || subject.length() == 0 ? this.getSubject() : subject, "UTF-8");
		message.setRecipients(RecipientType.TO, buildInternetAddresses(toRecipients));
		if (ccRecipients != null)
			message.setRecipients(RecipientType.CC, buildInternetAddresses(ccRecipients));
		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();
		String mail = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
		mail = mail + "<html>\n";
		mail = mail + "<head>" + mailHeader + "</head>\n";

		mail = mail + "<body>" + body + mailFooter + "</body>\n";
		mail = mail + "</html>\n";
		messageBodyPart.setContent(mail, "text/html; charset=\"UTF-8\"");
		multipart.addBodyPart(messageBodyPart);
		try {
			if (attachedFiles != null) {
				for (int i = 0; i < attachedFiles.length; i++) {
					String attachedFile = attachedFiles[i];
					MimeBodyPart messageBodyParts = new MimeBodyPart();
					messageBodyParts.attachFile(attachedFiles[i]);
					messageBodyParts.setFileName((new File(attachedFile)).getName());
					multipart.addBodyPart(messageBodyParts);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		message.setContent(multipart);
		Transport.send(message);
	}

	public void sendMailWithAckReceipt(String toRecipients[], String ccRecipients[], String subject, String body, String attachedFiles[], boolean ackReceipt) throws MessagingException {
		initMailSession();
		mailSession.setDebug(true);
		MimeMessage message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress(mailFrom));
		message.setSubject(subject == null || subject.length() == 0 ? this.getSubject() : subject, "UTF-8");
		message.setRecipients(RecipientType.TO, buildInternetAddresses(toRecipients));
		if (ccRecipients != null)
			message.setRecipients(RecipientType.CC, buildInternetAddresses(ccRecipients));
		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();
		String mail = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
		mail = mail + "<html>\n";
		mail = mail + "<head>" + mailHeader + "</head>\n";
		mail = mail + "<body>" + body + mailFooter + "</body>\n";
		mail = mail + "</html>\n";
		messageBodyPart.setContent(mail, "text/html; charset=\"UTF-8\"");
		multipart.addBodyPart(messageBodyPart);
		if (attachedFiles != null) {
			for (int i = 0; i < attachedFiles.length; i++) {
				String attachedFile = attachedFiles[i];
				messageBodyPart = new MimeBodyPart();
				javax.activation.DataSource source = new FileDataSource(attachedFile);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName((new File(attachedFile)).getName());
				multipart.addBodyPart(messageBodyPart);
			}

		}
		message.setContent(multipart);
		if (ackReceipt)
			message.setHeader("Disposition-Notification-To", mailFrom);
		Transport.send(message);
	}

	public void sendMailWithDebug(String toRecipients[], String ccRecipients[], String subject, String body, String attachedFiles[]) throws MessagingException {
		initMailSession();
		mailSession.setDebug(true);
		MimeMessage message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress(mailFrom));
		message.setSubject(subject == null || subject.length() == 0 ? this.getSubject() : subject, "UTF-8");
		message.setRecipients(RecipientType.TO, buildInternetAddresses(toRecipients));
		if (ccRecipients != null)
			message.setRecipients(RecipientType.CC, buildInternetAddresses(ccRecipients));
		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();
		String mail = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
		mail = mail + "<html>\n";
		mail = mail + "<head>" + mailHeader + "</head>\n";
		mail = mail + "<body>" + body + mailFooter + "</body>\n";
		mail = mail + "</html>\n";
		messageBodyPart.setContent(mail, "text/html; charset=\"UTF-8\"");
		multipart.addBodyPart(messageBodyPart);
		if (attachedFiles != null) {
			for (int i = 0; i < attachedFiles.length; i++) {
				String attachedFile = attachedFiles[i];
				messageBodyPart = new MimeBodyPart();
				javax.activation.DataSource source = new FileDataSource(attachedFile);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName((new File(attachedFile)).getName());
				multipart.addBodyPart(messageBodyPart);
			}

		}
		message.setContent(multipart);
		Transport.send(message);
	}

	@SuppressWarnings("unchecked")
	private InternetAddress[] buildInternetAddresses(String emails[]) throws AddressException {
		if (emails == null)
			return null;
		List result = new ArrayList(emails.length);
		for (int i = 0; i < emails.length; i++) {
			InternetAddress address = new InternetAddress(emails[i]);
			if (!result.contains(address))
				result.add(address);
		}

		return (InternetAddress[]) result.toArray(new InternetAddress[result.size()]);
	}

	public void setDefault() {
		smtpHost = properties.getProperty("smtp.host");
		mailFrom = properties.getProperty("mail.from");
		mailHeader = properties.getProperty("mail.header");
		mailFooter = properties.getProperty("mail.footer");
		subject = properties.getProperty("mail.subject");
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public String getMailHeader() {
		return mailHeader;
	}

	public void setMailHeader(String mailHeader) {
		this.mailHeader = mailHeader;
	}

	public String getMailFooter() {
		return mailFooter;
	}

	public void setMailFooter(String mailFooter) {
		this.mailFooter = mailFooter;
	}

	public String getSubject() {
		return this.subject;
	}

	public static final String CONFIG_FILE_PATH = "/com/ezui/mail/conf/EmailNotify.properties";
	private Session mailSession;
	private String smtpHost;
	private String mailFrom;
	private String mailHeader;
	private String mailFooter;
	private Properties properties;
	private String subject;

}
