package com.ezui.utils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;

public class StringUtil {
	public static Pattern OPEN_TAG = Pattern.compile("<");

	public static String escapeJSForHTML(Object o) {
		if (o == null) {
			return null;
		}
		String s = StringEscapeUtils.escapeJavaScript(o.toString());
		if (s != null) {
			s = OPEN_TAG.matcher(s).replaceAll(Matcher.quoteReplacement("\\u003C"));
		}
		return s;
	}

	public static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() == 0) {
			return null;
		}
		byte[] ba = new byte[hex.length() / 2];
		for (int i = 0; i < ba.length; i++) {
			ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return ba;
	}

	public static String byteArrayToHex(byte[] ba) {
		if (ba == null || ba.length == 0) {
			return null;
		}
		StringBuffer sb = new StringBuffer(ba.length * 2);
		String hexNumber;
		for (int x = 0; x < ba.length; x++) {
			hexNumber = "0" + Integer.toHexString(0xff & ba[x]);
			sb.append(hexNumber.substring(hexNumber.length() - 2));
		}
		return sb.toString();
	}

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	
	/**
	 * 金額數值格式化
	 *
	 * @param num
	 *            金額數值
	 * @return currencyNum - 格式化金額數值
	 */
	public static String currencyFormat(String num) {
		String currencyNum = "";
		NumberFormat dollarFormat = NumberFormat.getCurrencyInstance(Locale.TAIWAN);
		currencyNum = dollarFormat.format(Double.parseDouble(num));
		currencyNum = currencyNum.substring(3, currencyNum.length() - 3);
		return currencyNum;
	}
	/**
	 * 重新format資料, 移除字串中有換行, TAB
	 * 
	 * @param value
	 * @return
	 */
	public static String trimBreakLine(String value) {
		if (value == null)
			return "";
//		System.out.println(" filterBreak(前):" + value);
		String formatValue = value;
		if (formatValue.indexOf("\r") > 0) // 移除字串中有換行
			formatValue = formatValue.replaceAll("\r", "");
		if (formatValue.indexOf("\n") > 0) // 移除字串中有換行
			formatValue = formatValue.replaceAll("\n", "");
//		System.out.println(" filterBreak(後):" + formatValue);
		return formatValue;
	}
	/**
	 * 檢核值的長度是否需進行截字動作
	 * 
	 * @param format_value
	 * @param limitLength
	 * @return
	 * @throws Exception
	 */
	public static String limitString(String value, String encoding, int limitLength) {
		int byteArrayLen=getByteLen(value,encoding);
		if(byteArrayLen>limitLength) {
			for(int i=(limitLength>value.length()?value.length()-1:limitLength);i>=0;i--) {
				value = value.substring(0, i);
				if(getByteLen(value,encoding)<=limitLength) {
					break;
				}
			}
		}
		return value;
	}
	public static boolean strBytesLengthThan(String utf8str, String encoding, int bytelen) {
		return (getByteLen(utf8str,encoding)>bytelen);
	}

	public static int getByteLen(String str, String encording) {
		int byteArrayLen = 0;
		try {
			byteArrayLen = str.getBytes(encording).length;
		}catch(Exception e) {
			byteArrayLen=str.getBytes().length;
		}
		return byteArrayLen;
	}
	public static String subUTF8String(String utf8str, int bytelen) {
		return limitString(utf8str,"UTF-8",bytelen);
	}

	public static List<String> utf8StrToArray(String utf8str, int bytelen, List<String> arrs) {
		if (arrs == null) {
			arrs = new ArrayList<String>();
		}
		String a = subUTF8String(utf8str, bytelen);
		if (a.length() == utf8str.length()) {
			arrs.add(a);
			return arrs;
		} else {
			arrs.add(a);
			return utf8StrToArray(utf8str.substring(a.length()), bytelen, arrs);
		}
	}

}
