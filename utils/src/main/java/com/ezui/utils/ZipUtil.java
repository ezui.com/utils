
package com.ezui.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;

/**
 * 压缩解压缩文件 工具类
 * 
 * @company ezui.com
 * @author Xiandong Liu
 * @email xiandong@ezui.com
 * @date 2022年4月29日
 */
public class ZipUtil {

	/**
	 * 压缩文件 （压缩后 删除原有的文件）
	 * 
	 * @param path 压缩文件\文件夹路径
	 * @throws IOException
	 */
	public static void zip(String path) throws ZipException {
		zip(path, true);
	}

	public static void main(String[] args) throws ZipException {
		String path = "D:/test";
		zip(path, false);
	}

	/**
	 * 压缩文件
	 * 
	 * @param path     压缩文件\文件夹路径
	 * @param isDelete 压缩后是否删除原文件\文件夹
	 * @throws IOException
	 */
	public static void zip(String path, Boolean isDelete) throws ZipException {
		ZipFile zipFile = null;
		ZipParameters parameters = new ZipParameters();
		parameters.setCompressionMethod(CompressionMethod.DEFLATE);
		parameters.setCompressionLevel(CompressionLevel.NORMAL);
		File file = new File(path);
		if (!file.exists()) {
			throw new ZipException("文件不存在");
		}
		if (file.isDirectory()) {
			zipFile = new ZipFile(new File(path + ".zip"));
			zipFile.setCharset(Charset.forName("UTF-8"));
			zipFile.addFolder(file, parameters);
		} else {
			zipFile = new ZipFile(new File(path.split(".")[0] + ".zip"));
			zipFile.setCharset(Charset.forName("UTF-8"));
			zipFile.addFile(file, parameters);
		}

		if (isDelete)
			FileUtil.deleteDir(file);
	}

	/**
	 * 压缩文件夹(加密)
	 * 
	 * @param path     压缩文件\文件夹路径
	 * @param isDelete 压缩后是否删除原文件\文件夹
	 * @param password 加密密码
	 * @throws ZipException
	 */
	public static void zipSetPass(String path, Boolean isDelete, String password) throws ZipException {
		ZipFile zipFile = null;
		ZipParameters parameters = new ZipParameters();
		parameters.setCompressionMethod(CompressionMethod.DEFLATE);
		parameters.setCompressionLevel(CompressionLevel.NORMAL);
		// 设置密码
		parameters.setEncryptFiles(true);
		parameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);
		File file = new File(path);
		if (file.isDirectory()) {
			zipFile = new ZipFile(new File(path + ".zip"), password.toCharArray());
			zipFile.setCharset(Charset.forName("UTF-8"));
			zipFile.addFolder(file, parameters);
		} else {
			zipFile = new ZipFile(new File(path.split(".")[0] + ".zip"));
			zipFile.setCharset(Charset.forName("UTF-8"));
			zipFile.addFile(file, parameters);
		}
		if (isDelete) {
			FileUtil.deleteDir(new File(path));
		}
	}

	/**
	 * 解压压缩文件
	 * 
	 * @param filePath 压缩文件路径
	 * @param toPath   解压到的文件夹路径
	 * @param password 密码 没有密码设置为""
	 * @throws ZipException
	 */
	public static void unZip(String filePath, String toPath) throws ZipException {
		unZipFile(new ZipFile(filePath), toPath);
	}

	/**
	 * 解压压缩文件
	 * 
	 * @param zipFile
	 * @param toPath
	 * @param password
	 * @throws ZipException
	 */
	public static void unZipFile(ZipFile zipFile, String toPath) throws ZipException {
		List<?> fileHeaderList = zipFile.getFileHeaders();
		for (Object o : fileHeaderList) {
			FileHeader fileHeader = (FileHeader) o;
			zipFile.extractFile(fileHeader, toPath);
		}
	}

	/**
	 * 解压文件。
	 * 
	 * @param filePath zip文件路径
	 * @param toPath   目录 void
	 * @throws ZipException
	 */
	public static void unZipFile(String filePath, String toPath) throws ZipException {
		unZip(filePath, toPath);
	}

	/**
	 * 创建文件目录
	 * 
	 * @param tempPath
	 * @param fileName 文件名称
	 * @return 文件的完整目录
	 */
	public static String createFilePath(String tempPath, String fileName) {
		File file = new File(tempPath);
		// 文件夹不存在创建
		if (!file.exists())
			file.mkdirs();
		return file.getPath() + File.separator + fileName;
	}
}
